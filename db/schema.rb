# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150822154529) do

  create_table "articles", force: :cascade do |t|
    t.string   "title",            limit: 255
    t.text     "content",          limit: 65535
    t.text     "image",            limit: 65535
    t.string   "url",              limit: 255
    t.integer  "original",         limit: 4
    t.integer  "count",            limit: 4
    t.integer  "category_news_id", limit: 4,     null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "short_content",    limit: 255
    t.string   "slug",             limit: 255
  end

  add_index "articles", ["category_news_id"], name: "index_articles_on_category_news_id", using: :btree
  add_index "articles", ["slug"], name: "index_articles_on_slug", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "type",       limit: 255
    t.integer  "visited",    limit: 4
    t.text     "image",      limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_news", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "category_stories", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "image",         limit: 255
    t.string   "description",   limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "category_type", limit: 4
    t.string   "slug",          limit: 255
  end

  add_index "category_stories", ["slug"], name: "index_category_stories_on_slug", using: :btree

  create_table "chapters", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "content",    limit: 65535
    t.string   "image",      limit: 255
    t.integer  "story_id",   limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "slug",       limit: 255
  end

  add_index "chapters", ["slug"], name: "index_chapters_on_slug", using: :btree
  add_index "chapters", ["story_id"], name: "index_chapters_on_story_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "messages", force: :cascade do |t|
    t.string   "title",      limit: 255,                   null: false
    t.string   "content",    limit: 10000,                 null: false
    t.integer  "user_id",    limit: 4,                     null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "watched",    limit: 1,     default: false
  end

  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "musics", force: :cascade do |t|
    t.string   "title",           limit: 255
    t.string   "type_music",      limit: 255
    t.string   "format",          limit: 255
    t.string   "rank",            limit: 255
    t.integer  "count_listen",    limit: 4
    t.boolean  "isvisit",         limit: 1
    t.string   "album",           limit: 255
    t.string   "singer_name",     limit: 255
    t.string   "artist",          limit: 255
    t.text     "lyrics",          limit: 65535
    t.string   "country",         limit: 255
    t.text     "url",             limit: 65535
    t.text     "url_original",    limit: 65535
    t.date     "birthday_singer"
    t.text     "information",     limit: 65535
    t.boolean  "new_imported",    limit: 1
    t.integer  "site_id",         limit: 4
    t.string   "quantity",        limit: 255
    t.text     "image",           limit: 65535
    t.integer  "category_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "lsxanh",          limit: 1,     default: false
    t.boolean  "nhachot",         limit: 1,     default: false
    t.boolean  "music_top",       limit: 1,     default: false
    t.integer  "singer_id",       limit: 4
    t.text     "singer_image",    limit: 65535
    t.string   "slug",            limit: 255
  end

  add_index "musics", ["category_id"], name: "index_musics_on_category_id", using: :btree
  add_index "musics", ["slug"], name: "index_musics_on_slug", using: :btree

  create_table "proxies", force: :cascade do |t|
    t.string   "proxy",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "proxies", ["proxy"], name: "index_proxies_on_proxy", using: :btree

  create_table "singers", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "image_singer", limit: 255
    t.string   "type",         limit: 255
    t.string   "country",      limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "singers", ["image_singer"], name: "index_singers_on_image_singer", using: :btree
  add_index "singers", ["name"], name: "index_singers_on_name", using: :btree

  create_table "stories", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "image",         limit: 255
    t.text     "content",       limit: 65535
    t.integer  "type_story",    limit: 4
    t.string   "short_content", limit: 255
    t.integer  "story_type_id", limit: 4
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "slug",          limit: 255
    t.boolean  "hot",           limit: 1,     default: false
    t.string   "memo",          limit: 255
  end

  add_index "stories", ["slug"], name: "index_stories_on_slug", using: :btree
  add_index "stories", ["story_type_id"], name: "index_stories_on_story_type_id", using: :btree

  create_table "story_types", force: :cascade do |t|
    t.string   "title",             limit: 255
    t.string   "image",             limit: 255
    t.string   "link",              limit: 255
    t.text     "description",       limit: 65535
    t.string   "slug",              limit: 255
    t.integer  "category_story_id", limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "story_types", ["category_story_id"], name: "index_story_types_on_category_story_id", using: :btree
  add_index "story_types", ["slug"], name: "index_story_types_on_slug", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.boolean  "admin",                  limit: 1,   default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name",             limit: 255,                 null: false
    t.string   "last_name",              limit: 255,                 null: false
    t.string   "nick_name",              limit: 255,                 null: false
    t.datetime "birthday",                                           null: false
    t.string   "avatar",                 limit: 255,                 null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "videos", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "channel",     limit: 255
    t.text     "link",        limit: 65535
    t.integer  "view_count",  limit: 4
    t.string   "duration",    limit: 255
    t.string   "category",    limit: 255
    t.text     "lyrics",      limit: 65535
    t.text     "images",      limit: 65535
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "category_id", limit: 4,     default: 100
    t.string   "slug",        limit: 255
  end

  add_index "videos", ["category"], name: "index_videos_on_category", using: :btree
  add_index "videos", ["name"], name: "index_videos_on_name", using: :btree
  add_index "videos", ["slug"], name: "index_videos_on_slug", using: :btree

end
