class CreateMusics < ActiveRecord::Migration
  def change
    create_table :musics do |t|
      t.string :title
      t.string :type_music
      t.string :format
      t.string :rank
      t.integer :count_listen
      t.boolean :isvisit
      t.string :album
      t.string :singer
      t.string :artist
      t.text :lyrics
      t.string :country
      t.text :url
      t.text :url_original
      t.date :birthday_singer
      t.text :information
      t.boolean :new_imported
      t.integer :site_id
      t.string :quantity
      t.string :image
      t.references :category, index: true

      t.timestamps
    end
  end
end
