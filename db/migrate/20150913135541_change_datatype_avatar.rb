class ChangeDatatypeAvatar < ActiveRecord::Migration
  def change
    change_column :users, :avatar, :string, :default => "http://sieunhac.com/images/download.gif", :null => false
    change_column :users, :nick_name, :string, :default => "Sieunhac", :null => false
  end
end
