class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :title, null: false
      t.string :content, null: false, limit: 10_000

      t.references :user, null: false, index: true
      t.timestamps null: false
    end
  end
end
