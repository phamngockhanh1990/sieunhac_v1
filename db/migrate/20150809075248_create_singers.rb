class CreateSingers < ActiveRecord::Migration
  def change
    create_table :singers do |t|
      t.string :name, index: true
      t.string :image_singer, index: true
      t.string :type
      t.string :country

      t.timestamps null: false
    end
  end
end
