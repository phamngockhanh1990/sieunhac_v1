class AddAttributesToUser < ActiveRecord::Migration
  def change
    add_column :users, :first_name,     :string,    null: false
    add_column :users, :last_name,      :string,    null: false
    add_column :users, :nick_name,      :string,    null: false
    add_column :users, :birthday,       :datetime,  null: false
    add_column :users, :avatar,         :string,    null: false
  end
end
