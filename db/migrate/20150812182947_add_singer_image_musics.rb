class AddSingerImageMusics < ActiveRecord::Migration
  def change
    add_column :musics, :singer_image, :text, limit: 1000
  end
end
