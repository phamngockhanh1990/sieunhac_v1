class AddWatchedToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :watched, :boolean, default: false
  end
end
