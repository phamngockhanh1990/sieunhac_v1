class ChangeShortContentToText < ActiveRecord::Migration
  def change
    change_column :articles, :short_content, :text, limit: 10000
  end
end
