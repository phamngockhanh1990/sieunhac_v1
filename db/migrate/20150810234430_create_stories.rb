class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.string :name
      t.string :image
      t.text :content
      t.integer :type_story
      t.string :short_content
      t.references :category_story, index: true

      t.timestamps null: false
    end
  end
end
