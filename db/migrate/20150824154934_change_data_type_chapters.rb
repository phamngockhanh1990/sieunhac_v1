class ChangeDataTypeChapters < ActiveRecord::Migration
  def change
    add_column :stories, :link, :text, limit: 2000, index: true
    add_column :chapters, :link, :text, limit: 2000, index: true

    change_column :stories, :short_content, :text, limit: 10000
    change_column :chapters, :image, :text
  end
end
