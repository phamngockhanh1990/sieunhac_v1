class AddSlugToStoryChapter < ActiveRecord::Migration
  def change
    add_column :chapters, :slug, :string, unique: true
    add_index :chapters, :slug

    add_column :category_stories, :slug, :string, unique: true
    add_index :category_stories, :slug

    add_column :stories, :slug, :string, unique: true
    add_index :stories, :slug
  end
end
