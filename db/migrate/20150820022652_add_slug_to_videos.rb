class AddSlugToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :slug, :string, unique: true
    add_index :videos, :slug
    add_column :musics, :slug, :string, unique: true
    add_index :musics, :slug
  end
end
