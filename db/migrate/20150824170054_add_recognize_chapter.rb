class AddRecognizeChapter < ActiveRecord::Migration
  def change
    add_column :chapters, :recognize, :integer, index: true
  end
end
