class AddSlugToCategoryNews < ActiveRecord::Migration
  def change
    add_column :category_news, :slug, :string, unique: true
    add_index :category_news, :slug
  end
end
