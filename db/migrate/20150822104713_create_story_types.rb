class CreateStoryTypes < ActiveRecord::Migration
  def change
    create_table :story_types do |t|
      t.string :title
      t.string :image
      t.string :link
      t.text :description
      t.string :slug, unique: true, index: true
      t.references :category_story, index: true

      t.timestamps null: false
    end

    rename_column :stories, :category_story_id, :story_type_id
  end
end
