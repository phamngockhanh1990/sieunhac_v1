class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :type
      t.integer :visited
      t.text :image

      t.timestamps
    end
  end
end
