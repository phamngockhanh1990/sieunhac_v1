class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :content
      t.text :image, limit: 300
      t.string :url
      t.integer :original
      t.integer :count
      t.references :category_news, null: false, index: true

      t.timestamps null: false
    end
  end
end
