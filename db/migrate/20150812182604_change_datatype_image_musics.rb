class ChangeDatatypeImageMusics < ActiveRecord::Migration
  def change
    change_column :musics, :image, :text, limit: 4000
  end
end
