class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :name, index: true
      t.string :channel, index: true
      t.text :link, limit: 9000
      t.integer :view_count
      t.string :duration
      t.string :category, index: true
      t.text :lyrics
      t.text :images

      t.timestamps null: false
    end
  end
end
