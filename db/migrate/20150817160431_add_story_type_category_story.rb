class AddStoryTypeCategoryStory < ActiveRecord::Migration
  def change
    add_column :category_stories, :category_type, :integer, index: true
  end
end
