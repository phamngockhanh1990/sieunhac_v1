class CreateChapters < ActiveRecord::Migration
  def change
    create_table :chapters do |t|
      t.string :title
      t.text :content
      t.string :image
      t.references :story, index: true
      t.timestamps null: false
    end
  end
end
