class ChangeSingerToSingerName < ActiveRecord::Migration
  def change
    rename_column :musics, :singer, :singer_name
  end
end
