class AddSubdomainToCategory < ActiveRecord::Migration
  def change
    add_column :category_news, :sub_category, :string
    add_column :category_news, :url, :string, index: true
  end
end
