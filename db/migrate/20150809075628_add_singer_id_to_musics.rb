class AddSingerIdToMusics < ActiveRecord::Migration
  def change
    add_column :musics, :singer_id, :integer, index: true
  end
end
