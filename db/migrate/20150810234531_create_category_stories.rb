class CreateCategoryStories < ActiveRecord::Migration
  def change
    create_table :category_stories do |t|
      t.string :name
      t.string :image
      t.string :description

      t.timestamps null: false
    end
  end
end
