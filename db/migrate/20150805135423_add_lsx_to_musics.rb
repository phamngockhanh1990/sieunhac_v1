class AddLsxToMusics < ActiveRecord::Migration
  def change
    add_column :musics, :lsxanh, :boolean, default: false
    add_column :musics, :nhachot, :boolean, default: false
    add_column :musics, :music_top, :boolean, default: false
  end
end
