class AddHotToStory < ActiveRecord::Migration
  def change
    add_column :stories, :hot, :boolean, default: false, index: true
    add_column :stories, :memo, :string
  end
end
