class AddCategoryIdToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :category_id, :integer, index: true, default: 100
  end
end
