# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
ActiveRecord::Base.connection.execute("truncate category_stories")
CategoryStory.create(name: "Truyện tranh")
CategoryStory.create(name: "Truyện chữ")
CategoryStory.create(name: "Truyện audio")

# ActiveRecord::Base.connection.execute("truncate story_types")
# obj_category_story = CategoryStory.find(1)
# [
#   {name: "Hành động", link: "http://comicvn.net/truyen-tranh/hanh-dong"},
#   {name: "Trưởng Thành", link: "http://comicvn.net/truyen-tranh/truong-thanh"},
#   {name: "Phiêu lưu", link: "http://comicvn.net/truyen-tranh/phieu-luu"},
#   {name: "Hài", link: "http://comicvn.net/truyen-tranh/hai"},
#   {name: "Comic", link: "http://comicvn.net/truyen-tranh/comic"},
#   {name: "Kinh dị", link: "http://comicvn.net/truyen-tranh/kinh-di"},
#   {name: "Manhwa", link: "http://comicvn.net/truyen-tranh/manhwa"},
#   {name: "Võ thuật", link: "http://comicvn.net/truyen-tranh/vo-thuat"},
#   {name: "One shot", link: "http://comicvn.net/truyen-tranh/one-shot"},
#   {name: "Tình cảm", link: "http://comicvn.net/truyen-tranh/tinh-cam-"},
#   {name: "Học Đường", link: "http://comicvn.net/truyen-tranh/hoc-duong"},
#   {name: "Viễn Tưởng", link: "http://comicvn.net/truyen-tranh/vien-tuong"},
#   {name: "Thể thao", link: "http://comicvn.net/truyen-tranh/the-thao"},
#   {name: "HOT", link: "http://comicvn.net/truyen-tranh/truyen-comicvn-net"},
#   {name: "Manhua", link: "http://comicvn.net/truyen-tranh/manhua"},
#   {name: "Manga", link: "http://comicvn.net/truyen-tranh/manga"},
#   {name: "Trinh Thám", link: "http://comicvn.net/truyen-tranh/trinh-tham-t"},
#   {name: "Huyền Huyễn", link: "http://comicvn.net/truyen-tranh/huyen-huyen"},
#   {name: "Truyện tranh Việt", link: "http://comicvn.net/truyen-tranh/truyen-tranh-viet"},
#   {name: "Siêu Nhiên", link: "http://comicvn.net/truyen-tranh/sieu-nhien"},
#   {name: "Art-work", link: "http://comicvn.net/truyen-tranh/art-work"},
#   {name: "Webtoon", link: "http://comicvn.net/truyen-tranh/webtoon"},
#   {name: "Ngoại Truyện", link: "http://comicvn.net/truyen-tranh/ngoai-truyen"},
#   {name: "Đời sống", link: "http://comicvn.net/truyen-tranh/doi-song"},
#   {name: "Kịch tính", link: "http://comicvn.net/truyen-tranh/kich-tinh"},
#   {name: "Bẩn bựa", link: "http://comicvn.net/truyen-tranh/ban-bua"},
#   {name: "Lịch sử", link: "http://comicvn.net/truyen-tranh/lich-su"},
#   {name: "Tâm Lý", link: "http://comicvn.net/truyen-tranh/tam-ly"},
#   {name: "Truyện siêu bựa", link: "http://comicvn.net/truyen-che/truyen-sieu-bua"},
#   {name: "Hàn Quốc Bựa", link: "http://comicvn.net/truyen-che/han-quoc-bua"},
#   {name: "Doraemon chế", link: "http://comicvn.net/truyen-che/doraemon-che"},
#   {name: "Rage Comic ( Troll )", link: "http://comicvn.net/truyen-che/rage-comic-troll"},
#   {name: "Truyện chế khác", link: "http://comicvn.net/truyen-che/truyen-che-khac"},
#   {name: "Truyện chế tục", link: "http://comicvn.net/truyen-che/truyen-che-tuc"}
# ].each do |item|
#   obj_category_story.story_types.create(
#     title: item[:name],
#     image: "/images/musics/default.jpg",
#     link: item[:link]
#   )
#   puts "Create data #{item[:name]}"
# end

puts "Start import Picture Story"
obj_category_story = CategoryStory.find(1)
[
  {name: "Hành động", link: "http://thichtruyentranh.com/action.html"},
  {name: "Trưởng Thành", link: "http://thichtruyentranh.com/adult.html"},
  {name: "Phiêu lưu", link: "http://thichtruyentranh.com/adventure.html"},
  {name: "Hài", link: "http://thichtruyentranh.com/comedy.html"},
  {name: "Drama", link: "http://thichtruyentranh.com/drama.html"},
  {name: "Ecchi", link: "http://thichtruyentranh.com/ecchi.html"},
  {name: "Viễn Tưởng", link: "http://thichtruyentranh.com/fantasy.html"},
  {name: "Gender Bender", link: "http://thichtruyentranh.com/gender-bender.html"},
  {name: "Harem", link: "http://thichtruyentranh.com/harem.html"},
  {name: "Horror", link: "http://thichtruyentranh.com/horror.html"},
  {name: "Josei", link: "http://thichtruyentranh.com/josei.html"},
  {name: "Manhua", link: "http://thichtruyentranh.com/manhua.html"},
  {name: "Manhwa", link: "http://thichtruyentranh.com/manhwa.html"},
  {name: "Martial Arts", link: "http://thichtruyentranh.com/martial-arts.html"},
  {name: "Mature", link: "http://thichtruyentranh.com/mature.html"},
  {name: "Mecha", link: "http://thichtruyentranh.com/mecha.html"},
  {name: "Mystery", link: "http://thichtruyentranh.com/mystery.html"},
  {name: "One Shot", link: "http://thichtruyentranh.com/one-shot.html"},
  {name: "Psychological", link: "http://thichtruyentranh.com/psychological.html"},
  {name: "Romance", link: "http://thichtruyentranh.com/romance.html"},
  {name: "School Life", link: "http://thichtruyentranh.com/school-life.html"},
  {name: "Sci-fi", link: "http://thichtruyentranh.com/sci-fi.html"},
  {name: "Seinen", link: "http://thichtruyentranh.com/seinen.html"},
  {name: "Shoujo", link: "http://thichtruyentranh.com/shoujo.html"},
  {name: "Shoujo-ai", link: "http://thichtruyentranh.com/shoujo-ai.html"},
  {name: "Soft Yuri", link: "http://thichtruyentranh.com/soft-yuri.html"},
  {name: "Lịch sử", link: "http://thichtruyentranh.com/historical.html"},
  {name: "Sports", link: "http://thichtruyentranh.com/sports.html"},
  {name: "Supernatural", link: "http://thichtruyentranh.com/supernatural.html"},
  {name: "Tragedy", link: "http://thichtruyentranh.com/tragedy.html"},
  {name: "16+", link: "http://thichtruyentranh.com/16.html"},
  {name: "18+", link: "http://thichtruyentranh.com/18.html"},
  {name: "Anime", link: "http://thichtruyentranh.com/anime.html"},
  {name: "Comic", link: "http://thichtruyentranh.com/comic.html"},
  {name: "Doujinshi", link: "http://thichtruyentranh.com/doujinshi.html"},
  {name: "Live action", link: "http://thichtruyentranh.com/live-action.html"},
  {name: "Magic", link: "http://thichtruyentranh.com/magic.html"},
  {name: "Manga", link: "http://thichtruyentranh.com/manga.html"},
  {name: "Nấu Ăn", link: "http://thichtruyentranh.com/nau-an.html"},
  {name: "Smut", link: "http://thichtruyentranh.com/smut.html"},
  {name: "Tạp chí truyện tranh", link: "http://thichtruyentranh.com/tap-chi-truyen-tranh.html"},
  {name: "Trap (Crossdressing)", link: "http://thichtruyentranh.com/trap-crossdressing.html"}
].each do |item|
  info = {
    title: item[:name],
    image: "/images/musics/default.jpg",
    link: item[:link]
  }

  check_story_type = obj_category_story.story_types.find_by(link: info[:link])

  if check_story_type.present?
    check_story_type.update(info)
    puts "Update data #{item[:name]}"
  else
    obj_category_story.story_types.create(info)
    puts "Create data #{item[:name]}"
  end
end


##### Truyen Chu
# puts "Start import Novel"
# obj_category_story = CategoryStory.find(2)
# [
#   {name: "Kiếm Hiệp", link: "http://truyenyy.com/danhmuctruyen/?loai_truyen=all&the_loai=1&sap_xep=alphabet&page=1"},
#   {name: "Huyền Huyễn", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=10&sap_xep=alphabet&page=1"},
#   {name: "Trinh Thám", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=15&sap_xep=alphabet&page=1"},
#   {name: "Kỳ Bí", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=21&sap_xep=alphabet&page=1"},
#   {name: "Tiên Hiệp", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=2&sap_xep=alphabet&page=1"},
#   {name: "Võng Du", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=26&sap_xep=alphabet&page=1"},
#   {name: "Khoa Huyễn", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=11&sap_xep=alphabet&page=1"},
#   {name: "Ngôn Tình", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=16&sap_xep=alphabet&page=1"},
#   {name: "Ma Pháp", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=22&sap_xep=alphabet&page=1"},
#   {name: "Huyển Ảo", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=3&sap_xep=alphabet&page=1"},
#   {name: "Xuyên Không", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=5&sap_xep=alphabet&page=1"},
#   {name: "Tu Chân", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=12&sap_xep=alphabet&page=1"},
#   {name: "Quan Trường", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=19&sap_xep=alphabet&page=1"},
#   {name: "Quỷ Tu", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=24&sap_xep=alphabet&page=1"},
#   {name: "Sắc Hiệp", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=4&sap_xep=alphabet&page=1"},
#   {name: "Dị Giới", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=6&sap_xep=alphabet&page=1"},
#   {name: "Lịch Sử Quân Sự", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=13&sap_xep=alphabet&page=1"},
#   {name: "Truyện Teen", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=17&sap_xep=alphabet&page=1"},
#   {name: "Cổ Đại", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=25&sap_xep=alphabet&page=1"},
#   {name: "Đô Thị", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=7&sap_xep=alphabet&page=1"},
#   {name: "Dị Năng", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=8&sap_xep=alphabet&page=1"},
#   {name: "Viễn Tưởng", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=14&sap_xep=alphabet&page=1"},
#   {name: "Thám Hiểm", link: "http://truyenyy.com/danhmuctruyen?loai_truyen=all&the_loai=20&sap_xep=alphabet&page=1"}
# ].each do |item|
#   obj_category_story.story_types.create(
#     title: item[:name],
#     image: "/images/musics/default.jpg",
#     link: item[:link]
#   )
#   puts "Create data #{item[:name]}"
# end


##### Truyen Audio
puts "Start import Audio Story"
obj_category_story = CategoryStory.find(3)
[
  {name: "Ý nghĩa cuộc sống", link: "http://www.radiotruyen.com/radio-y-nghia-cuoc-song.html"},
  {name: "Truyện thiếu nhi", link: "http://www.radiotruyen.com/radio-truyen-thieu-nhi.html"},
  {name: "Truyện ngắn", link: "http://www.radiotruyen.com/radio-truyen-ngan.html"},
  {name: "Truyện dài kỳ", link: "http://www.radiotruyen.com/radio-truyen-dai-ky.html"},
  {name: "Kinh điển", link: "http://www.radiotruyen.com/radio-truyen-kinh-dien.html"},
  {name: "Tình yêu", link: "http://www.radiotruyen.com/radio-tinh-yeu.html"},
  {name: "Cung Hoàng Đạo", link: "http://www.radiotruyen.com/cung-hoang-dao.html"},
  {name: "Quick & Snow", link: "http://www.radiotruyen.com/radio-quick-and-snow-show.html"},
  {name: "Sách nói", link: "http://www.radiotruyen.com/sach-noi.html"},
  {name: "Dã sử & Kiếm hiệp", link: "http://www.radiotruyen.com/radio-truyen-kiem-hiep.html"},
  {name: "Kinh dị", link: "http://www.radiotruyen.com/radio-kinh-di.html"}
].each do |item|
  info = {
    title: item[:name],
    image: "/images/musics/default.jpg",
    link: item[:link]
  }

  check_story_type = obj_category_story.story_types.find_by(link: info[:link])

  if check_story_type.present?
    check_story_type.update(info)
    puts "Update data #{item[:name]}"
  else
    obj_category_story.story_types.create(info)
    puts "Create data #{item[:name]}"
  end
end
