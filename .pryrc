require 'hirb'
require 'awesome_print'

# hirb | awesome_print

Hirb.enable

old_print = Pry.config.print
Pry.config.print = proc do |output, value|
  Hirb::View.view_or_page_output(value) || output.puts(value.ai)
  # old_print.call(output, value)
end

