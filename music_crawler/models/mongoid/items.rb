# coding: utf-8
require 'mongoid'

module MusicCrawler
  module Models
    class Item
      include Mongoid::Document
      before_save :updated_at

      field :title, type: String, default: ""
      field :type_music, type: String, default: ""
      field :count_listen, type: Integer
      field :rank, type:  String, default: ""
      field :format, type: String, default: ""
      field :isvisit, type: Integer
      field :album, type: String, default: ""
      field :singer, type: String
      field :artist, type: String
      field :link, type: String, default: ""
      field :link_original, type: String, default: ""
      field :information, type: String, default: ""
      field :lyrics, type: String, default: ""
      field :country, type: String, default: ""
      field :category_id, type: Integer
      field :category_name, type: String
      field :album_description, type: String, default: ""
      field :images, type: String, default: ""
      field :added_or_updated, type: Boolean, default: true
      field :updated_at, type: DateTime
      field :birthday_singer, type: DateTime
      field :quality, type: String
      field :site_id, type: Integer

      index link: 1
      index site_name: 1
      index site_id: 1
      index title: 1
      index brand_name: 1

      def updated_at
        self.updated_at = DateTime.now
        self.added_or_updated = true
      end

      def initialize_mechanize(url)
        agent = Mechanize.new
        page = agent.get(url)
        uri = page.try(:uri)
        host_name = uri.try(:host)
        path = uri.try(:path)
        "http://#{host_name}#{path}"
      end

    end
  end
end
