# encoding: utf-8
require File.expand_path(File.dirname(__FILE__)) + '/yaml_base_with_code.rb'
class MasterCategory < YAMLBaseWithCode
  set_file(File.dirname(__FILE__) + "/master_db/category.yml")

  def category_id
    data[0].to_i
  end

  def category_name
    data[1].try(:to_s).parameterize
  end

  class << self
    def all
      # return @all_categories if @all_categories
      @all_categories = super.sort_by{|c| c.category_id}
      return @all_categories
    end

    def by_category_3_id(id)
      all.detect{|o| id == o.category_id
      }
    end

  end

end
