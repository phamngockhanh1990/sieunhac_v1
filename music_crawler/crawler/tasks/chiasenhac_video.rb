# encoding: utf-8
require 'nokogiri'
require 'unicode'
require 'uri'
require 'date'
require 'pry'

module MusicCrawler
  module Tasks
    class ChiaSeNhacVideoProcessAtMainPage
      def self.execute(url, body)
        begin
          list_all = ChiaSeNhacVideoCategory.all
          list_all.each do |link|
            # GlobalClass.request_class(link.id, "ChiaSeNhacVideoProcessAtCatePage")
            MusicCrawler::Models::Resource.create({
              url: link.id,
              task: 'ChiaSeNhacVideoProcessAtCatePage',
              is_visited: false,
              site_name: 'ChiasenhacVideo',
              store: MusicCrawler::Models::Store.find_by(name: 'ChiasenhacVideo')
            })
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class GlobalClass
      def self.request_class(link, name)
        MusicCrawler::Models::Resource.create({
          url: link,
          task: name,
          is_visited: false,
          site_name: 'ChiasenhacVideo',
          store: MusicCrawler::Models::Store.find_by(name: 'ChiasenhacVideo')
        })
      end
    end

    class ChiaSeNhacVideoProcessAtCatePage
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          binding.pry
          parse_link = doc.xpath("//div[@class='jqueryslidemenu']//@href")
          arr_link_cat = []
          if parse_link.present?
            parse_link.each do |get_link|
              arr_link_cat << "http://chiasenhac.com/" + get_link.value if get_link.value.present?
            end
          end

        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end
  end
end
