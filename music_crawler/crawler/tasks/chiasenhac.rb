# encoding: utf-8
require 'nokogiri'
require 'unicode'
require 'uri'
require 'date'
require 'pry'

module MusicCrawler
  module Tasks
    class ChiaSeNhacProcessAtMainPage
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          parse_link = doc.xpath("//div[@class='center']//div[@class='artist-txt']//a")
          arr_link_cat = []
          if parse_link.present?
            parse_link.each do |get_link|
              arr_link_cat << { name: get_link.attributes["title"].value, link: get_link.attributes["href"].value }
            end
          end

          arr_link_cat.each do |link|
            unless MusicCrawler::Models::Resource.where(url: link[:link]).exists?
              MusicCrawler::Models::Resource.create({
                url: link[:link],
                task: 'ChiaSeNhacProcessAtCatePage',
                is_visited: false,
                site_name: 'Chiasenhac',
                store: MusicCrawler::Models::Store.find_by(name: 'Chiasenhac'),
                singer: link[:name]
              })
            end
          end

        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ChiaSeNhacProcessAtCatePage
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          singer_name = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)
          (1..6).each do |page|
            link = "#{url}?page=#{page}"
            unless MusicCrawler::Models::Resource.where(url: link).exists?
              MusicCrawler::Models::Resource.create({
                url: link,
                task: 'ChiaSeNhacProcessAtCatePageList',
                is_visited: false,
                site_name: 'Chiasenhac',
                store: MusicCrawler::Models::Store.find_by(name: 'Chiasenhac'),
                singer: singer_name
              })
            end
          end

        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ChiaSeNhacProcessAtCatePageList
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          singer_name = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)
          list_songs = doc.xpath("//div[@class='tenbh']//a")

          list_songs.each do |song|
            link = "http://chiasenhac.com/#{song.attributes["href"].value}"
            unless MusicCrawler::Models::Resource.where(url: link).exists?
              MusicCrawler::Models::Resource.create({
                url: link,
                task: 'ChiaSeNhacProcessAtCatePageDetail',
                is_visited: false,
                site_name: 'Chiasenhac',
                store: MusicCrawler::Models::Store.find_by(name: 'Chiasenhac'),
                singer: singer_name,
                song_title: song.children.first.text()
              })
            end
          end

        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ChiaSeNhacProcessAtCatePageDetail
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        singer_name = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)
        song_title = MusicCrawler::Models::Resource.find_by(url: url).try(:song_title)

        begin
          item = MusicCrawler::Models::Item.new
          item.type_music = "Other"
          item.link_original = url
          item.site_id = 3
          item.images = ""
          item.category_name = "Other"
          item.title = song_title
          item.singer = singer_name

          lyrics = doc.xpath("//p[@class='genmed']")
          item.lyrics = lyrics.present? ? lyrics : "Updating lyrics..."

          ### parse album vs country
          info_song = "Other"

          # parse album
          item.album = "Other"

          parse_music = doc.css("noscript")
          if parse_music.present?
            get_content = ""
            parse_music.first.children.children.each do |script|
              if script.attributes.present? && script.attributes["src"].present? && script.attributes["src"].value.present? && script.attributes["src"].value.include?("http://chiasenhac.com/player/audio-player")
                item.link = script.attributes["src"].value
                break
              end
            end
          end
          if item.link_original.present? && item.title.present?
            puts "add data"
            item.save!
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end
  end
end
