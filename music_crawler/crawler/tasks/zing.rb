# encoding: utf-8
require 'nokogiri'
require 'unicode'
require 'uri'
require 'date'
require 'net/http'
require 'pry'
require 'mechanize'

module MusicCrawler
  module Tasks
    class ZingProcessAtMainPage
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          [
            {link: "http://mp3.zing.vn/the-loai-nghe-si/Viet-Nam/IWZ9Z08I.html", type_music: "vietnam"}
            # {link: "http://mp3.zing.vn/the-loai-nghe-si/Au-My/IWZ9Z08O.html", type_music: "aumy"},
            # {link: "http://mp3.zing.vn/the-loai-nghe-si/Khong-Loi/IWZ9Z086.html", type_music: "khongloi"},
            # {link: "http://mp3.zing.vn/the-loai-nghe-si/Han-Quoc/IWZ9Z08W.html", type_music: "korea"},
            # {link: "http://mp3.zing.vn/the-loai-nghe-si/Nhat-Ban/IWZ9Z08Z.html", type_music: "japan"},
            # {link: "http://mp3.zing.vn/the-loai-nghe-si/Hoa-Ngu/IWZ9Z08U.html", type_music: "china"}
          ].each do |item|
              puts "Start crawl #{item[:link]}"
              MusicCrawler::Models::Resource.create({
                url: item[:link],
                task: 'ZingProcessAtCatePagePaging',
                is_visited: false,
                site_name: 'Zing',
                type_music: item[:type_music],
                store: MusicCrawler::Models::Store.find_by(name: 'Zing')
              })
            # end
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ZingProcessAtCatePagePaging
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          type_music = MusicCrawler::Models::Resource.find_by(url: url).try(:type_music)

          parse_paging = doc.xpath("//div[@class='pagination']//a")
          if parse_paging.present?
            number_paging = parse_paging.last.try(:attributes)["href"].try(:value).split("?page=").try(:last).to_i
            if number_paging.present?
              (1..number_paging).each do |i|
                detail_url = url + "?page=#{i}"
                unless MusicCrawler::Models::Resource.where(url: detail_url).exists?
                  MusicCrawler::Models::Resource.create({
                    url: detail_url,
                    task: 'ZingProcessAtCatePage',
                    is_visited: false,
                    site_name: 'Zing',
                    type_music: type_music,
                    store: MusicCrawler::Models::Store.find_by(name: 'Zing')
                  })
                end
              end
            end
          else
            parse_url = "#{url}?test"
            unless MusicCrawler::Models::Resource.where(url: parse_url).exists?
              MusicCrawler::Models::Resource.create({
                url: parse_url,
                task: 'ZingProcessAtCatePage',
                is_visited: false,
                site_name: 'Zing',
                type_music: type_music,
                store: MusicCrawler::Models::Store.find_by(name: 'Zing')
              })
            end
          end

        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ZingProcessAtCatePage
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          type_music = MusicCrawler::Models::Resource.find_by(url: url).try(:type_music)
          parse_link = doc.xpath("//div[@class='tab-pane']//a[@class='thumb']")

          if parse_link.present?
            parse_link.each do |category|
              singer_name = category.try(:attributes)["title"].try(:value)
              image_singer = category.try(:children)[1].try(:attributes)["src"].try(:value)
              detail_url = "#{category.try(:attributes)["href"].try(:value)}/bai-hat"
              if category.try(:attributes)["href"].try(:value).present?
                unless MusicCrawler::Models::Resource.where(url: detail_url).exists?
                  MusicCrawler::Models::Resource.create({
                    url: detail_url,
                    task: 'ZingProcessGetAllLinkPaging',
                    is_visited: false,
                    site_name: 'Zing',
                    type_music: type_music,
                    singer: singer_name,
                    image_singer: image_singer,
                    store: MusicCrawler::Models::Store.find_by(name: 'Zing')
                  })
                end
              end
            end
          end

        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ZingProcessGetAllLinkPaging
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          type_music = MusicCrawler::Models::Resource.find_by(url: url).try(:type_music)
          singer_name = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)
          image_singer = MusicCrawler::Models::Resource.find_by(url: url).try(:image_singer)

          parse_paging = doc.xpath("//div[@class='pagination']//a")
          if parse_paging.present?
            number_paging = parse_paging.last.try(:attributes)["href"].try(:value).split("&page=").try(:last).to_i
            if number_paging.present?
              (1..number_paging).each do |i|
                detail_url = url + "?&page=#{i}"
                unless MusicCrawler::Models::Resource.where(url: detail_url).exists?
                  MusicCrawler::Models::Resource.create({
                    url: detail_url,
                    task: 'ZingProcessGetAllLink',
                    is_visited: false,
                    site_name: 'Zing',
                    singer: singer_name,
                    image_singer: image_singer,
                    type_music: type_music,
                    store: MusicCrawler::Models::Store.find_by(name: 'Zing')
                  })
                end
              end
            end
          else
            check_present = doc.xpath("//div[@class='box-404']//p[@class='title-404']").text().strip.squish
            unless check_present.present?
              all_link_page = "#{url}?test_link"
              unless MusicCrawler::Models::Resource.where(url: all_link_page).exists?
                MusicCrawler::Models::Resource.create({
                  url: all_link_page,
                  task: 'ZingProcessGetAllLink',
                  is_visited: false,
                  site_name: 'Zing',
                  singer: singer_name,
                  image_singer: image_singer,
                  type_music: type_music,
                  store: MusicCrawler::Models::Store.find_by(name: 'Zing')
                })
              end
            end
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ZingProcessGetAllLink
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          type_music = MusicCrawler::Models::Resource.find_by(url: url).try(:type_music)
          singer_name = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)
          image_singer = MusicCrawler::Models::Resource.find_by(url: url).try(:image_singer)

          link_musics = doc.xpath("//div[@class='list-item full-width']//h3[@class='txt-primary']//a[@href]")
          if link_musics.present?
            link_musics.each do |link|
              detail_url = link.try(:attributes)["href"].try(:value)
              unless MusicCrawler::Models::Resource.where(url: detail_url).exists?
                MusicCrawler::Models::Resource.create({
                  url: detail_url,
                  task: 'ZingProcessGetLinkDetail',
                  is_visited: false,
                  site_name: 'Zing',
                  singer: singer_name,
                  image_singer: image_singer,
                  type_music: type_music,
                  store: MusicCrawler::Models::Store.find_by(name: 'Zing')
                })
              end
            end
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ZingProcessGetLinkDetail
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        type_music = MusicCrawler::Models::Resource.find_by(url: url).try(:type_music)
        singer_name = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)
        image_singer = MusicCrawler::Models::Resource.find_by(url: url).try(:image_singer)

        if MusicCrawler::Models::Item.where(url: url).exists?
          MusicCrawler::Models::Resource.where(url: url).destroy_all
        end
        begin
          item = MusicCrawler::Models::Item.new
          item.type_music = type_music
          item.link_original = url
          item.site_id = 1
          item.images = image_singer
          # item.category_name = category_name
          parse_title = doc.at_xpath("//div[@class='info-content']/h1[@class='txt-primary']").try(:text).try(:strip)
          if parse_title.present?
            item.title = parse_title
          end

          item.singer = singer_name

          lyrics = doc.xpath("//div[@class='lyrics-container po-r']")
          item.lyrics = lyrics

          ### album_description
          item.information = lyrics.text().try(:strip).try(:squish)

          ### parse album vs country
          info_song = doc.xpath("//div[@class='info-song-top']")

          # parse album
          album = info_song.try(:first).text().strip.squish
          item.album = album if album.present? && album.downcase.include?("album")

          # parse_music = doc.css("script")
          # if parse_music.present?
          #   get_content = ""
          #   parse_music.each do |script|
          #     if script.content.include?("http://mp3.zing.vn/xml/song-xml/")
          #       get_content = script.content
          #       break
          #     end
          #   end
          #   if get_content.present?
          #     response_link = get_content.scan(/http\:\/\/mp3\.zing\.vn\/xml\/song\-xml\/[a-zA-Z]+/).try(:first)
          #   end
          #   if response_link.present?
          #     begin
          #       uri = URI(response_link)
          #       request = Net::HTTP::Get.new(uri.request_uri)
          #       response = Net::HTTP.get_response(uri)
          #       get_real_link = response.body.squish.scan(/\<\!\[CDATA\[http[0-9a-zA-Z|.|:|\/\/|\-|\_|\=]+/)
          #       arr_link = []
          #       get_real_link.each do |link|
          #         if link.include?("load-song")
          #           item.link = item.initialize_mechanize(link.gsub("<![CDATA[",""))
          #         end
          #       end
          #     rescue => e
          #       puts e.inspect
          #       puts e.backtrace
          #     end
          #   end
          # end
          if item.link_original.present? && item.title.present?
            puts "add data"
            item.save!
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end
  end
end
