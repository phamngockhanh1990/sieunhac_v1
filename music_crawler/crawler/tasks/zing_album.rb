# encoding: utf-8
require 'nokogiri'
require 'unicode'
require 'uri'
require 'date'
require 'net/http'
require 'pry'

module MusicCrawler
  module Tasks
    class ZingAlbumProcessAtMainPage
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        base_url = "http://mp3.zing.vn"
        begin
          parse_cat = doc.xpath("//ul[@class='genre-tab']//a")
          if parse_cat.present?
            parse_cat.each do |category|
              detail_url = base_url + category.attributes["href"].try(:value)
              country_name = category.try(:children).try(:text)
              puts "#{country_name} with url: #{detail_url}"
              MusicCrawler::Models::Resource.create({
                url: detail_url,
                task: 'ZingAlbumProcessAtCatePage',
                is_visited: false,
                site_name: 'Zing',
                country: country_name,
                store: MusicCrawler::Models::Store.find_by(name: 'Zing')
              })
            end
          end

        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ZingAlbumProcessAtCatePage
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        base_url = "http://mp3.zing.vn"
        begin
          country_name = MusicCrawler::Models::Resource.find_by(url: url).try(:country)
          parse_link = doc.xpath("//div[@class='genre-tab-item']//a")
          if parse_link.present?
            parse_link.each do |category|
              category_name = category.try(:children).try(:text)
              detail_url = base_url + category.attributes["href"].try(:value)
              MusicCrawler::Models::Resource.create({
                url: detail_url,
                task: 'ZingAlbumProcessGetAllLink',
                is_visited: false,
                site_name: 'Zing',
                category: category_name,
                country: country_name,
                store: MusicCrawler::Models::Store.find_by(name: 'Zing')
              })
            end
          end

        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ZingAlbumProcessGetAllLink
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        base_url = "http://mp3.zing.vn"
        begin
          country_name = MusicCrawler::Models::Resource.find_by(url: url).try(:country)
          category_name = MusicCrawler::Models::Resource.find_by(url: url).try(:category)
          parse_paging = doc.xpath("//div[@class='content-block']/div[@class='paging']//@href").try(:last).try(:value)
          if parse_paging.present?
            number_paging = parse_paging.split(/total_play\&p\=/).try(:last).try(:to_i)
            for i in 1..number_paging
              detail_url = url + "?sort=total_play&p=#{i}"
              MusicCrawler::Models::Resource.create({
                url: detail_url,
                task: 'ZingAlbumProcessGetAllLinkDetail',
                is_visited: false,
                site_name: 'Zing',
                category: category_name,
                country: country_name,
                store: MusicCrawler::Models::Store.find_by(name: 'Zing')
              })
            end
          else
            MusicCrawler::Models::Resource.create({
              url: url,
              task: 'ZingAlbumProcessGetAllLinkDetail',
              is_visited: false,
              site_name: 'Zing',
              category: category_name,
              country: country_name,
              store: MusicCrawler::Models::Store.find_by(name: 'Zing')
            })
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ZingAlbumProcessGetAllLinkDetail
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        base_url = "http://mp3.zing.vn"
        begin
          country_name = MusicCrawler::Models::Resource.find_by(url: url).try(:country)
          category_name = MusicCrawler::Models::Resource.find_by(url: url).try(:category)
          parse_list = doc.xpath("//div[@class='content-block']/div[@class='album-item']/span[@class='album-detail-img']")

          if parse_list.present?
            parse_list.each do |list|
              detail_url = base_url + list.try(:children)[0].try(:attributes)["href"].try(:value)
              unless MusicCrawler::Models::Resource.where(url: detail_url).exists?
                MusicCrawler::Models::Resource.create({
                  url: detail_url,
                  task: 'ZingProcessGetLinkDetail',
                  is_visited: false,
                  site_name: 'Zing',
                  category: category_name,
                  country: country_name,
                  store: MusicCrawler::Models::Store.find_by(name: 'Zing')
                })
              end
            end
          end

        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class ZingAlbumProcessGetLinkDetail
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        country_name = MusicCrawler::Models::Resource.find_by(url: url).try(:country)
        category_name = MusicCrawler::Models::Resource.find_by(url: url).try(:category)
        begin
          item = MusicCrawler::Models::Item.new
          item.country = country_name
          item.link_original = url

          parse_title = doc.xpath("//h1[@class='detail-title']")[0].try(:children)[0].try(:text)
          if parse_title.present?
            song_title = parse_title.gsub(/\-/,"").try(:strip) if parse_title.include?("-")
            item.title = song_title
          end

          parse_singer = doc.xpath("//h1[@class='detail-title']")[0].try(:children)[1].try(:children).try(:text).try(:strip)
          item.singer = parse_singer if parse_singer.present?

          parse_description = doc.xpath("//div[@class='content-block singer-info']//div[@class='singer-info-block']//p[@id='_artistBio']")
          if parse_description.present?
            item.information = parse_description.try(:text).try(:strip)
          end

          parse_image = doc.xpath("//div[@class='content-block singer-info']//div[@class='singer-info-block']//span[@class='singer-image']//@src")
          if parse_image.present?
            item.images = parse_image[0].try(:value)
          end

          
          # quality = doc.at_xpath("//div[@class='box_tag']/span")
          # item.quality = quality.text.squish if quality.present?
          item.site_id = 6

          parse_album_description = doc.xpath("//div[@class='album-info']//p[@id='_albumIntro']")
          if parse_album_description.present?
            item.album_description = parse_album_description.try(:text).try(:strip)
          end

          # lyrics = doc.xpath("//p[@class='pd_lyric']")
          # item.lyrics = lyrics.text if lyrics.present?

          # count_listen = doc.xpath("//span[@id='nowPlayingListenCount']")
          # item.count_listen = count_listen.text.to_i if count_listen.present?
          # type_music = doc.xpath("//*[@id='divShowInfo']/div[4]/a")
          # item.type_music = type_music.text if type_music.present?
          parse_lyrics = doc.xpath("//div[@class='music-function-box _divPlsLite none']")
          arr_lyrics = []
          if parse_lyrics.present?
            parse_lyrics.each do |lyrics|
              id_lyrics = lyrics.try(:attributes)["id"].try(:value).gsub("_divPlsLite","")
              if id_lyrics.present?
                puts "http://mp3.zing.vn/ajax/lyrics/lyrics?from=0&id=#{id_lyrics}&callback=zmCore.js"
                arr_lyrics << "http://mp3.zing.vn/ajax/lyrics/lyrics?from=0&id=#{id_lyrics}&callback=zmCore.js"
              end
            end
          end
          if arr_lyrics.present?
            begin
              arr_lyrics.each do |get_lyrics|
                uri = URI(get_lyrics)
                response = Net::HTTP.get_response(uri)
                get_content_lyrics = response.body
              end
            rescue => e
              puts e.inspect
              puts e.backtrace
            end
          end

          parse_music = doc.css("script")
          if parse_music.present?
            get_content = ""
            parse_music.each do |script|
              if script.content.include?("http://mp3.zing.vn/xml/album-xml/")
                get_content = script.content 
                break
              end
            end

            if get_content.present?
              response_link = get_content.scan(/http\:\/\/mp3\.zing\.vn\/xml\/album\-xml\/[a-zA-Z]+/).try(:first)
            end
            if response_link.present?
              begin
                uri = URI(response_link)
                request = Net::HTTP::Get.new(uri.request_uri)
                response = Net::HTTP.get_response(uri)
                get_real_link = response.body.squish.scan(/\<\!\[CDATA\[http[0-9a-zA-Z|.|:|\/\/|\-|\_|\=]+/)
                arr_link = []
                get_real_link.each do |link|
                  arr_link << link.gsub("<![CDATA[","")
                end
                item.link = arr_link.join(",")
              rescue => e
                puts e.inspect
                puts e.backtrace
              end
            end
          end

          puts item.format
          if item.link.present? && item.title.present?
            item.save!
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

  end
end
