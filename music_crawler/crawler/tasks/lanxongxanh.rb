# encoding: utf-8
require 'nokogiri'
require 'unicode'
require 'uri'
require 'date'
require 'net/http'
require 'pry'

module MusicCrawler
  module Tasks
    class LanXongXanhProcessAtMainPage
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        base_url = "http://www.lansongxanh.vn/"
        begin
          list_links = doc.xpath("//div[@class='moduleHolderLeft shadowModuleHolderLeft']//div[@id='tab-2']//a[@class='resultNameLink']")
          list_links.each do |link|
            link_detail = "#{base_url}#{link.attributes["href"].value}"
            MusicCrawler::Models::Resource.create({
              url: link_detail,
              task: 'LanXongXanhProcessGetLinkDetail',
              is_visited: false,
              site_name: 'Lanxongxanh',
              store: MusicCrawler::Models::Store.find_by(name: 'Lanxongxanh')
            })
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class LanXongXanhProcessGetLinkDetail
      def self.title(doc)
        doc.xpath("//h1[@itemprop='name']").try(:text).try(:strip)
      end

      def self.get_singer(doc, url)
      end

      def self.type_music(doc, url)
        doc.xpath("//div[@class='box_download']//a[@alt]").try(:text).try(:strip)
      end

      def self.get_format(doc, url)
      end

      def self.get_image(doc, url)
      end

      def self.get_artist(doc, url)
      end

      def self.get_lyrics(doc, url)
      end

      def self.get_url(doc, url)
      end

      def self.execute(url, body)
        doc = Nokogiri::HTML(body)

        begin
          item = MusicCrawler::Models::Item.new
          item.title = self.title(doc)
          quality = doc.at_xpath("//div[@class='box_tag']/span")
          item.quality = quality.text.squish if quality.present?
          item.site_id = 0

          lyrics = doc.xpath("//p[@class='pd_lyric']")
          item.lyrics = lyrics.text if lyrics.present?

          count_listen = doc.xpath("//span[@id='nowPlayingListenCount']")
          item.count_listen = count_listen.text.to_i if count_listen.present?
          type_music = doc.xpath("//*[@id='divShowInfo']/div[4]/a")
          item.type_music = type_music.present? ? type_music : self.type_music(doc, url)
          item.link_original = url

          ### process get link mp3
          parse_music = doc.css("script")
          if parse_music.present?
            get_content = ""
            parse_music.each do |script|
              get_content = script.content if script.content.include?("http://www.nhaccuatui.com/flash/xml?key1")
            end

            if get_content.present?
              response_link = get_content.scan(/http\:\/\/www\.nhaccuatui\.com\/flash\/xml\?key1\=[0-9a-zA-Z]+/)[0]
            end
            if response_link.present?
              begin
                uri = URI(response_link)
                request = Net::HTTP::Get.new(uri.request_uri)
                response = Net::HTTP.get_response(uri)
                get_real_link = response.body.squish.scan(/\<\!\[CDATA\[[0-9a-zA-Z|.|:|\/\/|\-|\_]+/)
                get_real_link.each do |link|
                  item.link = link.gsub("<![CDATA[","") if link.include?("mp3")
                end

                puts "-------------------------------------"
                puts url
                puts item.link
                puts "-------------------------------------"
                item.category_name = title
                if item.link.present?
                  item.format = item.link.split(".").last
                end
              rescue => e
                puts e.inspect
                puts e.backtrace
              end
            end
          end

          puts item.format
          if item.link.present? && item.title.present?
            item.save!
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

  end
end
