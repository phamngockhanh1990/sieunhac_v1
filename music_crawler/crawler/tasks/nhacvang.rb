# encoding: utf-8
require 'nokogiri'
require 'unicode'
require 'uri'
require 'date'
require 'net/http'
require 'pry'
require 'mechanize'
NHACVANG = "http://nhacvang.org"

module MusicCrawler
  module Tasks
    class NhacVangProcessAtMainPage
      def self.execute(url, body)
        begin
          (1..9).each do |page|
            detail_url = url.gsub("-1", "-#{page}")
            puts "Start crawl #{detail_url}"
            MusicCrawler::Models::Resource.create({
              url: detail_url,
              task: 'NhacVangProcessAtCatePagePaging',
              is_visited: false,
              site_name: 'NhacVang',
              store: MusicCrawler::Models::Store.find_by(name: 'NhacVang')
            })
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class NhacVangProcessAtCatePagePaging
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          parse_singers = doc.xpath("//div[@class='album_s02']/div[@class='Col']/div/a")
          if parse_singers.present?
            parse_singers.each do |singer|
              last_url = singer.attributes["href"].value.split("ca-si-").last.gsub("/","-111/")
              detail_url = "#{NHACVANG}/bai-hat-ca-si-#{last_url}"
              unless MusicCrawler::Models::Resource.where(url: detail_url).exists?
                MusicCrawler::Models::Resource.create({
                  url: detail_url,
                  task: 'NhacVangProcessAtCatePage',
                  is_visited: false,
                  site_name: 'NhacVang',
                  image_singer: "#{NHACVANG}#{singer.children.first.attributes["src"].value}",
                  singer: singer.attributes["title"].value,
                  store: MusicCrawler::Models::Store.find_by(name: 'NhacVang')
                })
              end
            end
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class NhacVangProcessAtCatePage
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          image_singer = MusicCrawler::Models::Resource.find_by(url: url).try(:image_singer)
          singer_name = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)
          singer = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)
          (1..21).each do |page|
            detail_url =  url.gsub("-111/","-#{page}/")
            unless MusicCrawler::Models::Resource.where(url: detail_url).exists?
              MusicCrawler::Models::Resource.create({
                url: detail_url,
                task: 'NhacVangProcessGetAllLinkPaging',
                is_visited: false,
                site_name: 'NhacVang',
                singer: singer_name,
                image_singer: image_singer,
                store: MusicCrawler::Models::Store.find_by(name: 'NhacVang')
              })
            end
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class NhacVangProcessGetAllLinkPaging
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        begin
          image_singer = MusicCrawler::Models::Resource.find_by(url: url).try(:image_singer)
          singer_name = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)

          parse_musics = doc.xpath("//div[@class='list_song']/div[@class='c002']/div[@class='title']/a")
          if parse_musics.present?
            parse_musics.each do |music|
              detail_url = "#{NHACVANG}#{music.attributes["href"].value}"
              unless MusicCrawler::Models::Resource.where(url: detail_url).exists?
                MusicCrawler::Models::Resource.create({
                  url: detail_url,
                  task: 'NhacVangProcessGetAllLink',
                  is_visited: false,
                  site_name: 'NhacVang',
                  singer: singer_name,
                  image_singer: image_singer,
                  title: music.attributes["title"].value,
                  store: MusicCrawler::Models::Store.find_by(name: 'NhacVang')
                })
              end
            end
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          #change the link status
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end

    class NhacVangProcessGetAllLink
      def self.execute(url, body)
        doc = Nokogiri::HTML(body)
        title = MusicCrawler::Models::Resource.find_by(url: url).try(:title)
        singer_name = MusicCrawler::Models::Resource.find_by(url: url).try(:singer)
        image_singer = MusicCrawler::Models::Resource.find_by(url: url).try(:image_singer)

        begin
          item = MusicCrawler::Models::Item.new

          type_music = doc.xpath("//div[@class='song_detail']/h2/a").last.text().try(:strip)
          item.type_music = type_music.present? ? type_music : "Other"

          item.link_original = url
          item.site_id = 5
          item.images = image_singer
          # item.category_name = category_name
          item.title = title

          item.singer = singer_name

          item.lyrics = "Updating..."
          item.information = "Updating..."

          ### parse album vs country
          info_song = "Updating..."

          # parse album
          album = doc.xpath("//div[@class='song_detail']/h2/a").first.text().try(:strip)
          item.album = album

          parse_music = doc.css("script")
          if parse_music.present?
            get_content = ""
            parse_music.each do |script|
              if script.content.include?("'/song_")
                get_content = script.content
                break
              end
            end
            if get_content.present?
              response_link = get_content.scan(/\/song_\d*\.xml/).first
            end
            if response_link.present?
              response_link = "#{NHACVANG}#{response_link}"
              begin
                uri = URI(response_link)
                request = Net::HTTP::Get.new(uri.request_uri)
                response = Net::HTTP.get_response(uri)
                get_real_link = response.body.squish.scan(/\<\!\[CDATA\[http[0-9a-zA-Z|.|:|\/\/|\-|\_|\=]+/)
                get_real_link.each do |link|
                  if link.include?("mp3")
                    item.link = link.gsub("<![CDATA[","")
                  end
                end
              rescue => e
                puts e.inspect
                puts e.backtrace
              end
            end
          end
          if item.link_original.present? && item.title.present?
            puts "add data"
            item.save!
          end
        rescue => e
          puts e.inspect
          puts e.backtrace
        ensure
          MusicCrawler::Models::Resource.where(url: url).update_all(is_visited: true)
        end
      end
    end
  end
end
