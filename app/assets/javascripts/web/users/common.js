$(function() {
  $("#user_birthday_picker").datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+0",
    showWeek: true,
    showAnim: "fold"
  });
});
