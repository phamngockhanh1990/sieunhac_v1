var app = angular.module('myApp', []);
app.controller('personCtrl', function($scope) {
  $scope.firstName = "John";
  $scope.lastName = "Lincon";
  $scope.fullName = function() {
    return $scope.firstName + " " + $scope.lastName;
  }
});


var app1 = angular.module('myApp1', []);
app1.controller('nameCtrl', function($scope, $http) {
  $http.get("http://www.w3schools.com/angular/customers.php")
  .success(function(response) {$scope.names = response.records;});
});
