// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery
// require jquery_ujs
// require jquery-ui
// require tinymce
// require tinymce-jquery
// require angularjs/angular.min
// require angularjs/angular-resource.min
// require app
// require scroll_top
// require modernizr.custom
// require_tree ./users
// require_tree ./angularjs
// require plugins/bootstrap.min
// require plugins/flexslider/jquery.flexslider-min
// require plugins/horizontal-parallax/js/horizontal-parallax
// require plugins/horizontal-parallax/js/sequence.jquery-min
// require plugins/bxslider/jquery.bxslider
// require back-to-top
// require_tree .
