$(function() {
  $("img.lazy").lazy({
    enableThrottle: true,
    throttle: 250
  });
});

$(function() {
  var current_background = $(".booking-blocks").css("background-color");
  $(".booking-blocks").hover(
    function() {
      $(this).css("background", "pink");
    },
    function() {
      $(this).css("background", current_background);
    }
  );
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
