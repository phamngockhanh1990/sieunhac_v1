// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
// require all jquery libraries in vendor directory
//= require jquery-ui.min
//= require jquery.scrollbox
//= require jquery-2.1.4
//= require jquery.lazy.min
//= require common

//= require js/modernizr.custom
//= require plugins/bootstrap/js/bootstrap.min
//= require plugins/flexslider/jquery.flexslider-min
//= require plugins/parallax-slider/js/modernizr
//= require plugins/parallax-slider/js/jquery.cslider
//= require plugins/flexslider/jquery.flexslider-min
//= require plugins/bxslider/jquery.bxslider
//= require plugins/portfolioSorting/js/jquery.quicksand
//= require plugins/portfolioSorting/js/sorting
//= require js/app
//= require js/pages/index
//= require web/startup
//= require back-to-top

// Js for signin
// require web/users/common
//= require scroll_more
//= require turbolinks
