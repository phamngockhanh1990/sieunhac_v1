# coding: utf-8
class UserDecorator < Draper::Decorator
  delegate_all

  def full_name
    "#{first_name} #{last_name}"
  end

  def check_user_signin
    current_user ? "Welcome #{full_name}" : nil
  end
end
