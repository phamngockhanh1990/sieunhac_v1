class VideoDecorator < Draper::Decorator
  delegate_all

  def parse_media(link)
    doc = Base::Base.new.nokogiri(link)

    parse_music = doc.css("script")
    if parse_music.present?
      get_content = ""
      parse_music.each do |script|
        get_content = script.content if script.content.include?("clip.vegacdn.vn")
      end

      if get_content.present?
        response_link = get_content.scan(/clip\.vegacdn\.vn\/[0-9a-zA-Z|\/]*\.mp4/)
      end
      response_link.try(:first)
    end
  end
end
