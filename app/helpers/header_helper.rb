module HeaderHelper
  def ngoisao_header
    Rails.cache.fetch("cache_ngoisao_header") {
      CategoryNews.where(name: "Ngôi sao")
    }
  end

  def video_header
    Rails.cache.fetch("cache_video_header") {
      CategoryNews.where(name: "Video")
    }
  end

  def tre_header
    Rails.cache.fetch("cache_tre_header") {
      CategoryNews.where(name: "Trẻ")
    }
  end

  def lamdep_header
    Rails.cache.fetch("cache_lamdep_header") {
      CategoryNews.where(name: "Làm đẹp")
    }
  end

  def tamsu_header
    Rails.cache.fetch("cache_tamsu_header") {
      CategoryNews.find(34).slug
    }
  end

  def anchoi_header
    Rails.cache.fetch("cache_anchoi_header") {
      CategoryNews.find(33).slug
    }
  end

  def doisong_header
    Rails.cache.fetch("cache_doisong_header") {
      CategoryNews.find(24).slug
    }
  end

  def bepeva_header
    Rails.cache.fetch("cache_bepeva_header") {
      CategoryNews.where(name: "Bếp Eva")
    }
  end

  def truyen_header
    Rails.cache.fetch("cache_category_story_all") { CategoryStory.all }
  end
end
