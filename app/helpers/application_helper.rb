module ApplicationHelper
  include HeaderHelper

  def title_tag_content
    @title_tag || ""
  end

  def meta_robots
    @meta_robots || ""
  end

  def meta_description
    @meta_description || ""
  end

  def meta_author
    @meta_author || ""
  end

  def meta_keywords
    @meta_keywords || ""
  end

  def meta_title
    @meta_title || ""
  end


  ## Functions which truncate display title, song, lyrics, singer, artist
  ## Truncate song title
  def song_title(name, upcase_name = true)
    return nil unless name.present?
    name = name.capitalize
    upcase_name ? name.truncate(30) : name
  end

  ## Updated at song
  def song_updated(date)
  end

  ## Truncate type music
  def song_type_music(name, upcase_name = true)
    return nil unless name.present?
    name = name.capitalize
    upcase_name ? name.truncate(30) : name
  end

  ## Truncate lyrics
  def song_lyrics(name, upcase_name = true)
    return "Updating lyrics..." if name.blank?
    name = name.capitalize
    upcase_name ? raw(name.truncate(30)) : raw(name)
  end

  ## Truncate singer
  def song_singer(name, upcase_name = true)
    return nil unless name.present?
    name = name.capitalize
    upcase_name ? name.truncate(20) : name
  end

  ## get link original
  def get_link_original(music)
    OriginalLink.new(
      music["url"],
      music["url_original"],
      music["site_id"]
    ).parse_url_original
  end

  def format_date_time(date)
    date.strftime('%H:%M %d-%m-%Y')
  end

  def short_content_article(short_content)
    return nil unless short_content.present?
    short_content.truncate(160)
  end

  def clean_title(title)
    title.include?("[") ? title.gsub(/\[[\D|\d]*\]/, "").strip.squish : title
  end

  def hot_news?(updated_at)
    updated_at > Time.zone.now - 3.hours
  end

  ## devise user login
  def resource_class
    devise_mapping.to
  end
end
