class Admin::HomeController < Admin::BaseController
  layout "admin"
  before_action :authenticate_admin

  def index
    if params["condition_sort"] == "Time"
      @articles = Article.all.order(id: :desc).page(params[:page])
    else
      @articles = Article.all.page(params[:page])
    end
    respond_to do |format|
      format.js
      format.html
    end
  end
end
