class MusicsController < ApplicationController
  before_action :set_music, only: [:edit, :update, :destroy]

  def index
    @musics = Music.page(params[:page])
  end

  def edit; end

  def show
    @music = Rails.cache.fetch("cache_music_controller_show_#{params[:id]}", expires_in: 80.hours) do
      Music.detail(params[:id]).first
    end

    @music_recommend = Rails.cache.fetch("cache_music_controller_show_recommend_#{params[:id]}", expires_in: 3.hours) do
      Music.recommend(@music).sample(6)
    end
  end

  def new
    @music = Music.new
  end

  def create
    @music = Music.new(music_params)
    if @music.save
      redirect_to musics_path, notice: "Create a new music successfull"
    else
      render :new
    end
  end

  def update
    if @music.update(music_params)
    else
      render :edit
    end
  end

  def destroy
    @music.destroy
  end

  private
  def set_music
    @music = Music.find_by(params[:id])
  end

  def music_params(music_params=:music)
    params.require(music_params).permit(
      :title,
      :type_music,
      :format,
      :album,
      :singer,
      :lyrics,
      :country,
      :url,
      :url_original,
      :birthday_singer,
      :information,
      :quantity,
      :artist,
      :image,
      :category_id
    )
  end

  def rebuild_params
    music_params[:birthday_singer] = DateTime.strptime(music_params["birthday_singer"], '%m/%d/%Y')
  end
end

