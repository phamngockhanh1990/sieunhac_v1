class CategoryStoryController < ApplicationController
  before_action :set_category_story

  def show; end

  private
  def set_category_story
    @story_hots = Rails.cache.fetch("cache_category_story_hots_update", expires_in: 12.hours) {
      Story.where(hot: true).order(updated_at: :desc).limit(12)
    }
    @story_types = Rails.cache.fetch("cache_category_story_show_all_#{params[:id]}") {
     StoryType.where(category_story_id: params[:id])
    }
    @category_story = Rails.cache.fetch("cache_category_story_show_#{params[:id]}") {
      CategoryStory.friendly.find(params[:id])
    }
  end
end
