module Youtube::Controller
  extend ActiveSupport::Concern

  def start_cache(key_name, id, time)
    Rails.cache.fetch(key_name, expires_in: time.hours) do
      Video.where(category_id: id).order(updated_at: :desc)
    end
  end

  def read_cache(key_name, id, time)
    start_cache(key_name, id, time)
    Rails.cache.fetch(key_name)
  end

end
