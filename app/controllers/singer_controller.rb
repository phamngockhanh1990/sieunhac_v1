class SingerController < ApplicationController
  def index
  end

  def show
    @singer_musics = Rails.cache.fetch("cache_singer_musics_#{params[:id]}_#{params[:page]}", expires_in: 80.hours) do
      Music.singer_detail(params[:id], params[:page]).page(params[:page]).per(10)
    end
  end
end
