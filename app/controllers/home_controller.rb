class HomeController < ApplicationController
  before_action :set_default_music, only: [:contact]

  def index
    @unread_messages = nil
    @most_videos = Rails.cache.fetch("cache_youtube_top", expires_in: 6.hours) {
      Video.where(category_id: 12).order(updated_at: :desc).sample(4)
    }
    @saoviet = Rails.cache.fetch("cache_saoviet_top", expires_in: 2.hours) {
      CategoryNews.saoviet_top
    }
    @tamsu = Rails.cache.fetch("cache_tamsu_top", expires_in: 5.hours) {
      CategoryNews.tamsu
    }
    @doisong = Rails.cache.fetch("cache_doisong_top", expires_in: 4.hours) {
      CategoryNews.doisong
    }
    @truyenchu = Rails.cache.fetch("cache_truyenchu_top", expires_in: 64.hours) {
      StoryType.truyenchu
    }
    @bepeva = Rails.cache.fetch("cache_bepeva_top", expires_in: 6.hours) {
      CategoryNews.bepeva
    }
    @top_musics = Rails.cache.fetch("cache_musics_top", expires_in: 20.hours) { Music.top_musics_world }
  end

  def about; end

  def contact; end

  def privacy; end

  def list_api; end

  def confirm
    ContactMailer.send_contact(contact_params).deliver
    respond_to {|format| format.js}
  end

  private
  def contact_params
    params.permit(:name, :email, :content)
  end

  def set_default_music
    @music = Music.new
  end
end
