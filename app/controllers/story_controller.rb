class StoryController < ApplicationController
  before_action :set_story

  def show
    @chapters = @story.chapters.page(params[:page])
  end

  private
  def set_story
    @story = Rails.cache.fetch("cache_set_story_show_#{params[:id]}") {
      Story.friendly.find(params[:id])
    }

    @story_type = Rails.cache.fetch("cache_set_story_type_show_#{@story.story_type_id}", expires_in: 90.hours) {
      StoryType.find(@story.story_type_id)
    }

    @category_story = Rails.cache.fetch("cache_category_story_default", expires_in: 90.hours) {
      CategoryStory.find(@story_type.category_story_id)
    }

    @recommend_stories = Rails.cache.fetch("cache_recommend_stories_in_chapter_#{@story.story_type_id}") {
      StoryType.find(@story.story_type_id).stories.order(updated_at: :desc).limit(10).sample(8)
    }
  end
end
