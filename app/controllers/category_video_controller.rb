class CategoryVideoController < ApplicationController
  include Youtube::Controller

  def show
    result = read_cache("youtube_video_#{params[:id]}", params[:id], 20)
    @videos = Kaminari.paginate_array(result).page(params[:page]).per(12)
  end
end
