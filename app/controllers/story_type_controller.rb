class StoryTypeController < ApplicationController
  before_action :set_story_type
  def show
    @stories = @story_type.stories.page(params[:page]).per_page(10)
    respond_to do |format|
      format.html
      format.js
    end
  end

  private
  def set_story_type
    @story_type = Rails.cache.fetch("cache_set_story_type_show_#{params[:id]}", expires_in: 40.hours) {
      StoryType.friendly.find(params[:id])
    }

    @category_story = Rails.cache.fetch("cache_category_story_default", expires_in: 90.hours) {
      CategoryStory.find(@story_type.category_story_id)
    }
  end
end
