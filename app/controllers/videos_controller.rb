class VideosController < ApplicationController
  before_action :set_videos, only: [:show]

  def index; end

  def show
    @video_recommend = @video.recommend(@video.category_id, params[:id])
  end

  private
  def set_videos
    @video = Rails.cache.fetch("cache_video_controller_set_video_#{params[:id]}", expires_in: 80.hours) { Video.friendly.find(params[:id]) }
  end
end
