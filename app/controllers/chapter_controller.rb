class ChapterController < ApplicationController
  before_action :set_chapter, only: [:show, :choose_chapter]

  def show
  end

  def choose_chapter
  end

  private
  def set_chapter
    @story = Rails.cache.fetch("cache_set_chapter_show_stories_#{params[:story_id]}", expires_in: 90.hours) {
      Story.friendly.find(params[:story_id])
    }

    @story_type = Rails.cache.fetch("cache_set_story_type_show_#{@story.story_type_id}", expires_in: 90.hours) {
      StoryType.find(@story.story_type_id)
    }

    @category_story = Rails.cache.fetch("cache_category_story_default", expires_in: 90.hours) {
      CategoryStory.find(@story_type.category_story_id)
    }

    @chapter = Rails.cache.fetch("cache_set_chapter_show_#{params[:id]}", expires_in: 90.hours) {
      Chapter.friendly.find(params[:id])
    }

    @list_chapters = Rails.cache.fetch("cache_chapters_belongs_to_story_#{params[:story_id]}", expires_in: 90.hours) {
      Chapter.all_chapters(@chapter.story_id)
    }
  end
end
