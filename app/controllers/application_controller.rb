class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  helper_method :resource_name, :resource, :devise_mapping

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  # # custom 404
  # unless Rails.application.config.consider_all_requests_local
  #   rescue_from ActiveRecord::RecordNotFound,
  #               ActionController::RoutingError,
  #               ActionController::UnknownController,
  #               ActionController::UnknownAction,
  #               ActionController::MethodNotAllowed do |exception|
  #     # Put loggers here, if desired.
  #     redirect_to root_path
  #   end
  # end
  def authenticate_admin
    redirect_to root_path if current_user.admin == false
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(
      :first_name,
      :last_name,
      :nick_name,
      :birthday,
      :avatar,
      :email,
      :password
    )}
  end

end
