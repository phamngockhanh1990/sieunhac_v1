class CategoryNewsController < ApplicationController
  before_action :set_category_news, only: [:show]

  def show
    @articles = @category_news.articles.order(created_at: :desc).page(params[:page]).per_page(10)
    respond_to do |format|
      format.html
      format.js
    end
  end

  private
  def set_category_news
    @category_news = Rails.cache.fetch("cache_category_new_#{params[:id]}") {
      CategoryNews.friendly.find(params[:id])
    }
  end
end
