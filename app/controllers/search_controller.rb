class SearchController < ApplicationController
  before_action :set_search, only: [:index]
  before_action :search_new, only: [:index]

  def index
    @results = @new_music.search(set_search).page(params[:page])
  end

  private
  def set_search
    params.permit(:query).merge(page: params[:page])
  end

  def search_new
    @new_music = Music.new
  end
end
