class MessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :message_params, only: [:create, :update]
  before_action :set_message, only: [:edit, :destroy, :show, :update]
  before_action :all_messages, only: [:index]

  def index; end

  def new
    @message = Message.new
  end

  def create
    @message = Message.new(message_params)
    if @message.save
      redirect_to messages_path, notice: "Create it successfull"
    else
      render 'new'
    end
  end

  def edit; end

  def update
  end

  def show
    @message.watched = true
    @message.save
  end

  def destroy
  end

  private

  def message_params
    params.require(:message).permit(:title, :content).merge(user_id: current_user.id)
  end

  def set_message
    @message = Message.find_by(id: params[:id])
  end

  def all_messages
    @messages = Message.page(params[:page]).per(10)
  end
end

