class ArticlesController < ApplicationController
  layout 'admin', only: [:edit]
  before_action :set_article, only: [:show]
  before_action :authenticate_admin, only: [:edit, :update]

  def show
    @article_recommend = @article.recommend(@article.category_news_id, params[:id])
  end

  def edit
    @article = Article.friendly.find(params["id"])
    respond_to do |format|
      format.js
    end
  end

  def update
    binding.pry
  end

  private
  def set_article
    @article = Rails.cache.fetch("cache_article_controller_show_#{params[:id]}") do
      Article.friendly.find(params[:id])
    end

    @category_news = Rails.cache.fetch("cache_category_new_#{@article.category_news_id}") {
      CategoryNews.friendly.find(@article.category_news_id)
    }
  end
end
