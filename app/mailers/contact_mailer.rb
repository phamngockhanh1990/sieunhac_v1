class ContactMailer < ApplicationMailer
  default from: 'sieunhac@gmail.com'

  def send_contact(user)
    @user = user
    @url = "http://www.sieunhac.com"
    mail(to: @user[:email], subject: "Contact")
  end
end
