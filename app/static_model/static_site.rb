class StaticSite < StaticModel::Base
  set_data_file "#{Rails.root}/db/statics/site.yml"
end
