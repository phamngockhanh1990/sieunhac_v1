class StaticCountry < StaticModel::Base
  set_data_file "#{Rails.root}/db/statics/countries.yml"
end
