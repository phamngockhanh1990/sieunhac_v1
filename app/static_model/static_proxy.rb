class StaticProxy < StaticModel::Base
  set_data_file "#{Rails.root}/db/statics/proxy.yml"

  def self.crawl_new_proxy
    uri = URI.parse("http://spys.ru/free-proxy-list/VN/")
    http = Net::HTTP.new(uri.host, uri.port)

    request = Net::HTTP::Post.new("/free-proxy-list/VN/")
    request.set_form_data({xpp: 3, xf1: 0, xf2: 2, xf4: 1})
    response = http.request(request)

    page = Nokogiri::HTML(response.body)
    list_ip = page.xpath(".//table/tr[position()>3]/td[1]/font[2]").map{|c| c.children.first.text()+ ":3128"}
    list = []

    page.xpath('.//table/tr[position()>3]/td[7]/font[1]/table').each_with_index do |element, index|
      if element['width'].to_i > 17
        list << list_ip[index]
      end
    end

    list.each do |proxy|
      obj_proxy = Proxy.find_by(proxy: proxy)
      next if obj_proxy
      Proxy.create(proxy: proxy)
    end

    # File.open("#{Rails.root}/db/statics/proxy.yml", "w") do |file|
    #   list.each_with_index do |ip, index|
    #     line = "- id: #{index}\n  proxy: #{ip}" + "\n"
    #     file.write(line)
    #   end
    # end
  end
end