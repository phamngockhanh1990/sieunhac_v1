class OriginalLink
  attr_accessor :link, :url_original, :site_id, :log

  def initialize(link, url_original, site_id)
    @link            = link
    @url_original    = url_original
    @site_id         = site_id
    @log             = Logger.new('log/original_link', 'daily')
  end

  def initialize_mechanize(url)
    agent = Mechanize.new
    page = agent.get(url)
    uri = page.try(:uri)
    host_name = uri.try(:host)
    path = uri.try(:path)
    "http://#{host_name}#{path}"
  end

  def parse_link
    body, status = check_link_exist
    status ? Nokogiri::HTML(body) : nil
  end

  def check_link_exist
    status = true
    begin
      body = open(@url_original, :read_timeout => 10)
    rescue => e
      status = false
      body = nil
    end
    return body, status
  end

  def parse_url_original
    return @link if check_link?
    return nhacso(url_original) if site_id.to_i == 4
    doc = parse_link
    parse_music = doc.css("script")
    link = ""
    if parse_music.present?
      link = nhaccuatui(parse_music)      if site_id.to_i == 0
      link = zing(parse_music)            if site_id.to_i == 1
      link = lansongxanh(doc)             if site_id.to_i == 2
      link = chiasenhac(doc)              if site_id.to_i == 3
      link = nhacvang(parse_music)        if site_id.to_i == 5
    end
    link
  end

  def check_link?
    return false unless @link.present?
    @link = URI::encode(@link)
    response = Faraday.head @link
    response.status == 200 ? true : false
  end

  def nhaccuatui(parse_music)
    get_content = ""
    parse_music.each do |script|
      get_content = script.content if script.content.include?("http://www.nhaccuatui.com/flash/xml?key1")
    end

    if get_content.present?
      response_link = get_content.scan(/http\:\/\/www\.nhaccuatui\.com\/flash\/xml\?key1\=[0-9a-zA-Z]+/)[0]
    end

    if response_link.present?
      begin
        response = request_uri_link(response_link)
        get_real_link = response.body.squish.scan(/\<\!\[CDATA\[[0-9a-zA-Z|.|:|\/\/|\-|\_]+/)
        get_real_link.each do |link|
          return link.gsub("<![CDATA[","") if link.include?("mp3")
        end
      rescue => e
        log.error("Nhac cua tui #{Time.now} #{e.inspect} #{e.backtrace}")
        puts e.inspect
        puts e.backtrace
      end
    end
  end

  def zing(parse_music)
    get_content = ""
    parse_music.each do |script|
      if script.content.include?("http://mp3.zing.vn/xml/song-xml/")
        get_content = script.content
        break
      end
    end
    if get_content.present?
      response_link = get_content.scan(/http\:\/\/mp3\.zing\.vn\/xml\/song\-xml\/[a-zA-Z]+/).try(:first)
    end
    if response_link.present?
      begin
        response = request_uri_link(response_link)
        get_real_link = response.body.squish.scan(/\<\!\[CDATA\[http[0-9a-zA-Z|.|:|\/\/|\-|\_|\=]+/)
        arr_link = []
        get_real_link.each do |link|
          if link.include?("load-song")
            return initialize_mechanize(link.gsub("<![CDATA[",""))
          end
        end
        ""
      rescue => e
        log.error("Zing #{Time.now} #{e.inspect} #{e.backtrace}")
        puts e.inspect
        puts e.backtrace
      end
    end
  end

  def nhacso(url_original)
    LanSongXanh.new.nhacso(url_original)
  end

  def chiasenhac(doc)
    parse_music = doc.css("noscript")
    get_content = ""
    parse_music.first.children.children.each do |script|
      if script.attributes.present? && script.attributes["src"].present? && script.attributes["src"].value.present? && script.attributes["src"].value.include?("http://chiasenhac.com/player/audio-player")
        return script.attributes["src"].value
        break
      end
    end
    ""
  end

  def lansongxanh(doc)
    LanSongXanh.new.lansongxanh(doc)
  end

  def nhacvang(doc)
    get_content = ""
    parse_music.each do |script|
      if script.content.include?("'/song_")
        get_content = script.content
        break
      end
    end
    if get_content.present?
      response_link = get_content.scan(/\/song_\d*\.xml/).first
    end
    if response_link.present?
      response_link = "#{NHACVANG}#{response_link}"
      begin
        response = request_uri_link(response_link)
        get_real_link = response.body.squish.scan(/\<\!\[CDATA\[http[0-9a-zA-Z|.|:|\/\/|\-|\_|\=]+/)
        get_real_link.each do |link|
          if link.include?("mp3")
            return link.gsub("<![CDATA[","")
          end
        end
        ""
      rescue => e
        log.error("Nhac vang #{Time.now} #{e.inspect} #{e.backtrace}")
        puts e.inspect
        puts e.backtrace
      end
    end
  end

  private
  def request_uri_link(response_link)
    uri = URI(response_link)
    request = Net::HTTP::Get.new(uri.request_uri)
    Net::HTTP.get_response(uri)
  end
end
