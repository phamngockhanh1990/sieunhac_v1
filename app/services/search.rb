require 'rsolr-ext'
class Search
  attr_accessor :conditions, :offset, :pg, :per_page, :solr_connection

  def initialize(queries: nil, pg: pg, start: nil, sort: nil)
    @queries = queries
    @start = start
    @pg = pg || 1
    @sort = sort
    @solr_connection = RSolr::Ext.connect(
        url: Settings.solr.connection.url,
        open_timeout: Settings.solr.connection.open_timeout,
        read_timeout: Settings.solr.connection.read_timeout,
        retry_503: Settings.solr.connection.retry_503)
  end

  def execute
    solr_connection.find(start: start, row: 10, :queries => main_query, :sort => sort)
  end

  def response(cache = true)
    @response ||= execute
  end

  def sort
    "#{@sort} desc"
  end

  def results
    response['response']['docs']
  end

  def total_hits
    response['response']['numFound']
  end

  def start
    if @start
      @start
    elsif @pg
      (@pg.to_i - 1) * per_page
    else
      fail 'start param not found.'
    end
  end

  def per_page
    10
  end

  def main_query
    @queries.presence || '*:*'
  end

  def filter_query; end

  ## query detail of music
  def detail
    results
  end

  # for kaminari
  def paginater
    paginater ||= Kaminari.paginate_array(
      Array.new(results),
      total_count: total_hits
    ).page(pg).per(per_page)
  end

end
