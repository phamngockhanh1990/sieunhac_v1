class FacebookPost
  attr_accessor :graph

  def initialize
    @graph = Koala::Facebook::API.new(Settings.facebook.page_token)
  end

  def post_wall
    graph.put_wall_post("test post comment on facebook automatically")
  end

  def profile
    graph.get_object("me")
  end

  def friends
    graph.get_connections("me", "friends")
  end
end

