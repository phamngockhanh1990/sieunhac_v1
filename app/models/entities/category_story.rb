class CategoryStory < ActiveRecord::Base
  extend FriendlyId
  include Common
  friendly_id :name, use: :slugged
  before_save :remove_accent
  paginates_per 20
  has_many :story_types
end
