class Article < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  paginates_per 10
  belongs_to :category_new
  before_save :remove_accent

  def self.newest_article(technology, sport, political)
    [
      Article.where(category_news_id: technology).order(created_at: :desc).first,
      Article.where(category_news_id: sport).order(created_at: :desc).first,
      Article.where(category_news_id: political).order(created_at: :desc).first
    ]
  end

  def self.newest_education(education)
    Article.where(category_news_id: education).order(created_at: :desc).take(10)
  end

  def recommend(category_news_id, id)
    Rails.cache.fetch("cache_article_model_recommend_#{category_news_id}_#{id}", expires_in: 6.hours) do
      Article.where(category_news_id: category_news_id).limit(20).order(updated_at: :desc).sample(16)
    end
  end

  def recommend_vides_others(id)
    Rails.cache.fetch("cache_article_model_recommend_videos_others_id#{id}", expires_in: 3.hours) do
      Video.where(category_id: 1).order(updated_at: :desc).limit(12).sample(6)
    end
  end

  def remove_accent
    source = self.title
    begin
      return source if source.blank?
      source = source.downcase
      source = source.strip
      unicode = {
        'a'=>/[á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ]/,
        'd'=>/[đ|Đ]/,
        'e'=>/[é|è|é|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ]/,
        'i'=>/[í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị]/,
        'o'=>/[ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ]/,
        'u'=>/[ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự]/,
        'y'=>/[ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ]/,
        '-'=>/ /
       }
      unicode.each do |nonUni, uni|
        source = source.gsub(uni, nonUni)
      end
      source = source.gsub('/','-')
      source = source.gsub(',','-')
      source = source.gsub(')','-')
      source = source.gsub('(','-')
      source = source.gsub('+','-')
      source = source.gsub('.','-')
      source = source.gsub('…','-')
      source = source.gsub('---','-')
      source = source.gsub('--','-')
      source = source.gsub('\'','')
      source = source.gsub('̉','')
      source = source.gsub('́','')
      source = source.gsub('̀','')
    rescue Exception => e
    end
    self.slug = source.gsub(/[<>~!@#$%^&*()_+|}{';:\'`,?."“‘’” ]/,'') + "-#{rand(9999..99999)}"
  end

end
