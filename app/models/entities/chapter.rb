class Chapter < ActiveRecord::Base
  extend FriendlyId
  include Common
  friendly_id :title, use: :slugged
  before_save :build_name_url

  belongs_to :story

  validates :title, presence: true

  ### get all infomations of story
  def self.all_chapters(story_id)
    where(story_id: story_id).pluck(:title, :slug)
  end
end
