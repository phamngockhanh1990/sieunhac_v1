class Message < ActiveRecord::Base
  belongs_to :user

  validates :title,
    presence: {
      message: "Tiêu đề không được để trống."
    },
    length: {
      maximum: 40
    }

   validates :content,
    presence: {
      message: "Nội dung không được để trống."
    },
    length: {
      minimum: 10,
      message: "Nội dung phải chứa tối đa 10 ký tự trở lên."
    }

  def self.count_messages_unwatched(user_id)
    where(watched: false, user_id: user_id).size
  end
end
