class CategoryNews < ActiveRecord::Base
  extend FriendlyId
  include Common
  friendly_id :sub_category, use: :slugged
  before_save :build_name_category

  has_many :articles

  def self.saoviet_top
    CategoryNews.find(25).articles.order(created_at: :desc).limit(10).sample(4)
  end

  def self.tamsu
    CategoryNews.find(34).articles.order(created_at: :desc).limit(10).sample(4)
  end

  def self.doisong
    CategoryNews.find(24).articles.order(created_at: :desc).limit(8).sample(4)
  end

  def self.bepeva
    CategoryNews.find_by_name("Bếp Eva").articles.order(created_at: :desc).limit(8).sample(4)
  end
end
