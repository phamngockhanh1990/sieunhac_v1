class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  mount_uploader :avatar, AvatarUploader

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]


  has_many :messages, dependent: :destroy

  validates :first_name,
    presence: { message: "Không đuợc để trống" },
    length: { maximum: 100 }

  validates :last_name,
    presence: { message: "Không đuợc để trống" },
    length: { maximum: 100 }
  validates :nick_name,
    presence: { message: "Không đuợc để trống" },
    length: { maximum: 100 }
  validates :email,
    presence: {
      message: "Vui lòng nhập địa chỉ e-mail của bạn."
    },
    format: {
      message: "Vui lòng nhập địa chỉ e-mail thích hợp.",
      with: Settings.regexp.email,
      allow_nil: true, allow_blank: true
    },
    length: {
      maximum: 100,
      message: "Địa chỉ e-mail quá dài.",
      allow_nil: true, allow_blank: true
    },
    uniqueness: {
      message: "Địa chỉ e-mail đã tồn tại.",
      allow_nil: true, allow_blank: true
    }
  validates :password,
    presence: {
      message: "Vui lòng nhập mật khẩu của bạn."
    },
    format: {
      with: /\A[a-zA-Z0-9]{6,20}\z/,
      message: "Mật khẩu là không đúng.",
      allow_nil: true, allow_blank: true
    },
    confirmation: {
      message: 'Mật khẩu xác nhận không khớp.'
    },
    length: {
      minimum: 8,
      message: "Mật khẩu quá ngắn."
    }

  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(:email => data["email"]).first

    unless user
      user = User.create(name: data["name"],
                         email: data["email"],
                         password: Devise.friendly_token[0,20]
                        )
    end
    user
  end
end
