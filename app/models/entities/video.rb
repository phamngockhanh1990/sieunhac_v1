class Video < ActiveRecord::Base
  extend FriendlyId
  include Common
  friendly_id :name, use: :slugged
  before_save :remove_accent
  paginates_per 12
  validates :name, presence: true

  def recommend(category_id, id)
    Rails.cache.fetch("cache_video_model_recommend_#{category_id}_#{id}", expires_in: 8.hours) {
      Video.where(category_id: category_id).order(updated_at: :desc).limit(10).sample(6)
    }
  end
end
