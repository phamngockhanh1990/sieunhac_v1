class Music < ActiveRecord::Base
  paginates_per 10

  mount_uploader :image, MusicUploader
  belongs_to :category
  belongs_to :singer

  validates :title,
    presence: { message: "Không đuợc để trống" },
    length: { maximum: 100 }

  validates :type_music,
    presence: { message: "Không đuợc để trống" },
    length: { maximum: 100 }

  validates :format,
    presence: { message: "Không đuợc để trống" },
    length: { maximum: 100 }

  validates :singer_name,
    presence: { message: "Không đuợc để trống" },
    length: { maximum: 100 }

  validates :url_original,
    presence: { message: "Không đuợc để trống" },
    length: { minimum: 10 }

  validates :site_id,
    presence: { message: "Không đuợc để trống" },
    length: { minimum: 1 }

  def search(params)
    Search.new(queries: execute_query(params), pg: params[:page], start: nil, sort: "updated_at").paginater
  end

  def self.detail(id)
    Search.new(queries: "id: #{id}", pg: nil, start: nil, sort: "updated_at").detail
  end

  def self.lansongxanh
    Search.new(queries: "site_id: 2", pg: nil, start: nil, sort: "updated_at").detail
  end

  def self.recommend(music)
    Search.new(queries: "singer_name: #{music['singer_name']}", pg: nil, start: nil, sort: "updated_at").detail
  end

  def self.singer_detail(id, page)
    Search.new(queries: "singer_id: #{id}", pg: page, start: nil, sort: "updated_at").paginater
  end

  def self.top_musics_world
    [
      Search.new(queries: "nhachot: true", pg: nil, start: nil, sort: "updated_at").detail,
      Search.new(queries: "music_top: true", pg: nil, start: nil, sort: "updated_at").detail,
      Search.new(queries: "lsxanh: true", pg: nil, start: nil, sort: "updated_at").detail,
      Search.new(queries: "site_id: 5", pg: nil, start: nil, sort: "updated_at").detail
    ]
  end

  private
  def filter_query(query)
    query.gsub(/[\#|\!|\@|\%|\^|\$|\~|\`|\&|\*|\(|\)|\-|\_|\=|\+|\]|\[|\{|\}|\"|\'|\;|\:|\?|\/|\>|\.|\<|\,|\||\\]/, "")
  end

  def execute_query(params)
    query = filter_query(params[:query]).try(:to_s).try(:strip).try(:squish) if params[:query].try(:strip).try(:squish).present?
    query.present? ? build_query(query) : "*:*"
  end

  def build_query(query)
    "title:(#{query}) OR singer_name:(#{query})"
  end

end
