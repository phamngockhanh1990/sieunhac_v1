class Story < ActiveRecord::Base
  extend FriendlyId
  include Common
  friendly_id :name, use: :slugged
  before_save :remove_accent

  belongs_to :category_story
  has_many :chapters, dependent: :destroy

  validates :name, presence: true
end
