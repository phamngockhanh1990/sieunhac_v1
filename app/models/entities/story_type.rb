class StoryType < ActiveRecord::Base
  extend FriendlyId
  include Common
  friendly_id :title, use: :slugged
  before_save :build_name_url

  belongs_to :category_story
  has_many :stories

  validates :title, presence: true

  def self.truyenchu
    find(54).stories.order(created_at: :desc).limit(10).sample(4)
  end
end
