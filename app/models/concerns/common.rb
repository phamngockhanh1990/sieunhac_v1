module Common
  extend ActiveSupport::Concern
  included do
    def remove_accent
      source = self.try(:name)
      source = remove_unicode_characters(source)
      add_random_key(source)
    end

    def build_name_url
      source = self.try(:title)
      source = remove_unicode_characters(source)
      add_random_key(source)
    end

    def build_name_category
      source = self.try(:sub_category)
      source = remove_unicode_characters(source)
      remove_special_characters(source)
    end

    def remove_unicode_characters(source)
      begin
        return source if source.blank?
        source = source.downcase
        source = source.strip
        unicode = {
          'a'=>/[á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ]/,
          'd'=>/[đ|Đ]/,
          'e'=>/[é|è|é|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ]/,
          'i'=>/[í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị]/,
          'o'=>/[ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ]/,
          'u'=>/[ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự]/,
          'y'=>/[ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ]/,
          '-'=>/ /
        }
        unicode.each do |nonUni, uni|
          source = source.gsub(uni, nonUni)
        end
        source = source.gsub('/','-')
        source = source.gsub(',','-')
        source = source.gsub(')','-')
        source = source.gsub('(','-')
        source = source.gsub('+','-')
        source = source.gsub('.','-')
        source = source.gsub('…','-')
        source = source.gsub('---','-')
        source = source.gsub('--','-')
        source = source.gsub('\'','')
        source = source.gsub('̉','')
        source = source.gsub('́','')
        source = source.gsub('̀','')
      rescue Exception => e
      end
      source
    end

    def add_random_key(source)
      self.slug = source.gsub(/[<>~!@#$%^&*()_+|}{';:\'`,?."“‘’” ]/,'') + "-#{rand(9999..99999)}"
    end

    def remove_special_characters(source)
      self.slug = source.gsub(/[<>~!@#$%^&*()_+|}{';:\'`,?."“‘’” ]/,'')
    end
  end
end
