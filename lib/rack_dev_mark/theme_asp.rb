require 'rack/dev-mark/theme/base'

module RackDevMark
  class ThemeASP < Rack::DevMark::Theme::Base
    def insert_into(html, env, params = {})
      bgColor = @options[:bgColor] || '#f00'
      color = @options[:color] || '#fff'

      part = <<-EOS
<style>
#rack_dev_mark_asp_wrap {
  opacity: 0.75;
  -webkit-transition: opacity 0.5s ease-out;
  -moz-transition: opacity 0.5s ease-out;
  -ms-transition: opacity 0.5s ease-out;
  transition: opacity 0.5s ease-out;
}
#rack_dev_mark_asp_wrap:hover {
  opacity: 0.45;
}
.rack_dev_mark_asp {
  position: fixed;
  background-color: #{bgColor};
  color: #{color};
  text-align: center;
  font-size: 22px;
  font-weight: bold;
  text-shadow: none;
}
#rack_dev_mark_asp_t {
  left: 0;
  top: 0;
  width: 100%;
  height: 40px;
  line-height: 40px;
  overflow: hidden;
}
#rack_dev_mark_asp_b {
  left: 0;
  bottom: 0;
  width: 100%;
  height: 40px;
  line-height: 40px;
}
#rack_dev_mark_asp_l {
  left: 0;
  top: 0;
  width: 40px;
  height: 100%;
  margin: 40px 0;
  display: none;
}
#rack_dev_mark_asp_r {
  right: 0;
  top: 0;
  width: 40px;
  height: 100%;
  margin: 40px 0;
  display: none;
}
</style>
<div id="rack_dev_mark_asp_wrap">
  <div class="rack_dev_mark_asp" id="rack_dev_mark_asp_t"></div>
  <div class="rack_dev_mark_asp" id="rack_dev_mark_asp_l"></div>
  <div class="rack_dev_mark_asp" id="rack_dev_mark_asp_r"></div>
  <div class="rack_dev_mark_asp" id="rack_dev_mark_asp_b">#{env.capitalize}</div>
</div>
<script>
  $(function(){
    var $top = $('#rack_dev_mark_asp_t');
    var $bottom = $('#rack_dev_mark_asp_b');
    var $left = $('#rack_dev_mark_asp_l');
    var $right = $('#rack_dev_mark_asp_r');

    $top.add($bottom).add($left).add($right).on('click', function(){
      var speed = 50;

      $top.animate({top: -$top.outerHeight()}, speed, function(){
        // window.setTimeout(function(){
        //   $top.animate({top: 0}, 5000);
        //   $bottom.animate({bottom: 0}, 5000);
        //   $left.animate({left: 0, marginTop: 40, marginBottom: 40}, 5000);
        //   $right.animate({right: 0, marginTop: 40, marginBottom: 40}, 5000);
        // }, 60000);
      });

      $bottom.animate({bottom: -$bottom.outerHeight()}, speed);
      $left.animate({left: -$left.outerWidth(), marginTop: 0, marginBottom: 0}, speed);
      $right.animate({right: -$right.outerWidth(), marginTop: 0, marginBottom: 0}, speed);
    });
  });
</script>
      EOS

      html.sub %r{(</body[^>]*>)}i, "#{part}\\1"
    end
  end
end
