require 'sites/music/lansongxanh'
require 'sites/music/zing_singer'
require 'sites/music/billboard'
require 'sites/music/nhacso'

namespace :crawler do
  desc "Crawl video from youtube"
  task :music => :environment do
    LanSongXanh.new.start_crawler_music
  end

  desc "Crawler image and name singer"
  task :singer => :environment do
    ActiveRecord::Base.connection.execute("TRUNCATE singers")
    FileUtils.rm_rf(Dir.glob("#{Rails.root}/public/images/musics/singers/vietnam/*.jpg"))
    ZingSinger.new.start_crawler_music
  end

  desc "Crawler music hot and top vietnam [nhachot]"
  task :viet => :environment do
    NhacSo.new.start_crawler_music
  end

  desc "Crawler music hot and top au my [music_top]"
  task :us => :environment do
  end
end
