namespace :remove do
  desc "Remove production log when it large than 6 megabytes"
  task :logs => :environment do
    if Rails.env.development?
      MAX_LOG_SIZE = 6.megabytes
      logs = [
        File.join(Rails.root, 'current', 'log', 'production.log'),
        File.join(Rails.root, 'current', 'log', 'cron_log.log'),
        File.join(Rails.root, 'current', 'log', 'crawl_import.log')
      ]

      logs.each do |log|
        log_size = File.size?(log).to_i
        if log_size > MAX_LOG_SIZE
          $stdout.puts "Removing Log: #{log}"
          `rm #{log}`
        end
      end
    end
  end
end
