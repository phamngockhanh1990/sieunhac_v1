file_path = File.dirname(__FILE__).gsub(/lib\/tasks/, "")
Dir[File.expand_path(file_path) + '/lib/sites/story/picture/*.rb'].each { |file| require file }
Dir[File.expand_path(file_path) + '/lib/sites/story/category/*.rb'].each { |file| require file }
namespace :crawler do
  desc "Crawl story from these famous website story"
  task :story => :environment do
    Site::Vechai.new.start_crawl_news if ENV["site_id"].to_i == 0

    Site::BlogTruyen.new.start_crawl_news if ENV["site_id"].to_i == 1

    Site::Truyen24.new.start_crawl_news if ENV["site_id"].to_i == 2

    Site::ComicVN.new.start_crawler_story if ENV["site_id"].to_i == 3

    Site::ThichTruyenTranh.new.start_crawler_story if ENV["site_id"].to_i == 4

    ### Crawler novel
    Site::TruyenYY.new.start_crawler_story if ENV["site_id"].to_i == 5
    Site::WebTruyen.new.start_crawler_story if ENV["site_id"].to_i == 6

    ### Crawler audio story
    Site::RadioTruyen.new.start_crawler_story if ENV["site_id"].to_i == 7
  end


  desc "Update category story"
  task :cat_novel => :environment do
    Site::CatWebTruyen.new.start_crawl_category
  end
end
