require 'sites/clipvn'

namespace :crawler do
  desc "Crawl video from youtube"
  multitask :videos => [:crawl_proxy] do
    Site::ClipVN.new.start_crawl
  end

  task :crawl_proxy => :environment do
    StaticProxy.crawl_new_proxy
  end
end
