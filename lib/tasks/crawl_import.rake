# encoding: utf-8
require 'mongoid'
require 'csv'
require 'logger'
require 'pry'

file_path = File.dirname(__FILE__).gsub(/lib\/tasks/, "")
Dir[File.expand_path(file_path) + '/music_crawler/models/mongoid/*.rb'].each {|file| require file }
Mongoid.load!(File.expand_path(file_path) + '/music_crawler/crawler/config/mongoid.yml', :production)

namespace :crawler do
  log = Logger.new('log/crawl_import.log', 'daily')
  desc "Import items from mongodb"
  multitask :import => [:import_categories] do
    begin
      start_time = Time.now
      MusicCrawler::Models::Store.where(site_id: ENV['site_id'].to_i).update_all(importing: true)
      count = MusicCrawler::Models::Item.where(site_id: ENV['site_id'].to_i, added_or_updated: true).count
      log.info("Will be importing #{count} musics")
      MusicCrawler::Models::Item.where(site_id: ENV['site_id'].to_i, added_or_updated: true).no_timeout.each do |record|
          CrawlImport::Import.new.import_record(record, log)
      end
      MusicCrawler::Models::Store.where(site_id: ENV['site_id'].to_i).update_all(importing: false)
      duration = Time.now - start_time
      log.info("Imported #{count} musics in " + duration.to_s)
    rescue => e
      log.error("fail to import")
      log.error(e.inspect)
    ensure
      MusicCrawler::Models::Store.where(site_id: ENV['site_id'].to_i).update_all(importing: false)
    end
  end

  task :import_categories => :environment do
    log.info("#{Time.now} start import categories to mysql")
    begin
      if ENV['category']
        ActiveRecord::Base.connection.execute("TRUNCATE categories")
        categories = StaticCategory.all
        categories.each do |category|
          obj_category = Category.new
          obj_category.name = category.name
          obj_category.type = category.country
          obj_category.save!
          puts "Importing data #{category.name} to category...."
        end
      else
        puts "#{Time.now} ignore import categories"
      end
    rescue Exception => e
      log.error e.message
      log.error e.backtrace.inspect
    end
  end
end

module CrawlImport
  class Import
    def set_default_name(record=nil, name=nil)
      record.try(:strip).try(:squish).present? ? record : name
    end

    def import_record(record, log)
      begin
        puts "Start importing data from mongo to mysql....."
        existing_music = Music.find_by(url_original: record['link_original'])

        if existing_music.nil?
          music = Music.new
          music.url = record['link']
          music.url_original = record['link_original']
          music.site_id = record['site_id'].to_i
          puts "#{Time.now} Start create new record in database #{music.url}"
        else
          music = existing_music
          puts "#{Time.now} Update database record in database #{music.url}"
        end

        music.new_imported = true
        if record["singer"].include?("Nghệ sĩ")
          record["singer"] = record["singer"].gsub("Nghệ sĩ", "").strip.squish
        end
        music.singer_name = record["singer"]
        if record['title'].include?("-")
          music.title = record['title'].split("-").first.squish
          music.singer_name = record['title'].split("-").last.squish unless music.singer_name.present?
        else
          music.title = record['title'].squish
          music.singer_name = record['singer'].try(:strip).try(:parameterize) unless music.singer_name.present?
        end

        music.type_music = set_default_name(record['type_music'], "Other")
        music.format = set_default_name(record['format'], "mp3")
        music.lyrics = record['lyrics']
        music.count_listen = record['count_listen'].to_i
        music.site_id = record['site_id'].to_i
        music.quantity = record['quanlity']
        music.country = record['country']
        music.information = record['information']

        music.album = record['album']
        music.birthday_singer = record['birthday_singer']

        music.category_id = category(record['category_name'])

        if music.singer_name.present?
          obj_singer = singer(music.singer_name)
          music.singer_id = obj_singer.id
          music.singer_image = obj_singer.image_singer
        else
          music.singer_id = 99999
          music.singer_name = "Other"
        end

        if music.singer_name.present? && music.url_original.present?
          music.save!
          puts "Save data #{music.singer_name}"
        end
      rescue => e
        puts e.inspect
        puts e.backtrace
        log.error("fail to import music")
        log.error(e.inspect)
      end
    end

    def import_record_category(record, log)
      return if record['category_name'].blank?
      begin
        existing_category = Category.find_by(name: record['category_name'].try(:strip))
        if existing_category.nil?
          category = Category.new
        else
          category = existing_category
        end
        category.name = record['category_name']
        category.save!

        if existing_category.nil?
          log.info("category id #{category.id} added")
          puts "category id #{category.id} added"
        else
          log.info("category id #{category.id} updated")
          puts "category id #{category.id} updated"
        end

        record['added_or_updated'] = false
        record.save!
      rescue => e
        puts e.inspect
        puts e.backtrace
        log.error("fail to import category")
        log.error(e.inspect)
      end
    end

    private

    def category(category_name)
      return 39 unless category_name.present?
      category_name = category_name.try(:strip).try(:downcase)
      list_categories = Category.pluck(:name, :id)
      list_categories.map { |name, id|
        return id if name.downcase.strip.include?(category_name) || category_name.include?(name.downcase.strip)
      }
      39
    end

    def singer(singer_name)
      singer_name = singer_name.strip.squish
      obj_singer = Singer.find_by(name: singer_name)
      return obj_singer if obj_singer.present?
      obj_singer =  Singer.new
      obj_singer.name = singer_name
      obj_singer.image_singer = "musics/singers/default.jpg"
      obj_singer.save!
      obj_singer
    end

    def image(image_name)
    end
  end
end
