require 'sites/video_youtube'

namespace :crawler do
  desc "Crawl video from youtube"
  multitask :youtube => [:setup_youtube] do
    current_year = Time.now.year
    @videos_initial = Yt::Collections::Videos.new
    keywords = [
      { keyword: "son tung mtp", id: 12 },
      { keyword: "Kenvin Khánh", id: 5 },
      { keyword: "khoa học", id: 7 },
      { keyword: "solider us", id: 8 }
    ]
    keywords.each do |keyword|
      VideoYoutube.new.search_videos(keyword, @videos_initial)
    end
  end

  task :setup_youtube => :environment do
    # Video.where(Video.arel_table[:updated_at].lt(8.hours.ago))
  end
end
