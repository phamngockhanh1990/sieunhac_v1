file_path = File.dirname(__FILE__).gsub(/lib\/tasks/, "")
Dir[File.expand_path(file_path) + '/lib/sites/girl/*.rb'].each { |file| require file }
Dir[File.expand_path(file_path) + '/lib/sites/news/*.rb'].each { |file| require file }
Dir[File.expand_path(file_path) + '/lib/sites/*.rb'].each { |file| require file }

namespace :crawler do
  desc "Crawl news from these famous website news"
  multitask :news => [:initialize_video] do
    ### thoi su - Finish
    Site::VnExpress.new.start_crawl_news if ENV["site_id"].to_i == 0

    # Site::Zing.new.start_crawl_news if ENV["site_id"].to_i == 1

    ### Education - Finish
    Site::KenhTuyenSinh.new.start_crawl_news if ENV["site_id"].to_i == 2

    ### Technology
    Site::TinhTe.new.start_crawl_news if ENV["site_id"].to_i == 3

    ### Siencetist - Finish
    Site::KhamPha.new.start_crawl_news if ENV["site_id"].to_i == 4

    ### Technology - Finish
    Site::Genk.new.start_crawl_news if ENV["site_id"].to_i == 5

    ### Sports - Finish
    Site::TinTheThao.new.start_crawl_news if ENV["site_id"].to_i == 6

    ### Family - love - Finish
    Site::AFamily.new.start_crawl_news if ENV["site_id"].to_i == 7
    Site::SucKhoeDoiSong.new.start_crawl_news if ENV["site_id"].to_i == 9

    ### Finish
    Site::GiaoDuc.new.start_crawl_news if ENV["site_id"].to_i == 8

    ### The gioi
    Site::TheGioi.new.start_crawl_news if ENV["site_id"].to_i == 10

    ### Phap Luat
    Site::PhapLuat.new.start_crawl_news if ENV["site_id"].to_i == 11

    ### Anh girl xinh
    Site::XemAnh.new.start_crawl_news if ENV["site_id"].to_i == 12
    Site::Wn.new.start_crawl_news if ENV["site_id"].to_i == 13

    ### Teen Ao dai
    Site::AoDai.new.start_crawl_news if ENV["site_id"].to_i == 14

    Site::GirlDep.new.start_crawl_news if ENV["site_id"].to_i == 15
    Site::KenhSao.new.start_crawl_news if ENV["site_id"].to_i == 16
    Site::NgoiSao.new.start_crawl_news if ENV["site_id"].to_i == 17

    #### Special Category
    Site::YanNews.new.start_crawl_star_news if ENV["site_id"].to_i == 20
    Site::NgoiSaoNet.new.start_crawl_star_news if ENV["site_id"].to_i == 21
    Site::BlogTamSu.new.start_crawl_star_news if ENV["site_id"].to_i == 22
    Site::Zing.new.start_crawl_star_news if ENV["site_id"].to_i == 23
    # Site::ShowBizVN.new.start_crawl_star_news if ENV["site_id"].to_i == 24
    # Site::SaoStar.new.start_crawl_star_news if ENV["site_id"].to_i == 25



    #### Cong thuc nau an
    Site::BepEva.new.start_crawl_star_news if ENV["site_id"].to_i == 30

  end

  task :initialize_video => :environment do
  end

  desc "Update category news"
  task :category_news => :environment do
    ActiveRecord::Base.connection.execute("truncate category_news")
    StaticCategoryNews.all.each do |category|
      CategoryNews.create(name: category.name)
    end
  end

  desc "Tạo category cho mục ngôi sao---------------------------------------"
  task :category_star => :environment do
    [
      {name: "Video", sub_category: "Showbiz", url: "http://www.yan.vn/chuyen-muc-video/showbiz-360-4.html", id: 20},
      {name: "Video", sub_category: "S show", url: "http://www.yan.vn/chuyen-muc-video/yan-show-5.html", id: 21},
      {name: "Video", sub_category: "Kênh siêu nhạc", url: "http://www.yan.vn/chuyen-muc-video/mchannel-6.html", id: 22},
      {name: "Video", sub_category: "Cẩm nang giải trí", url: "http://www.yan.vn/chuyen-muc-video/cam-nang-giai-tri-7.html", id: 23},
      {name: "Đời sống", sub_category: "Đời sống", url: "http://www.yan.vn/c/tin-doi-6.html", id: 24},
      {name: "Ngôi sao", sub_category: "Sao việt", url: "http://www.yan.vn/chuyen-muc-sao/viet-nam-1.html", id: 25},
      {name: "Ngôi sao", sub_category: "Sao châu á", url: "http://www.yan.vn/chuyen-muc-sao/chau-a-2.html", id: 26},
      {name: "Ngôi sao", sub_category: "Sao âu châu", url: "http://www.yan.vn/chuyen-muc-sao/au-my-3.html", id: 27},
      {name: "Trẻ", sub_category: "Học đường", url: "http://www.yan.vn/chuyen-muc-tre/hoc-duong-21.html", id: 28},
      {name: "Trẻ", sub_category: "Tình yêu", url: "http://www.yan.vn/chuyen-muc-tre/yeu-22.html", id: 29},
      {name: "Trẻ", sub_category: "Cư dân mạng", url: "http://www.yan.vn/chuyen-muc-tre/cong-dong-mang-23.html", id: 30},
      {name: "Làm đẹp", sub_category: "Thời trang", url: "http://www.yan.vn/chuyen-muc-dep/thoi-trang-27.html", id: 31},
      {name: "Làm đẹp", sub_category: "Trang điểm", url: "http://www.yan.vn/chuyen-muc-dep/trang-diem-28.html", id: 32},
      {name: "Ăn chơi", sub_category: "Ăn chơi", url: "http://www.yan.vn/c/tin-choi-8.html", id: 33},
      {name: "Tâm sự", sub_category: "Tâm sự", url: "http://www.yan.vn/c/tin-mlog-1.html", id: 34},
      {name: "Bếp Eva", sub_category: "Món ngon mỗi ngày", url: "http://bepeva.com/tag/mon-ngon-moi-ngay/page/1/", id: 40},
      {name: "Bếp Eva", sub_category: "Món ngon cuối tuần", url: "http://bepeva.com/tag/mon-ngon-cuoi-tuan/page/1/", id: 41},
      {name: "Bếp Eva", sub_category: "Món ngon dễ làm", url: "http://bepeva.com/tag/mon-ngon-de-lam/page/1/", id: 42},
      {name: "Bếp Eva", sub_category: "Tối nay ăn gì", url: "http://bepeva.com/tag/toi-nay-an-gi/page/1/", id: 43},
      {name: "Bếp Eva", sub_category: "Mẹo hay", url: "http://bepeva.com/meo-hay-nha-bep/page/1/", id: 44},
    ].each do |item|
      obj_category_news = CategoryNews.find_by(url: item[:url])
      if obj_category_news
        obj_category_news.update(item)
        puts "#{Time.now} start update category news #{item[:name]}"
      else
        CategoryNews.create(item)
        puts "#{Time.now} start create category news #{item[:name]}"
      end
    end
  end
end
