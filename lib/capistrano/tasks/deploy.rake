namespace :deploy do
  desc "Create gemset"
  task :create_gemset do
    on roles(:web) do
      # execute "source /usr/local/rvm/scripts/rvm"
      # execute "/bin/bash --login"
      # execute "rvm use #{fetch(:rvm_ruby_version)} --create"
    end
  end

  # namespace :restart do
    # task :unicorn do
      # on roles(:all) do
        # execute "/etc/init.d/unicorn_sieunhac stop"
        # sleep(10)
        # execute "/etc/init.d/unicorn_sieunhac restart"
      # end
    # end
  # end

  namespace :assets do
    task :clear do
      on roles(:all) do
        within release_path do
          execute :rake, "assets:clean RAILS_ENV=production"
        end
      end
    end
  end

  namespace :clear do
    task :cache do
      on roles(:all) do
         within release_path do
           execute :rake, "tmp:cache:clear RAILS_ENV=production"
         end
      end
    end
  end

  task :restart do
    invoke 'unicorn:restart'
  end
end
