# encoding: utf-8
require 'base/base'
module Site
  class GiaoDuc < Base::Base
    GIAODUC = "http://giaoduc.net.vn/Giao-duc-24h/5/trang1.gd"

    private
    def parse_list_news
      (1..10).map{|page|
        GIAODUC.gsub("trang1","trang#{page}")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='listing']//p[@class='thumb']//a")
      lists.each do |item|
        arr_links << { link: item["href"], image: item.child.next["src"], title: item.child.next["alt"] }
      end
      arr_links
    end

    def parse_news_detail(item)
      link = "http://giaoduc.net.vn#{item[:link]}"
      doc = nokogiri(link)
      begin
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 5
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      short_content = doc.xpath("//p[@class='summary']").first
      long_content = doc.xpath("//div[@itemprop='articleBody']")
      "#{short_content}\n\t#{long_content}"
    end

    def get_short_content(doc)
      doc.xpath("//p[@class='summary']").first.text()
    end
  end
end
