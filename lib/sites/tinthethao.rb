# encoding: utf-8
require 'base/base'
module Site
  class TinTheThao < Base::Base
    TINTHETHAO = "http://www.bongda.com.vn/tin-moi-nhat/page/1/"

    private
    def parse_list_news
      (1..10).map{|page|
        TINTHETHAO.gsub("page/1/","page/#{page}/")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='panel widget widget_siteorigin-panels-postloop panel-first-child panel-last-child category-listing']//article//a")
      lists.each do |item|
        arr_links << { link: item["href"], image: item.child["src"], title: "" }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:title]            = get_title(doc)
        item[:content]          = get_content(doc)
        item[:short_content]    = get_short_content(doc)
        item[:category_news_id] = 3
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      short_content = doc.xpath("//div[@id='divfirst']/p").first
      long_content = doc.xpath("//div[@class='entry-content']/node()[not(@class='post-source')]")
      "#{short_content}\n\t#{long_content}"
    end

    def get_title(doc)
      doc.xpath("//h1[@class='entry-title']").text()
    end

    def get_short_content(doc)
      doc.xpath("//div[@id='divfirst']/p").first.text()
    end
  end
end
