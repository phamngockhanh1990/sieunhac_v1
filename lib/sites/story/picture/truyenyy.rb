# encoding: utf-8
require 'base/base'
module Site
  class TruyenYY < Base::Base
    attr_accessor :base_url, :story_types

    def initialize
      @base_url = "http://truyenyy.com"
      @story_types = StoryType.where(category_story_id: 2)
    end

    def start_crawler_story
      # ### test----------------------------
      # link = "http://thichtruyentranh.com/green-worldz/green-worldz-chap-1/8649.html"
      # info = {title: "test"}
      # obj_story = Story.first
      # parse_chapter(obj_story, info, link)
      # ####--------------------------------
      story_types.each_with_index do |type, index|
        puts "#{Time.now} #{type.title}"
        paging_stories(type)
      end
    end

    private

    def paging_stories(type)
      doc = nokogiri(type.link)
      paging = doc.xpath("//div[@class='pagination pagination-centered']//a")
      number_paging = 1
      if paging.present? && paging.text().present?
        number_paging = paging[paging.size - 2]["href"].split("page=").last.to_i
      end
      (1..number_paging).each do |page|
        link = type.link.gsub("page=1", "page=#{page}")
        puts "#{Time.now} Paging: #{link}"
        link_stories(link, type)
      end
    end

    #### parse get list stories link
    def link_stories(link, type)
      doc = nokogiri(link)
      lists = doc.xpath("//table[@class='table table-hover']//a[@href]")
      lists.each_with_index do |detail, index|
        puts "#{Time.now} #{detail.text().try(:strip)}"
        info = {
          link: "#{base_url}#{detail['href']}",
          short_content: "",
          name: detail.text().try(:strip),
          image: "/images/musics/default.jpg",
          content: "",
          hot: "",
          memo: "",
          type_story: ""
        }
        create_story(info, type)
      end
    end

    def create_story(info, type)
      doc = nokogiri(info[:link])
      image = doc.xpath("//div[@class='thumbnail']//img/@src")
      short_content =  doc.xpath("//div[@id='desc_story']").text().try(:strip).try(:squish)
      info[:image] = "#{base_url}#{image.text}" if image.present?
      info[:short_content] = short_content if short_content.present?
      begin
        obj_story = type.stories.find_by(link: info[:link])
        obj_story = obj_story.present? ? obj_story : type.stories.create(info)
        puts "#{Time.now} stories #{obj_story.id}"
        paging_chapter(obj_story, doc)
      rescue => e
        puts e.inspect
        puts e.backtrace
      end
    end

    ### get number paging chapters
    def paging_chapter(obj_story, doc)
      ### parse paging number chapters
      paging = doc.xpath("//div[@class='pagination pagination-centered']//a")
      number_paging = 1
      if paging.present?
        number_paging = paging[paging.size - 2]["href"].split("page=").last.to_i
      end

      ### list chapters link
      (1..number_paging).each do |page|
        link = "#{obj_story.link}?page=#{page}"
        puts "#{Time.now} Paging chapters: #{link}"
        link_chapters(obj_story, link)
      end
    end

    ### lists all link of chapters
    def link_chapters(obj_story, link)
      doc = nokogiri(link)
      list_chapters = doc.xpath("//div[@class='chaplist']//a[@href]")
      list_chapters.each do |chapter|
        info = {
          title: chapter.text().try(:strip).try(:squish),
          recognize: 1
        }
        link = chapter['href']
        puts "#{Time.now} Chapter number: #{link}"
        parse_chapter(obj_story, info, link)
      end
    end

    ### import images to chapter model
    ### @params: obj_story: object of model story
    ### @params: info: Information of chapter include title
    ### @return: Return chapters belongs to a story
    def parse_chapter(obj_story, info, link)
      doc = nokogiri(link)
      info[:content] = doc.xpath("//div[@class='text-truyen']").to_html
      info[:content] = info[:content].gsub("style=", "") if info[:content].include?("style=")
      info[:link] = link
      result_story = obj_story.chapters.find_by(link: info[:link])
      if info[:content].present?
        if result_story.present?
          result_story.update(info)
          puts "#{Time.now} Update info for stories"
        else
          obj_story.chapters.create(info)
          puts "#{Time.now} Create chapters for stories"
        end
      end
    end
  end
end
