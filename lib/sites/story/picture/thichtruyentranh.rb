# encoding: utf-8
require 'base/base'
module Site
  class ThichTruyenTranh < Base::Base
    attr_accessor :base_url, :story_types

    def initialize
      @base_url = "http://thichtruyentranh.com"
      @story_types = StoryType.where(category_story_id: 1)
    end

    def start_crawler_story
      # ### test----------------------------
      # link = "http://thichtruyentranh.com/green-worldz/green-worldz-chap-1/8649.html"
      # info = {title: "test"}
      # obj_story = Story.first
      # parse_chapter(obj_story, info, link)
      # ####--------------------------------
      story_types.each_with_index do |type, index|
        puts "#{Time.now} #{type.title}"
        paging_stories(type)
      end
    end

    private

    def paging_stories(type)
      doc = nokogiri(type.link)
      paging = doc.xpath("//div[@class='paging']//a").last
      number_paging = 1
      if paging.present?
        number_paging = paging["href"].split("trang.").last.gsub(".html","").to_i
      end
      (1..number_paging).each do |page|
        link = type.link.gsub(".html", "/trang.#{page}.html")
        puts "#{Time.now} Paging: #{link}"
        link_stories(link, type)
      end
    end

    def link_stories(link, type)
      doc = nokogiri(link)
      lists = doc.xpath("//ul[@class='ulListruyen']//div[@class='divthumb']/a")
      lists.each_with_index do |detail, index|
        puts "#{Time.now} #{detail["title"]}"
        info = {
          link: "#{base_url}#{detail['href']}",
          short_content: "",
          name: detail["title"],
          image: "#{base_url}#{detail.child['src']}",
          content: "",
          hot: "",
          memo: "",
          type_story: ""
        }
        create_story(info, type)
      end
    end

    def create_story(info, type)
      doc = nokogiri(info[:link])
      short_content = doc.xpath("//ul[@class='ulpro_line']/li/p")
      info[:short_content] = short_content.text().try(:strip).try(:squish) if short_content.present?
      begin
        obj_story = type.stories.find_by(link: info[:link])
        obj_story = obj_story.present? ? obj_story : type.stories.create(info)
        puts "#{Time.now} stories #{obj_story.id}"
        paging_chapter(obj_story, doc)
      rescue => e
        puts e.inspect
        puts e.backtrace
      end
    end

    def paging_chapter(obj_story, doc)
      ### parse paging number chapters
      paging = doc.xpath("//div[@class='paging']//a").last
      number_paging = 1
      if paging.present?
        number_paging = paging["href"].split("trang.").last.gsub(".html","").to_i
      end

      ### list chapters link
      (1..number_paging).each do |page|
        link = obj_story.link.gsub(".html", "/trang.#{page}.html")
        puts "#{Time.now} Paging chapters: #{link}"
        link_chapters(obj_story, link)
      end
    end

    def link_chapters(obj_story, link)
      doc = nokogiri(link)
      list_chapters = doc.xpath("//div[@id='listChapterBlock']//ul[@class='ul_listchap']//a[@href]")
      list_chapters.each do |chapter|
        info = {
          title: chapter["title"],
          recognize: 0
        }
        link = "#{base_url}#{chapter['href']}"
        puts "#{Time.now} Chapter number: #{link}"
        parse_chapter(obj_story, info, link)
      end
    end

    ### import images to chapter model
    ### @params: obj_story: object of model story
    ### @params: info: Information of chapter include title
    ### @return: Return chapters belongs to a story
    def parse_chapter(obj_story, info, link)
      doc = nokogiri(link)
      searching = doc.css("script")
      searching.each do |result|
        if result.content.include?("blogspot.com")
          info[:image] = result.content.scan(/http\:\/\/[a-zA-Z0-9|.|\/|\-|\_]*jpg/).join(",")
          info[:link] = link
          break
        end
      end
      if info[:image].present?
        result_story = obj_story.chapters.find_by(link: info[:link])
        if result_story.present?
          result_story.update(info)
          puts "#{Time.now} Update info for stories"
        else
          obj_story.chapters.create(info)
          puts "#{Time.now} Create chapters for stories"
        end
      end
    end
  end
end
