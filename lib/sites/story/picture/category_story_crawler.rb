# encoding: utf-8
require 'base/base'
module Site
  class CategoryStoryCrawler < Base::Base
    attr_accessor :base_url
    CATEGORYSTORY = "http://vechai.info/the-loai/Giong-game.50.html"

    def initialize
      @base_url = "http://vechai.info"
    end

    def start_crawl_category
      doc = nokogiri(CATEGORYSTORY)
      import_to_database(doc)
    end

    private

    def import_to_database(doc)
      list_cats = doc.xpath("//ul[@id='cateList']//a[@href]")
      list_cats.each do |item|
        obj_category = CategoryStory.new
        obj_category.name = item["title"]
        obj_category.image = item["href"]
        obj_category.save!
        puts "Save category #{item["title"]}"
      end
    end
  end
end
