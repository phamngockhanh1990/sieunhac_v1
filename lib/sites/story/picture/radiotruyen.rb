# encoding: utf-8
require 'base/base'
module Site
  class RadioTruyen < Base::Base
    attr_accessor :base_url, :ajax_url, :story_types

    def initialize
      @base_url = "http://www.radiotruyen.com/"
      @ajax_url = "http://webtruyen.com/home/pagingtabmain/1/1"
      @story_types = StoryType.where(category_story_id: 3)
    end

    def start_crawler_story
      story_types.each_with_index do |type, index|
        puts "#{Time.now} #{type.title}"
        paging_stories(type)
      end
    end

    private
    ### get number paging of this site
    def paging_stories(type)
      doc = nokogiri(type.link)
      paging = doc.at_xpath("//li[@class='pagination-end']//a")
      number_paging = 1
      if paging.present? && paging["href"].present?
        number_paging = paging["href"].split("start=").last.to_i
        number_paging = number_paging / 50
      end
      (1..number_paging).each do |page|
        link = "#{type.link}?start=#{page * 50}"
        puts "#{Time.now} Paging: #{link}"
        link_stories(link, type)
      end
    end

    #### parse get list stories link
    def link_stories(link, type)
      doc = nokogiri(link)
      binding.pry
      lists = doc.xpath("//ul[@class='list_ct']//div[@class='list_img']/a")
      lists.each_with_index do |detail, index|
        puts "#{Time.now} #{detail.text().try(:strip)}"
        info = {
          link: detail['href'],
          short_content: "",
          name: detail.child["alt"],
          image: detail["src"],
          content: "",
          hot: "",
          memo: "",
          type_story: ""
        }
        create_story(info, type)
      end
    end

    def create_story(info, type)
      doc = nokogiri(info[:link])
      image = doc.xpath("//div[@class='lineleftcont']//img[@class='imgdetail']/@src")
      short_content =  doc.xpath("//div[@id='detail-mota']").text().try(:strip).try(:squish)
      info[:image] = image.text if image.present?
      info[:short_content] = short_content if short_content.present?
      begin
        obj_story = type.stories.find_by(link: info[:link])
        if obj_story.present?
          obj_story.update(info)
        else
          obj_story = type.stories.create(info)
        end
        puts "#{Time.now} stories #{obj_story.id}"
        paging_chapter(obj_story, doc)
      rescue => e
        puts e.inspect
        puts e.backtrace
      end
    end

    ### get number paging chapters
    def paging_chapter(obj_story, doc)
      ### parse paging number chapters
      paging = doc.xpath("//span[@class='numbpage']").text().try(:strip).try(:squish)
      number_paging = 1
      if paging.present? && paging.include?("/")
        number_paging = paging.split("/").last.try(:strip).try(:to_i)
      end

      ### list chapters link
      (1..number_paging).each do |page|
        link = "#{obj_story.link}#{page}"
        puts "#{Time.now} Paging chapters: #{link}"
        link_chapters(obj_story, link)
      end
    end

    ### lists all link of chapters
    def link_chapters(obj_story, link)
      doc = nokogiri(link)
      list_chapters = doc.xpath("//div[@class='ullistchap']//a[@href]")
      list_chapters.each do |chapter|
        info = {
          title: chapter["title"].try(:strip).try(:squish),
          link: chapter["href"],
          recognize: 1
        }
        link = chapter['href']
        puts "#{Time.now} Chapter number: #{link}"
        parse_chapter(obj_story, info, link)
      end
    end

    ### import images to chapter model
    ### @params: obj_story: object of model story
    ### @params: info: Information of chapter include title
    ### @return: Return chapters belongs to a story
    def parse_chapter(obj_story, info, link)
      doc = nokogiri(link)
      author = doc.xpath("//h2[@class='detailauthor']").text().try(:strip).try(:squish)
      title = doc.xpath("//h1[@class='detailtitle']").text().try(:strip).try(:squish)
      chapter_info = doc.xpath("//h3[@class='detailchapter']").text().try(:strip).try(:squish)
      info[:content] = doc.xpath("//div[@id='detailcontent']")
      info[:content] = filter_content(info[:content])
      info[:link] = link
      info[:content] = before_save_content(info[:content])
      result_story = obj_story.chapters.find_by(link: info[:link])
      if info[:content].present?
        info[:content] = "<h2>#{title}</h2><p><h3>#{author} | #{chapter_info}</h3></p>\n\t#{info[:content]}"
        if result_story.present?
          result_story.update(info)
          puts "#{Time.now} Update info for stories"
        else
          obj_story.chapters.create(info)
          puts "#{Time.now} Create chapters for stories"
        end
      end
    end

    private
    def filter_content(content)
      content.search("//div[@class='setfontsize']").try(:remove)
    end
  end
end
