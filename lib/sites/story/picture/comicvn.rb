# encoding: utf-8
require 'base/base'
module Site
  class ComicVN < Base::Base
    attr_accessor :base_url, :story_types

    def initialize
      @base_url = "http://comicvn.net"
      @story_types = StoryType.all
    end

    def start_crawler_story
      binding.pry
      obj_story = Story.first
      doc = nokogiri(obj_story.short_content)
      paging_chapter(obj_story, doc)
      # story_types.each_with_index do |type, index|
      #   puts "#{Time.now} #{type.title}"
      #   paging_stories(type)
      # end
    end

    private

    def paging_stories(type)
      # [
      #   "A","B","C","D","E","F","G","H",
      #   "I","J","K","L","M","N","O","P",
      #   "Q","R","S","T","U","V","W","X",
      #   "Y","Z"
      # ].map {|character| "#{type.link}##{character}"}
      #  .each do |link|
      #     puts "#{Time.now} #{link}"
          link_stories(type.link, type)
       # end
    end

    def link_stories(link, type)
      doc = nokogiri(link)
      lists = doc.xpath("//ul[@id='category']//div[@class='thumnbail']")
      lists.each_with_index do |detail, index|
        puts "#{Time.now} #{detail.child.next["title"]}"
        info = {
          short_content: "#{base_url}#{detail.child.next['href']}",
          name: detail.child.next["title"],
          image: detail.child.next.child["data-original"],
          content: "",
          hot: "",
          memo: "",
          type_story: ""
        }
        create_story(info, type)
      end
    end

    def create_story(info, type)
      doc = nokogiri(info[:short_content])
      memo = doc.xpath("//div[@class='detail-content']/p")
      info[:content] = memo.text().try(:strip).try(:squish) if memo.present?
      begin
        obj_story = type.stories.create(info)
        puts "#{Time.now} create new stories #{obj_story.id}"
        paging_chapter(obj_story, doc)
      rescue => e
        puts e.inspect
        puts e.backtrace
      end
    end

    def paging_chapter(obj_story, doc)
      ### list chapters link
      lists = doc.xpath("//div[@class='list-chapter-content']/table//a[@href]")
      lists.each_with_index do |link, index|
        puts "#{Time.now} #{link['href']}"
        parse_chapter(obj_story, "#{base_url}#{link["href"]}")
      end
    end

    def parse_chapter(obj_story, link)
      binding.pry
      doc = nokogiri(link)
      images = doc.xpath("//div[@id='list_images']//img")
      images.each do |image|
        binding.pry
      end
    end
  end
end
