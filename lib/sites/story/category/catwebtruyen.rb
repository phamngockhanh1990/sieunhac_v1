# encoding: utf-8
require 'base/base'
module Site
  class CatWebTruyen < Base::Base
    attr_accessor :base_url, :obj_category_story
    WEBTRUYEN = "http://webtruyen.com/"

    def initialize
      @base_url = "http://webtruyen.com/"
    end

    def start_crawl_category
      @obj_category_story = CategoryStory.find(2)
      doc = nokogiri(WEBTRUYEN)
      import_to_database(doc)
    end

    private

    def import_to_database(doc)
      list_cats = doc.xpath("//ul[@class='menu_theloai']//a[@href]")
      list_cats.each do |item|
        info = {
          title: item.text().try(:strip).try(:squish),
          image: "/image/stories/story_2.jpg",
          link: item["href"],
          description: ajax_paging(item.text().try(:strip).try(:squish))
        }
        obj_type_story = obj_category_story.story_types.find_by(link: info[:link])
        if obj_type_story.present?
          obj_type_story.update(info)
          puts "Update category #{info[:title]}"
        else
          obj_category_story.story_types.create(info)
          puts "Create new category #{info[:title]}"
        end
      end
    end

    def ajax_paging(item)
      case item
      when "Tiên Hiệp"
        return "1"
      when "Kiếm Hiệp"
        return "2"
      when "Ngôn Tình"
        return "13"
      when "Truyện Teen"
        return "14"
      when "Đô Thị"
        return "4"
      when "Quân sự"
        return "8"
      when "Lịch sử"
        return "6"
      when "Xuyên Không"
        return "18"
      when "Truyện ma"
        return "19"
      when "Trinh thám"
        return "20"
      when "Huyền Huyễn"
        return "11"
      when "Khoa Huyễn"
        return "12"
      when "Dị Giới"
        return "10"
      when "Võng Du"
        return "9"
      else
        return nil
      end
    end
  end
end
