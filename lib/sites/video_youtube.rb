# encoding: utf-8
require 'base/base'

class VideoYoutube < Base::Base
  def search_videos(keyword=nil, videos_initial=nil)
    videos = videos_initial.where(q: keyword[:keyword], safe_search: 'none')
    puts "Keyword: #{keyword[:keyword]}"
    videos_greater_than_1000(videos, keyword[:id])
  end

  def videos_greater_than_1000(videos, id)
    videos.each.with_index(0) do |video, index|
      begin
        save_item_youtube(video, id) if video.view_count > 1000 && video.hd?
        puts "loop #{index}"
        break if index == 200
      rescue => e
        puts e
      end
    end
  end
end
