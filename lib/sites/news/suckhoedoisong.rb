# encoding: utf-8
require 'base/base'
module Site
  class SucKhoeDoiSong < Base::Base
    attr_accessor :base_url
    SUCKHOEDOISONG = "http://suckhoedoisong.vn/me-va-be/trang-1.htm"

    def initialize
      @base_url = "http://suckhoedoisong.vn"
    end

    private
    def parse_list_news
      (1..6).map{|page|
        SUCKHOEDOISONG.gsub("trang-1","trang-#{page}")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='list-category w660 clearfix mb10']//a[@class='img']")
      lists.each do |item|
        arr_links << { link: "#{base_url}#{item['href']}", image: item.child.next["src"], title: item["title"] }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 10
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      short_content = doc.xpath("//h2[@class='news-sapo']")
      long_content = doc.xpath("//div[@class='content-news clear']")
      "#{short_content}\n\t#{long_content}"
    end

    def get_short_content(doc)
      doc.xpath("//h2[@class='news-sapo']").text()
    end
  end
end
