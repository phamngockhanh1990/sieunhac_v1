# encoding: utf-8
require 'base/base'
module Site
  class TheGioi < Base::Base
    attr_accessor :base_url
    THEGIOI = "http://baodatviet.vn/the-gioi/?paged=1"

    def initialize
      @base_url = "http://baodatviet.vn"
    end

    private
    def parse_list_news
      (1..20).map{|page|
        THEGIOI.gsub("paged=1","paged=#{page}")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//ul[@class='list_top list_folder']//a[@class='thumb']")
      lists.each do |item|
        image = item.child.next["src"]
        arr_links << {
          link: "#{base_url}#{item['href']}",
          image: image.gsub("w=119","w=418&h=319"),
          title: item["title"].try(:squish).try(:strip)
        }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 1
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      short_content = doc.xpath("//h2[@class='lead']")
      long_content = doc.xpath("//div[@class='detail']/p")
      "#{short_content}\n\t#{long_content}"
    end

    def get_short_content(doc)
      doc.xpath("//h2[@class='lead']").text()
    end
  end
end
