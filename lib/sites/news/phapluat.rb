# encoding: utf-8
require 'base/base'
module Site
  class PhapLuat < Base::Base
    attr_accessor :base_url
    PHAPLUAT = "http://www.doisongphapluat.com/phap-luat/page/1"

    def initialize
      @base_url = "http://www.doisongphapluat.com"
    end

    private
    def parse_list_news
      (1..20).map{|page|
        PHAPLUAT.gsub("page/1","page/#{page}")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//ul[@class='module-vertical-list']//div[@class='info pkg']//a[@class='img']")
      lists.each do |item|
        arr_links << { link: item['href'], image: item.child["src"], title: item.child["alt"].try(:squish).try(:strip) }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 4
        save_articles(item)
      rescue => e
        puts e.inspect
        puts e.backtrace
      end
    end

    def get_content(doc)
      doc.xpath("//div[@id='main-detail']")
    end

    def get_short_content(doc)
      doc.xpath("//div[@id='main-detail']/p").first.text()
    end
  end
end
