# encoding: utf-8
require 'base/base'
module Site
  class AFamily < Base::Base
    attr_accessor :base_url
    AFAMILY = "http://afamily.vn/tinh-yeu-hon-nhan/trang-1.chn"

    def initialize
      @base_url = "http://afamily.vn"
    end

    private
    def parse_list_news
      (1..3).map{|page|
        sleep(4)
        AFAMILY.gsub("trang-1","trang-#{page}")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='list-news1']//a[@class='image-x']")
      lists.each do |item|
        arr_links << { link: "#{base_url}#{item['href']}", image: item.child["src"], title: item.child["alt"].try(:squish).try(:strip) }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      sleep(4)
      begin
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 10
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      short_content = doc.xpath("//div[@class='detailmain fl']//h2")
      long_content = doc.xpath("//div[@class='detailmain fl']//div[@class='detail_content fl mgt15']")
      "#{short_content}\n\t#{long_content}"
    end

    def get_short_content(doc)
      doc.xpath("//div[@class='detailmain fl']//h2").text()
    end
  end
end
