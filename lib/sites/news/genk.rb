# encoding: utf-8
require 'base/base'
module Site
  class Genk < Base::Base
    attr_accessor :base_url
    GENK = "http://genk.vn/home/page-1.chn"

    def initialize
      @base_url = "http://genk.vn"
    end

    private
    def parse_list_news
      (1..20).map{|page|
        GENK.gsub("page-1","page-#{page}")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='list-news-img fl mr30 mt10']//a")
      lists.each do |item|
        arr_links << { link: "#{base_url}#{item['href']}", image: item.child["src"], title: item["title"].try(:squish).try(:strip) }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 11
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      short_content = doc.xpath("//h2[@class='init_content oh']")
      long_content = doc.xpath("//div[@id='ContentDetail']")
      "#{short_content}\n\t#{long_content}"
    end

    def get_short_content(doc)
      doc.xpath("//h2[@class='init_content oh']").text()
    end
  end
end
