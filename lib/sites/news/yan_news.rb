# encoding: utf-8
require 'base/base'
module Site
  class YanNews < Base::Base
    attr_accessor :base_url, :logger

    def initialize
      @base_url = "http://www.yan.vn"
      @logger = Logger.new('log/crawl_yannews.log', 'daily')
    end

    def start_crawl_star_news
      CategoryNews.all.reverse.each do |item|
        transfer = []
        link_original = map_url_ajax(item.url, item.sub_category)
        next unless link_original.present?
        parse_list_news(link_original).each do |link|
          transfer << list_links(link)
        end
        transfer.flatten.compact.reverse.each do |detail|
          parse_news_detail(detail, item)
        end
      end
    end

    private

    def map_url_ajax(url, sub_category)
      # return "http://www.yan.vn/Video/LoadMoreData/4?Page=1" if sub_category.include?("Showbiz")
      # return "http://www.yan.vn/Video/LoadMoreData/5?Page=1" if sub_category.include?("S show")
      # return "http://www.yan.vn/Video/LoadMoreData/6?Page=1" if sub_category.include?("Kênh siêu nhạc")
      # return "http://www.yan.vn/Video/LoadMoreData/7?Page=1" if sub_category.include?("Cẩm nang giải trí")
      return "http://www.yan.vn/Doi/LoadMoreData/6?Page=1" if sub_category.include?("Đời sống")
      return "http://www.yan.vn/Celeb/LoadMoreData/1?Page=1" if sub_category.include?("Sao việt")
      return "http://www.yan.vn/Celeb/LoadMoreData/2?Page=1" if sub_category.include?("Sao châu á")
      return "http://www.yan.vn/Celeb/LoadMoreData/3?Page=1" if sub_category.include?("Sao âu châu")
      return "http://www.yan.vn/Tre/LoadMoreData/21?Page=1" if sub_category.include?("Học đường")
      return "http://www.yan.vn/Tre/LoadMoreData/22?Page=1" if sub_category.include?("Tình yêu")
      return "http://www.yan.vn/Tre/LoadMoreData/23?Page=1" if sub_category.include?("Cư dân mạng")
      return "http://www.yan.vn/Dep/LoadMoreData/27?Page=1" if sub_category.include?("Thời trang")
      return "http://www.yan.vn/Dep/LoadMoreData/28?Page=1" if sub_category.include?("Trang điểm")
      return "http://www.yan.vn/Choi/LoadMoreData/11?Page=1" if sub_category.include?("Ăn chơi")
      return "http://www.yan.vn/Mlog2/LoadMoreData/11?Page=1" if sub_category.include?("Tâm sự")
      ""
    end

    def parse_list_news(link_original)
      (1..6).map{ |page|
        link_original.gsub("Page=1", "Page=#{page}")
      }
    end

    def list_links(link)
      doc = nokogiri(link)
      unless doc.present?
        doc = nokogiri(URI.encode(link))
      end
      arr_links = []
      begin
        doc.xpath("//body/div/a").each do |parse|
          if parse.present? && parse.child.present? && parse.child["src"].present?
            arr_links <<  {
              link: base_url + parse["href"],
              image: parse.child["src"],
              title: parse.child["alt"]
            }
          puts "#{Time.now}  #{parse["href"]}"
          end
        end
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
      arr_links
    end

    def parse_news_detail(item, obj)
      doc = nokogiri(item[:link])
      unless doc.present?
        doc = nokogiri(URI.encode(item[:link]))
      end
      begin
        short_content = get_short_content(doc, item[:link])
        item[:title] = doc.xpath("//h1[@itemprop='name']").text().strip.squish
        item[:content] = get_content(doc)
        item[:short_content] = short_content.present? ? short_content : item[:title]
        item[:category_news_id] = obj.id
        save_articles(item, obj, logger, 20)
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end

    def get_content(doc)
      content = doc.xpath("//div[@id='contentBody']")
      exclude_content = content.search("//div[@class='inner-article']")
      if exclude_content.present?
        (0..exclude_content.size).each do |item|
          content.search("//div[@class='inner-article']")[item].try(:remove)
        end
      end

      exclude_content_add_google = content.search("//ins[@class='adsbygoogle']")
      if exclude_content_add_google.present?
        (0..exclude_content_add_google.size).each do |item|
          content.search("//ins[@class='adsbygoogle']")[item].try(:remove)
        end
      end

      exclude_content_video = content.search("//div[@class='embed-releated-video']")
      if exclude_content_video.present?
        (0..exclude_content_video.size).each do |item|
          content.search("//div[@class='embed-releated-video']")[item].try(:remove)
        end
      end

      exclude_content_facebook = content.search("//center")
      if exclude_content_facebook.present?
        (0..exclude_content_facebook.size).each do |item|
          content.search("//center")[item].try(:remove)
        end
      end
      content
    end

    def get_short_content(doc, link)
      begin
        doc.xpath("//div[@id='contentBody']/p/strong").first.text().strip.squish
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end
  end
end
