# encoding: utf-8
require 'base/base'
module Site
  class BlogTamSu < Base::Base
    attr_accessor :base_url, :logger

    def initialize
      @base_url = "http://blogtamsu.vn"
      @logger = Logger.new('log/crawl_blogtamsu.log', 'daily')
    end

    def start_crawl_star_news
      CategoryNews.all.reverse.each do |item|
        transfer = []
        link_original = map_url_ajax(item.url, item.sub_category)
        next unless link_original.present?
        parse_list_news(link_original).each do |link|
          transfer << list_links(link)
        end
        transfer.flatten.compact.reverse.each do |detail|
          parse_news_detail(detail, item)
        end
      end
    end

    private

    def map_url_ajax(url, sub_category)
      # return "http://www.yan.vn/Video/LoadMoreData/4?Page=1" if sub_category.include?("Showbiz")
      # return "http://www.yan.vn/Video/LoadMoreData/5?Page=1" if sub_category.include?("S show")
      # return "http://www.yan.vn/Video/LoadMoreData/6?Page=1" if sub_category.include?("Kênh siêu nhạc")
      # return "http://www.yan.vn/Video/LoadMoreData/7?Page=1" if sub_category.include?("Cẩm nang giải trí")
      return "http://blogtamsu.vn/cong-dong-2/page/2" if sub_category.include?("Đời sống")
      return "http://blogtamsu.vn/sao-2/tin-ngoi-sao/page/1" if sub_category.include?("Sao việt")
      return "http://blogtamsu.vn/sao-2/tin-sao-ngoai/page/1" if sub_category.include?("Sao châu á")
      return "http://blogtamsu.vn/gioi-tre-cong-dong-2/page/1" if sub_category.include?("Học đường")
      return "http://blogtamsu.vn/ngam-2/page/1" if sub_category.include?("Tình yêu")
      return "http://blogtamsu.vn/la-fun/page/1" if sub_category.include?("Cư dân mạng")
      return "http://blogtamsu.vn/lam-dep/dep-nhu-sao-lam-dep/page/1" if sub_category.include?("Thời trang")
      return "http://blogtamsu.vn/lam-dep/tips-lam-dep/page/1" if sub_category.include?("Trang điểm")
      return "http://blogtamsu.vn/tin-showbiz/page/1" if sub_category.include?("Ăn chơi")
      return "http://blogtamsu.vn/chia-se/page/1" if sub_category.include?("Tâm sự")
      ""
    end

    def parse_list_news(link_original)
      (1..2).map{ |page|
        link_original.gsub("page/1", "page/#{page}")
      }
    end

    def list_links(link)
      doc = nokogiri(link)
      unless doc.present?
        doc = nokogiri(URI.encode(link))
      end
      arr_links = []
      begin
        doc.xpath("//div[@class='folder']/div[@class='folder-530']//a[@class='thumb']").each do |parse|
          if parse.present? && parse.child.next["src"].present?
            arr_links <<  {
              link: parse["href"],
              image: parse.child.next["src"],
              title: ""
            }
          puts "#{Time.now}  #{parse["href"]}"
          end
        end
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
      arr_links
    end

    def parse_news_detail(item, obj)
      doc = nokogiri(item[:link])
      unless doc.present?
        doc = nokogiri(URI.encode(item[:link]))
      end
      begin
        short_content = get_short_content(doc, item[:link])
        item[:title] =  doc.xpath("//h1[@class='title']").text().strip.squish
        item[:content] = get_content(doc)
        item[:short_content] = short_content.present? ? short_content : item[:title]
        item[:category_news_id] = obj.id
        save_articles(item, obj, logger, 22)
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end

    def get_content(doc)
      content = doc.xpath("//div[@id='remain_detail']")
      exclude_content = content.search("//div[@class='box_news tincungchuyenmuc']")
      if exclude_content.present?
        (0..exclude_content.size).each do |item|
          content.search("//div[@class='box_news tincungchuyenmuc']")[item].try(:remove)
        end
      end

      exclude_content_add_google = content.search("//ins[@class='adsbygoogle']")
      if exclude_content_add_google.present?
        (0..exclude_content_add_google.size).each do |item|
          content.search("//ins[@class='adsbygoogle']")[item].try(:remove)
        end
      end

      exclude_content_video = content.search("//div[@id='remain_detail']//script")
      if exclude_content_video.present?
        (0..exclude_content_video.size).each do |item|
          content.search("//div[@id='remain_detail']//script")[item].try(:remove)
        end
      end

      exclude_content_adds = content.search("//div[@style='margin-top: 8px; margin-bottom: 15px;']")
      if exclude_content_adds.present?
        (0..exclude_content_adds.size).each do |item|
          content.search("//div[@style='margin-top: 8px; margin-bottom: 15px;']")[item].try(:remove)
        end
      end

      exclude_content_adds1 = content.search("//div[@style='margin-top: 8px; margin-bottom: 8px;']")
      if exclude_content_adds1.present?
        (0..exclude_content_adds1.size).each do |item|
          content.search("//div[@style='margin-top: 8px; margin-bottom: 8px;']")[item].try(:remove)
        end
      end

      exclude_content_adds_blog = content.search("//a[@class='ads-label']")
      if exclude_content_adds_blog.present?
        (0..exclude_content_adds_blog.size).each do |item|
          content.search("//a[@class='ads-label']")[item].try(:remove)
        end
      end

      content
    end

    def get_short_content(doc, link)
      begin
        doc.xpath("//p[@class='lead']").first.text().strip.squish
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end
  end
end
