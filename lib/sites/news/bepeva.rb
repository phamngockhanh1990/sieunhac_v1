# encoding: utf-8
require 'base/base'
module Site
  class BepEva < Base::Base
    attr_accessor :base_url, :logger

    def initialize
      @base_url = "http://bepeva.com"
      @logger = Logger.new('log/crawl_bepeva.log', 'daily')
    end

    def start_crawl_star_news
      CategoryNews.all.reverse.each do |item|
        transfer = []
        link_original = map_url_ajax(item.url, item.sub_category)
        next unless link_original.present?
        parse_list_news(link_original).each do |link|
          transfer << list_links(link)
        end
        transfer.flatten.compact.reverse.each do |detail|
          parse_news_detail(detail, item)
        end
      end
    end

    private

    def map_url_ajax(url, sub_category)
      return url if sub_category.include?("Món ngon mỗi ngày")
      return url if sub_category.include?("Món ngon cuối tuần")
      return url if sub_category.include?("Món ngon dễ làm")
      return url if sub_category.include?("Tối nay ăn gì")
      return url if sub_category.include?("Mẹo hay")
      ""
    end

    def parse_list_news(link_original)
      (1..2).map{ |page|
        link_original.gsub("page/1", "page/#{page}")
      }
    end

    def list_links(link)
      doc = nokogiri(link)
      unless doc.present?
        doc = nokogiri(URI.encode(link))
      end
      arr_links = []
      begin
        doc.xpath("//div[@id='content_box']//a[@href]").each do |parse|
          if parse.present?
            arr_links <<  {
              link: parse["href"],
              image: "",
              title: ""
            }
          puts "#{Time.now}  #{parse["href"]}"
          end
        end
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
      arr_links
    end

    def parse_news_detail(item, obj)
      doc = nokogiri(item[:link])
      unless doc.present?
        doc = nokogiri(URI.encode(item[:link]))
      end
      begin
        short_content = get_short_content(doc, item[:link])
        item[:title] = doc.xpath("//h1[@class='title single-title']").text().strip.squish
        item[:image] = get_image(doc, item[:link])
        item[:content] = get_content(doc)
        item[:short_content] = short_content.present? ? short_content : item[:title]
        item[:category_news_id] = obj.id
        save_articles(item, obj, logger, 30)
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end

    def get_image(doc, link)
      parse_img = doc.xpath("//div[@class='single_post']//img")
      parse_img.present? ? parse_img.first["src"] : nil
    end

    def get_content(doc)
      content = doc.xpath("//div[@class='post-single-content box mark-links']")
      exclude_content = content.search("//div[@class='yarpp-related']")
      if exclude_content.present?
        (0..exclude_content.size).each do |item|
          content.search("//div[@class='yarpp-related']")[item].try(:remove)
        end
      end

      exclude_content_add_google = content.search("//div[@class='bottomad']")
      if exclude_content_add_google.present?
        (0..exclude_content_add_google.size).each do |item|
          content.search("//div[@class='bottomad']")[item].try(:remove)
        end
      end

      exclude_content_video = content.search("//div[@class='tags']")
      if exclude_content_video.present?
        (0..exclude_content_video.size).each do |item|
          content.search("//div[@class='tags']")[item].try(:remove)
        end
      end
      content
    end

    def get_short_content(doc, link)
      begin
        doc.xpath("//div[@class='post-single-content box mark-links']//p").first.text().strip.squish
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end
  end
end
