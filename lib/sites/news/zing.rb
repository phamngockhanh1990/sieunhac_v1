# encoding: utf-8
require 'base/base'
module Site
  class Zing < Base::Base
    attr_accessor :base_url, :logger

    def initialize
      @base_url = "http://news.zing.vn"
      @logger = Logger.new('log/crawl_zing.log', 'daily')
    end

    def start_crawl_star_news
      CategoryNews.all.reverse.each do |item|
        transfer = []
        link_original = map_url_ajax(item.url, item.sub_category)
        next unless link_original.present?
        parse_list_news(link_original).each do |link|
          transfer << list_links(link)
        end
        transfer.flatten.compact.reverse.each do |detail|
          parse_news_detail(detail, item)
        end
      end
    end

    private

    def map_url_ajax(url, sub_category)
      return "http://news.zing.vn/am-nhac/nhac-viet-nam/trang1.html" if sub_category.include?("Sao việt")
      ""
    end

    def parse_list_news(link_original)
      (1..1).map{ |page|
        link_original.gsub("trang1", "trang#{page}")
      }
    end

    def list_links(link)
      doc = nokogiri(link)
      unless doc.present?
        doc = nokogiri(URI.encode(link))
      end
      arr_links = []
      begin
        doc.xpath("//section[@class='cate_content']//div[@class='cover']//a[@href]").each do |parse|
          if parse.present? && parse["href"].present?
            arr_links <<  {
              link: base_url + parse["href"],
              image: "",
              title: ""
            }
          puts "#{Time.now}  #{parse["href"]}"
          end
        end
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
      arr_links
    end

    def parse_news_detail(item, obj)
      doc = nokogiri(item[:link])
      unless doc.present?
        doc = nokogiri(URI.encode(item[:link]))
      end
      begin
        short_content = get_short_content(doc, item[:link])
        item[:title] = doc.xpath("//h1[@class='the-article-title']").text().strip.squish
        item[:content] = get_content(doc, item[:link])
        item[:image] = get_image(doc)
        item[:short_content] = short_content.present? ? short_content : item[:title]
        item[:category_news_id] = obj.id
        save_articles(item, obj, logger, 23)
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end

    def get_image(doc)
      image = doc.xpath("//div[@class='the-article-body']//img")
      image =
      if image.present?
        temp = image.first["src"]
        temp.include?("http://") ? temp : "http://news.zing.vn#{temp}"
      else
        ""
      end
    end

    def get_content(doc, link)
      content = doc.xpath("//div[@class='the-article-body']")
      exclude_content = content.search("//div[@class='inner-article']")
      if exclude_content.present?
        (0..exclude_content.size).each do |item|
          content.search("//div[@class='inner-article']")[item].try(:remove)
        end
      end

      exclude_content_add_google = content.search("//div[@class='inner-video']")
      if exclude_content_add_google.present?
        (0..exclude_content_add_google.size).each do |item|
          content.search("//div[@class='inner-video']")[item].remove() rescue nil
        end
      end

      exclude_content_video = content.search("//div[@class='inner-article']")
      if exclude_content_video.present?
        (0..exclude_content_video.size).each do |item|
          content.search("//div[@class='inner-article']")[item].try(:remove)
        end
      end
      content
    end

    def get_short_content(doc, link)
      begin
        doc.xpath("//p[@class='the-article-summary']").text().strip.squish
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end
  end
end
