# encoding: utf-8
require 'base/base'
module Site
  class ShowBizVN < Base::Base
    attr_accessor :base_url, :logger

    def initialize
      @base_url = "http://showbizvn.com"
      @logger = Logger.new('log/crawl_showbiz.log', 'daily')
    end

    def start_crawl_star_news
      CategoryNews.all.reverse.each do |item|
        transfer = []
        link_original = map_url_ajax(item.url, item.sub_category)
        next unless link_original.present?
        transfer << list_links(link_original)
        transfer.flatten.compact.reverse.each do |detail|
          parse_news_detail(detail, item)
        end
      end
    end

    private

    def map_url_ajax(url, sub_category)
      return "http://showbizvn.com/suc-khoe/" if sub_category.include?("Đời sống")
      return "http://showbizvn.com/nhac/viet-nam/" if sub_category.include?("Sao việt")
      return "http://showbizvn.com/tre/hot-boy/" if sub_category.include?("Học đường")
      return "http://showbizvn.com/12-cung-hoang-dao/" if sub_category.include?("Tình yêu")
      return "http://showbizvn.com/phim/" if sub_category.include?("Cư dân mạng")
      return "http://showbizvn.com/khong-gian-song/" if sub_category.include?("Ăn chơi")
      return "http://showbizvn.com/yeu/tam-su/" if sub_category.include?("Tâm sự")
      ""
    end

    def list_links(link)
      doc = nokogiri(link)
      unless doc.present?
        doc = nokogiri(URI.encode(link))
      end
      arr_links = []
      begin
        doc.xpath("//div[@class='folder']/div[@class='folder-530']//a[@class='thumb']").each do |parse|
          if parse.present? && parse.child.next["src"].present?
            arr_links <<  {
              link: parse["href"],
              image: parse.child.next["src"],
              title: ""
            }
          puts "#{Time.now}  #{parse["href"]}"
          end
        end
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
      arr_links
    end

    def parse_news_detail(item, obj)
      doc = nokogiri(item[:link])
      unless doc.present?
        doc = nokogiri(URI.encode(item[:link]))
      end
      begin
        short_content = get_short_content(doc, item[:link])
        item[:title] =  doc.xpath("//h1[@class='title']").text().strip.squish
        item[:content] = get_content(doc)
        item[:short_content] = short_content.present? ? short_content : item[:title]
        item[:category_news_id] = obj.id
        save_articles(item, obj, logger, 24)
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end

    def get_content(doc)
      content = doc.xpath("//div[@id='remain_detail']")
      exclude_content = content.search("//div[@class='box_news tincungchuyenmuc']")
      if exclude_content.present?
        (0..exclude_content.size).each do |item|
          content.search("//div[@class='box_news tincungchuyenmuc']")[item].try(:remove)
        end
      end

      exclude_content_add_google = content.search("//ins[@class='adsbygoogle']")
      if exclude_content_add_google.present?
        (0..exclude_content_add_google.size).each do |item|
          content.search("//ins[@class='adsbygoogle']")[item].try(:remove)
        end
      end

      exclude_content_video = content.search("//div[@id='remain_detail']//script")
      if exclude_content_video.present?
        (0..exclude_content_video.size).each do |item|
          content.search("//div[@id='remain_detail']//script")[item].try(:remove)
        end
      end

      exclude_content_adds = content.search("//div[@style='margin-top: 8px; margin-bottom: 15px;']")
      if exclude_content_adds.present?
        (0..exclude_content_adds.size).each do |item|
          content.search("//div[@style='margin-top: 8px; margin-bottom: 15px;']")[item].try(:remove)
        end
      end

      exclude_content_adds_blog = content.search("//a[@class='ads-label']")
      if exclude_content_adds_blog.present?
        (0..exclude_content_adds_blog.size).each do |item|
          content.search("//a[@class='ads-label']")[item].try(:remove)
        end
      end

      content
    end

    def get_short_content(doc, link)
      begin
        doc.xpath("//p[@class='lead']").first.text().strip.squish
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end
  end
end
