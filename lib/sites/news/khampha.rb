# encoding: utf-8
require 'base/base'
module Site
  class KhamPha < Base::Base
    attr_accessor :base_url
    KHAMPHA = "http://khampha.vn/ajax/box_bai_viet_trang_chuyen_muc/index/7/1/4/1/0/1/0"

    def initialize
      @base_url = "http://khampha.vn"
    end

    private
    def parse_list_news
      (1..20).map{|page|
        KHAMPHA.gsub("index/7/1/4/1/0/1/0","index/7/#{page}/4/1/0/1/0")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//span[@class='imgFloat imgNews-medium']//a")
      lists.each do |item|
        arr_links << { link: "#{base_url}#{item['href']}", image: item.child["src"], title: item.child["alt"].try(:squish).try(:strip) }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 7
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      short_content = doc.xpath("//p[@class='baiviet-sapo']")
      long_content = doc.xpath("//div[@class=' text-conent']")
      "#{short_content}\n\t#{long_content}"
    end

    def get_short_content(doc)
      doc.xpath("//p[@class='baiviet-sapo']").text()
    end
  end
end
