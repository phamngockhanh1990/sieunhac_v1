# encoding: utf-8
require 'base/base'
module Site
  class NgoiSaoNet < Base::Base
    attr_accessor :base_url, :logger

    def initialize
      @base_url = "http://ngoisao.net"
      @logger = Logger.new('log/crawl_ngoisaonet.log', 'daily')
    end

    def start_crawl_star_news
      CategoryNews.all.reverse.each do |item|
        link_original = map_url_ajax(item.url, item.sub_category)
        next unless link_original.present?
        list_links(link_original, item)
      end
    end

    private

    def map_url_ajax(url, sub_category)
      return "http://ngoisao.net/tin-tuc/hau-truong/showbiz-viet" if sub_category.include?("Sao việt")
      return "http://ngoisao.net/tin-tuc/hau-truong/chau-a" if sub_category.include?("Sao châu á")
      return "http://ngoisao.net/tin-tuc/hau-truong/hollywood" if sub_category.include?("Sao âu châu")
      return "http://ngoisao.net/tin-tuc/phong-cach/choi-blog" if sub_category.include?("Cư dân mạng")
      return "http://ngoisao.net/tin-tuc/phong-cach/thoi-trang" if sub_category.include?("Thời trang")
      return "http://ngoisao.net/tin-tuc/phong-cach/lam-dep" if sub_category.include?("Trang điểm")
      return "http://ngoisao.net/tin-tuc/thu-gian/an-choi/" if sub_category.include?("Ăn chơi")
      return "http://ngoisao.net/tin-tuc/gia-dinh/go-roi" if sub_category.include?("Tâm sự")
      ""
    end

    def list_links(link, item)
      doc = nokogiri(link)
      unless doc.present?
        doc = nokogiri(URI.encode(link))
      end
      begin
        info = {}
        doc.xpath("//div[@class='lnht2']//a[@class='fl ptw']").reverse.each do |parse|
          if parse.present? && parse["href"].present? && parse.child.next["src"].present?
            info = {
              link: parse["href"],
              image: parse.child.next["src"],
              title: parse["title"]
            }
            puts "#{Time.now}  #{parse["href"]}"
            parse_news_detail(info, item)
          end
        end
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end

    def parse_news_detail(item, obj)
      doc = nokogiri(item[:link])
      unless doc.present?
        doc = nokogiri(URI.encode(item[:link]))
      end
      begin
        short_content = get_short_content(doc, item[:link])
        item[:title] = item[:title].present? ? item[:title] : get_title(doc)
        item[:short_content] = short_content.present? ? short_content : item[:title]

        item[:content] = get_content(doc, item[:link])
        item[:category_news_id] = obj.id
        save_articles(item, obj, logger, 21)
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end

    def get_content(doc, link)
      content = doc.xpath("//div[@class='fck_detail']")
      exclude_content = content.search("//em[@style='color:rgb(51,51,51);line-height:18px;']")
      if exclude_content.present?
        (0..exclude_content.size).each do |item|
          content.search("//em[@style='color:rgb(51,51,51);line-height:18px;']")[item].try(:remove)
        end
      end

      exclude_content_add_google = content.search("//link[@href='http://st.f2.ngoisao.vnecdn.net/c/v17/general_small.css']")
      if exclude_content_add_google.present?
        (0..exclude_content_add_google.size).each do |item|
          content.search("//link[@href='http://st.f2.ngoisao.vnecdn.net/c/v17/general_small.css']")[item].try(:remove)
        end
      end

      exclude_content_video = content.search("//span[@style='color:#0000ff;']")
      if exclude_content_video.present?
        (0..exclude_content_video.size).each do |item|
          content.search("//span[@style='color:#0000ff;']")[item].try(:remove)
        end
      end

      exclude_content_android = content.search("//table[@style='border-collapse:collapse;border-spacing:0px;margin:0px auto 10px;color:rgb(0,0,0);line-height:1.6em;']")
      if exclude_content_android.present?
        (0..exclude_content_android.size).each do |item|
          content.search("//table[@style='border-collapse:collapse;border-spacing:0px;margin:0px auto 10px;color:rgb(0,0,0);line-height:1.6em;']")[item].try(:remove)
        end
      end

      exclude_content_ngoisao = content.search("//span[@style='color:#0000ff;']")
      if exclude_content_ngoisao.present?
        (0..exclude_content_ngoisao.size).each do |item|
          content.search("//span[@style='color:#0000ff;']")[item].try(:remove)
        end
      end
      content
    end

    def get_short_content(doc, link)
      begin
        doc.xpath("//h2[@class='lead']").text().strip.squish
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end

    def get_title(doc)
      begin
        doc.xpath("//h1[@class='title']").text().strip.squish
      rescue => e
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end
  end
end
