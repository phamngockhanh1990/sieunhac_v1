# encoding: utf-8
require 'base/base'
module Site
  class VnExpress < Base::Base
    VNEXPRESS = "http://vnexpress.net/tin-tuc/thoi-su"

    private
    def parse_list_news
      (1..20).map{|page|
        "#{VNEXPRESS}/page/#{page}.html"
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='block_mid_new']//div[@class='thumb']//a")
      lists.each do |item|
        arr_links << { link: item["href"], image: item.child["src"], title: item.child["alt"] }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      item[:content] = get_content(doc, item[:link])
      item[:short_content] = get_short_content(doc)
      item[:category_news_id] = 0
      save_articles(item)
    end

    def get_content(doc, url)
      short_content = doc.xpath("//div[@class='short_intro txt_666']")
      long_content = doc.xpath("//div[@class='fck_detail width_common']")
      "#{short_content}\n\t#{long_content}"
    end

    def get_short_content(doc)
      doc.xpath("//div[@class='short_intro txt_666']").text()
    end
  end
end

