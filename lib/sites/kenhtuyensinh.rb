# encoding: utf-8
require 'base/base'
module Site
  class KenhTuyenSinh < Base::Base
    KENHTUYENSINH = "http://kenhtuyensinh.vn/tuyen-sinh/1/0"

    private
    def parse_list_news
      (1..4).map{|page|
        KENHTUYENSINH.gsub("/1/0","/#{page}/#{12 * (page - 1)}")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='folder']//div[@class='detail_left']//a")
      lists.each do |item|
        arr_links << { link: item["href"], image: item.child.next["src"], title: item.child.next["alt"] }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 5
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      short_content = doc.xpath("//div[@class='descriptionactice']//b")
      long_content = doc.xpath("//div[@class='textfull']")
      "#{short_content}\n\t#{long_content}"
    end

    def get_short_content(doc)
      doc.xpath("//div[@class='descriptionactice']//b").text()
    end
  end
end
