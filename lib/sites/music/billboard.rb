# encoding: utf-8
require 'base/base'
class BillBoard < Base::Base
  attr_accessor :base_url
  def initialize
    @base_url = "http://www.billboard.com/charts/hot-100"
  end

  def start_crawler_music
    doc = nokogiri(base_url)
    links = get_links(doc)
    loop_details(links)
  end

  def get_links(doc)
    parse_links = doc.xpath("//div[@class='moduleHolderLeft shadowModuleHolderLeft']//div[@id='tab-2']//a[@class='resultNameLink']")
    arr_links = []
    parse_links.each do |link|
      arr_links << "#{base_url}#{link.attributes["href"].value}"
    end
    arr_links
  end

  def loop_details(links)
    links.each { |link| parse_detail(link) }
  end

  def parse_detail(link)
    doc = nokogiri(link)
    musics = {}
    musics[:title] = doc.at_xpath("//div[@class='moduleHolderLeft shadowModuleHolderLeft']//span[@class='songTitle']").text().strip.squish
    musics[:count_listen]   = 0
    musics[:category_id]    = 0
    musics[:singer] = doc.at_xpath("//div[@class='moduleHolderLeft shadowModuleHolderLeft']//a[@class=' artistName']").text().strip.squish
    musics[:site_id]        = 10
    musics[:url_original]   = link
    musics[:lsxanh]         = false
    musics[:nhachot]        = false
    musics[:music_top]      = true
    musics[:url]            = lansongxanh(doc)
    save_musics(musics)
  end

  def lansongxanh(doc)
    parse_music = doc.css("script")
    parse_music.each do |script|
      if script.content.include?("Data_LSX/SONGS")
        return "#{base_url}#{script.content.scan(/Data_LSX\/SONGS\/[\w*|\/| |_|-]*\.mp3/).first}"
      end
    end
  end
end
