# encoding: utf-8
require 'base/base'
class NhacSo < Base::Base
  attr_accessor :base_url
  def initialize
    @base_url = "http://nhacso.net"
  end

  def start_crawler_music
    doc = nokogiri(base_url)
    links = get_links(doc)
    loop_details(links)
  end

  def get_links(doc)
    parse_links = doc.xpath("//div[@class='listening']//h3")
    arr_links = []
    parse_links.each do |link|
      arr_links << "#{base_url}#{link.child.next["href"]}"
    end
    arr_links
  end

  def loop_details(links)
    links.each { |link| parse_detail(link) }
  end

  def parse_detail(link)
    doc = nokogiri(link)
    musics = {}
    musics[:title]          = doc.xpath("//div[@class='header']//h3[@class='name']").text().strip.squish
    musics[:count_listen]   = doc.xpath("//div[@class='header']//a[@class='like']/span").text().strip.squish
    musics[:category_id]    = 0
    musics[:singer]         = doc.xpath("//span[@itemprop='byArtist']//a").text().strip.squish
    musics[:site_id]        = 4
    musics[:url_original]   = link
    musics[:lsxanh]         = false
    musics[:lyrics]         = doc.xpath("//p[@class='desc expand-song readmore-js-section readmore-js-collapsed']")
    musics[:information]    = doc.xpath("//p[@class='desc expand-song readmore-js-section readmore-js-collapsed']").text().try(:strip).try(:squish)
    musics[:type_music]     = doc.xpath("//span[@class='cate']//a").text().try(:strip).try(:squish)
    musics[:nhachot]        = true
    musics[:music_top]      = false
    musics[:url]            = nhacso(link)
    save_musics(musics)
  end

  def nhacso(url)
    get_code = url.split(".")
    get_code.pop
    ajax_link = "http://nhacso.net/songs/ajax-get-detail-song?dataId=#{get_code.last}"
    JSON.load(open(ajax_link))["songs"].first["link_mp3"]
  end
end
