# encoding: utf-8
require 'base/base'

class ZingSinger < Base::Base
  attr_accessor :base_url
  def initialize
    @base_url = "http://mp3.zing.vn/the-loai-nghe-si/Viet-Nam/IWZ9Z08I.html"
  end

  def start_crawler_music
    ActiveRecord::Base.connection.execute("TRUNCATE singers")
    doc = nokogiri(base_url)
    number_paging = get_links_paging(doc)
    loop_details(number_paging)
  end

  def get_links_paging(doc)
    parse_links = doc.xpath("//div[@class='pagination']//a").last
    parse_links["href"].split("?page=").last.to_i
  end

  def loop_details(links)
    (1..26).each { |link| parse_detail("#{base_url}?page=#{link}") }
  end

  def parse_detail(link)
    doc = nokogiri(link)
    list_singers = doc.xpath("//div[@class='tab-pane']//div[@class='item']//img")
    arr = []
    list_singers.each do |singer|
      puts "save image"
      obj_image = mechanize(singer["src"])
      obj_image.save("public/images/musics/singers/vietnam/#{singer["alt"]}.jpg")
      save_singers(singer["alt"])
    end
  end
end
