# encoding: utf-8
require 'base/base'
module Site
  class NgoiSao < Base::Base
    attr_accessor :base_url
    NGOISAO = "http://ngoisao.net/tin-tuc/hau-truong/page/1.html"

    def initialize
      @base_url = "http://ngoisao.net"
    end

    private
    def parse_list_news
      arr_links = []
      (1..1935).map{|page|
        arr_links << NGOISAO.gsub("page/1","page/#{page}")
      }
      arr_links.uniq
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//ul[@class='news']//a[@class='fl ptw']")
      lists.each do |item|
        puts item["href"]
        begin
          arr_links << {
            link:  item["href"],
            image: item.child.next["src"],
            title: item.child.next["title"]
          }
        rescue => e
          puts e.inspect
          puts e.backtrace
        end
      end
      arr_links.reverse
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        short_ctn = get_short_content(doc)
        item[:title] = item[:title]
        item[:content] = "#{short_ctn}\r\n#{get_content(doc)}"
        item[:short_content] = short_ctn
        item[:category_news_id] = 15
        save_articles(item)
      rescue => e
      end
    end

    def get_title(doc)
      doc.xpath("//h2[@class='entry-title']").text().try(:strip).try(:squish)
    end

    def get_short_content(doc)
      doc.xpath("//h2[@class='lead']").text().try(:strip).try(:squish)
    end

    def get_content(doc)
      doc.xpath("//div[@class='fck_detail']").to_s.gsub(".css","").gsub("width=", "")
    end
  end
end
