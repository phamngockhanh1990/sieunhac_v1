# encoding: utf-8
require 'base/base'
module Site
  class Wn < Base::Base
    attr_accessor :base_url
    WN = "http://www.wn.com.vn/brands/Anh-Girl-xinh.html?page=1&sort=newest"

    def initialize
      @base_url = "http://www.wn.com.vn"
    end

    private
    def parse_list_news
      (1..20).map{|page|
        WN.gsub("page=1","page=#{page}")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//ul[@class='ProductList']//div[@class='article-photo']")
      lists.each do |item|
        arr_links << {
          link:  item.child.next["href"],
          image: item.child.next.child.next.child.next.next["data-original"],
          title: item.child.next.child.next.child.next.next["alt"]
        }
      end
      arr_links.reverse
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 12
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      doc.xpath("//div[@class='column9']")
    end

    def get_short_content(doc)
      doc.xpath("//div[@class='column9']/p/strong").first.text()
    end
  end
end
