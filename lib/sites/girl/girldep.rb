# encoding: utf-8
require 'base/base'
module Site
  class GirlDep < Base::Base
    attr_accessor :base_url
    GIRLDEP = "http://girldep.net/category/hot-girl/page/1/"
    GIRLDEPBIKINI = "http://girldep.net/category/hinh-bikini/page/1/"
    GIRLDEPLOHANG = "http://girldep.net/category/scandal/page/1/"
    GIRLDEPLOHANG1 = "http://girldep.net/category/anh-nong-lo-hang/page/1/"

    def initialize
      @base_url = "http://girldep.net"
    end

    private
    def parse_list_news
      arr_links = []
      (1..9).map{|page|
        arr_links << GIRLDEP.gsub("page/1","page/#{page}")
      }

      (1..4).map{|page|
        arr_links << GIRLDEPBIKINI.gsub("page/1","page/#{page}")
      }

      (1..6).map{|page|
        arr_links << GIRLDEPLOHANG.gsub("page/1","page/#{page}")
      }

      (1..5).map{|page|
        arr_links << GIRLDEPLOHANG1.gsub("page/1","page/#{page}")
      }
      arr_links.reverse.uniq
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='post-listing']//div[@class='post-thumbnail']")
      lists.each do |item|
        puts "#{item.child.next['href']}"
        begin
          arr_links << {
            link:  item.child.next["href"],
            image: item.child.next.child.next["src"],
            title: ""
          }
        rescue => e
          puts e.inspect
          puts e.backtrace
        end
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:title] = get_title(doc)
        item[:content] = filter_content(doc)
        item[:short_content] = get_short_content(doc, item[:link])
        item[:category_news_id] = 14
        save_articles(item)
      rescue => e
      end
    end

    def get_title(doc)
      doc.xpath("//h1[@class='post-title']").text().try(:strip).try(:squish)
    end

    def get_content(doc)
      text1 = doc.xpath("//div[@class='entry']/p")
      text2 = doc.xpath("//div[@class='entry']//table")
      return text1 if text1.size > 0
      return text2 if text2.size > 0
      ""
    end

    def get_short_content(doc)
      doc.xpath("//div[@class='entry']/p").first.text().try(:strip).try(:squish)
    end

    def filter_content(doc)
      content = get_content(doc)
      content = content.gsub("stype=", "") if content.include?("style=")
      content
    end
  end
end
