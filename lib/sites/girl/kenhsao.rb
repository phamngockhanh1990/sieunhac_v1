# encoding: utf-8
require 'base/base'
module Site
  class KenhSao < Base::Base
    attr_accessor :base_url
    KENHSAO = "http://kenhsao.net/teen-7/trang-26.html"
    KENHSAODIENANH = "http://kenhsao.net/dien-anh-4/trang-1.html"
    KENHSAOSAO = "http://kenhsao.net/sao-1/trang-1.html"
    KENHSAOHOAHAU = "http://kenhsao.net/hoa-hau-14/trang-1.html"

    def initialize
      @base_url = "http://kenhsao.net/"
    end

    private
    def parse_list_news
      arr_links = []
      (1..26).map{|page|
        arr_links << KENHSAO.gsub("trang-1","trang-#{page}")
      }

      (1..56).map{|page|
        arr_links << KENHSAODIENANH.gsub("trang-1","trang-#{page}")
      }

      (1..157).map{|page|
        arr_links << KENHSAOSAO.gsub("trang-1","trang-#{page}")
      }

      (1..10).map{|page|
        arr_links << KENHSAOHOAHAU.gsub("trang-1","trang-#{page}")
      }

      arr_links.uniq
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//ul[@class='news']//a[@class='fl ptw']")
      lists.each do |item|
        puts "#{base_url}#{item['href']}"
        begin
          arr_links << {
            link:  "#{base_url}#{item['href']}",
            image: "#{base_url}#{item.child.next["src"].gsub("../","")}",
            title: item["title"]
          }
        rescue => e
          puts e.inspect
          puts e.backtrace
        end
      end
      arr_links.reverse
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:title] = item[:title]
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 15
        save_articles(item)
      rescue => e
      end
    end

    def get_title(doc)
      doc.xpath("//h2[@class='entry-title']").text().try(:strip).try(:squish)
    end

    def get_short_content(doc)
      doc.xpath("//h2[@class='lead']").text().try(:strip).try(:squish)
    end

    def get_content(doc)
      doc.xpath("//div[@class='fck_detail']")
    end

  end
end
