# encoding: utf-8
require 'base/base'
module Site
  class XemAnh < Base::Base
    attr_accessor :base_url
    XEMANH = "http://www.xemanh.net/category/anh-girl-xinh/page/1"
    XEMANHSEXY = "http://www.xemanh.net/category/anh-girl-xinh/anh-sexy/page/1"

    def initialize
      @base_url = "http://www.xemanh.net"
    end

    private
    def parse_list_news
      arr_links = []
      (1..10).map{|page|
        arr_links << XEMANH.gsub("page/1","page/#{page}")
      }

      (1..5).map{|page|
        arr_links << XEMANHSEXY.gsub("page/1","page/#{page}")
      }
      arr_links
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      begin
        lists = doc.xpath("//div[@id='posts-container']//div[@class='image-extras-content']")
        lists.each do |item|
          arr_links << {
            link: item.child.next["href"],
            image: item.child.next.next.next["href"],
            title: ""
          }
        end
      rescue => e
        puts e.inspect
        puts e.backtrace
      end
      arr_links.reverse
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      puts "Start crawler #{item[:link]}"
      begin
        item[:title] = get_title(doc)
        item[:content] = filter_content(doc, item[:link])
        item[:short_content] = get_title(doc)
        item[:category_news_id] = 12
        save_articles(item)
      rescue => e
      end
    end

    def get_title(doc)
      doc.at_xpath("//h2[@class='entry-title']").text()
    end

    def get_content(doc, url)
      text1 = doc.xpath("//div[@class='post-content']//div[@class='separator']")
      text2 = doc.xpath("//div[@class='post-content']//p[@align='center']")
      text3 = doc.xpath("//div[@class='post-content']//div[@class='news-content']")
      text4 = doc.xpath("//div[@class='post-content']//center")
      text5 = doc.xpath("//div[@class='post-content']//table")
      text6 = doc.xpath("//div[@class='post-content']/p")

      return text1 if text1.size > 0
      return text2 if text2.size > 0
      return text3 if text3.size > 0
      return text4 if text4.size > 0
      return text5 if text5.size > 0
      return text6 if text6.size > 0
      ""
    end

    def filter_content(doc, url)
      content = get_content(doc, url)
      content = content.to_html.gsub("style=", "") if content.to_html.include?("style=")
      content
    end
  end
end
