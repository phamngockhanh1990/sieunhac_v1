# encoding: utf-8
require 'base/base'
module Site
  class AoDai < Base::Base
    attr_accessor :base_url
    AODAI = "http://www.hotvteen.vn/category/toi-yeu-ao-dai/page/1/"

    def initialize
      @base_url = "http://www.wn.com.vn"
    end

    private
    def parse_list_news
      (1..20).map{|page|
        AODAI.gsub("page/1","page/#{page}")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='home-widget']//li[@class='infinite-post']/a")
      lists.each do |item|
        arr_links << {
          link:  item["href"],
          image: item.child.next.child.next["data-original"],
          title: ""
        }
      end
      arr_links.reverse
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:title] = get_title(doc)
        item[:content] = get_content(doc)
        item[:short_content] = get_short_content(doc)
        item[:category_news_id] = 13
        save_articles(item)
      rescue => e
      end
    end

    def get_title(doc)
      doc.xpath("//h1[@class='story-title']").text().strip.squish
    end

    def get_content(doc)
      doc.xpath("//div[@id='content-area']/p")
    end

    def get_short_content(doc)
      short_content = doc.xpath("//div[@id='content-area']/p")
      short_content[0].text().strip.squish.present? ? short_content[0].text() : short_content[1].text().strip
    end
  end
end
