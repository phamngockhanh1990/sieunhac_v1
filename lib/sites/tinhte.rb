# encoding: utf-8
require 'base/base'
module Site
  class TinhTe < Base::Base
    TINHTE = "https://www.tinhte.vn/?wpage=1"

    private
    def parse_list_news
      (1..20).map{|page|
        TINHTE.gsub("?wpage/1/","?wpage/#{page}/")
      }
    end

    def links_new_detail(links)
      doc = nokogiri(links)
      arr_links = []
      lists = doc.xpath("//div[@class='newsBlocks']//a[@class='internalLink']")
      lists.each do |item|
        arr_links << { link: "https://www.tinhte.vn/#{item['href']}", image: item.child.next["src"], title: item.child.next["alt"] }
      end
      arr_links
    end

    def parse_news_detail(item)
      doc = nokogiri(item[:link])
      begin
        item[:content]          = get_content(doc)
        item[:short_content]    = get_short_content(doc)
        item[:category_news_id] = 11
        save_articles(item)
      rescue => e
      end
    end

    def get_content(doc)
      doc.xpath("//div[@class='messageContent']/article")[0]
    end

    def get_short_content(doc)
      get_content(doc).text().try(:strip).try(:squish).truncate(30)
    end
  end
end

