# encoding: utf-8
require 'base/base'
require 'base/nokogiri_proxy'

module Site
  class ClipVN < Base::Base
    CLIPVN = "http://clip.vn/"
    attr_accessor :url, :doc, :log
    def initialize
      @log = Logger.new('log/crawler_clipvn.log', 'daily')
    end

    def start_crawl
      log.info("#{Time.now} Start crawling all videos from clipvn")
      url = CLIPVN
      list_categories.each do |list|
        parse_list_news(list).each do |link|
          main(link)
        end
      end
      main(doc)
      log.info("#{Time.now} Finish crawling all videos from clipvn")
    end

    def main(link)
      doc = nokogiri(link)
      links = links_per_page(doc)
      parse_links_detail(links)
    end

    private
    def list_categories
      [
        "http://tintuc.clip.vn/ajax/getClips?t=new-c&pt=news&p=1&c=%5B%22131%22%2C%22132%22%2C%22134%22%2C%22139%22%5D",
        "http://thethao.clip.vn/ajax/getClips?t=new-c&pt=sport&p=1&c=%5B%2215%22%2C%2262%22%2C%22184%22%2C%22220%22%5D&ch=",
        "http://giaitri.clip.vn/ajax/getClips?t=new-c&pt=entertainment&p=2&c=%5B%22155%22%2C%22137%22%2C%224%22%2C%22140%22%2C%221%22%5D&ch=",
        "http://congnghe.clip.vn/ajax/getClips?t=new-c&pt=technology&p=1&c=%5B%22148%22%2C%2226%22%2C%22205%22%2C%22208%22%5D&ch=",
        "http://doisong.clip.vn/ajax/getClips?t=new-c&pt=life&p=1&c=%5B%22187%22%2C%2216%22%2C%2225%22%2C%2247%22%2C%2295%22%2C%2214%22%2C%2211%22%5D&ch=",
        "http://nhac.clip.vn/ajax/getMusics?t=new-c&p=1&c=0&o=the-loai&oid=Nhac-che"
      ]
    end

    #### get all links perpage
    ### @params: doc was document parsed by nokogiri
    ### @return: return list links of perpage
    def links_per_page(doc)
      results = []
      doc.xpath("//a[@class='item']").each do |item|
        hash_temp = {}
        begin
          hash_temp[:href] = item["href"]
          hash_temp[:img] = item.try(:children).try(:children).try(:children)[1]["src"]
          hash_temp[:title] = item["title"]
          results << hash_temp
        rescue => e
          log.error("#{Time.now} Error #{e.inspect}")
        end
      end
      results
    end

    def parse_list_news(link)
      (1..20).map {|page| link.gsub("&p=1&c", "&p=#{page}&c")}
    end

    def parse_links_detail(links)
      links.each { |link|
        parse_detail(link[:href], link[:img], link[:title])
      }
    end

    def parse_detail(link, img, title)
      doc = NokogiriProxy.new(link).parse_link
      save_item({
        name: title,
        url: link,
        author: "clip.vn",
        content: "",
        image: img,
        video: get_media(doc, link),
        category: get_category(doc, link)
      })
    end

    def get_media(doc, link)
      parse_music = doc.css("script")
      if parse_music.present?
        get_content = ""
        parse_music.each do |script|
          get_content = script.content if script.content.include?("clip.vegacdn.vn")
        end

        if get_content.present?
          response_link = get_content.scan(/clip\.vegacdn\.vn\/[0-9a-zA-Z|\/]*\.mp4/)
        end
      end
      response_link.try(:first)
    end

    def get_category(doc, link)
    end
  end
end

