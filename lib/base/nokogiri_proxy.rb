class NokogiriProxy
  attr_accessor :url

  def initialize(url)
    @url = url
  end

  def parse_link
    proxy = Proxy.all.try(:sample).try(:proxy)
    body, status = check_url_exist(proxy)
    if status == true
      Nokogiri::HTML(body)
    else
      puts "Call proxy"
      Proxy.find_by(proxy: proxy).destroy
      StaticProxy.crawl_new_proxy if Proxy.all.size == 0
      parse_link
    end
  end

  def check_url_exist(proxy)
    status = true
    begin
      body = open(url, proxy: "http://#{proxy}")
    rescue => e
      status = false
      body = nil
    end
    return body, status
  end
end
