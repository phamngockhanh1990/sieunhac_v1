# encoding: utf-8
require 'nokogiri'
require 'mechanize'
require 'rubygems'
require 'open-uri'
require 'pry'
require 'logger'

module Base
  class Base
    attr_reader :user_agent

    def initialize
      user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.854.0 Safari/535.2"
    end

    def nokogiri(url)
      body, status = open_url(url)
      doc = status == true ? Nokogiri::HTML.parse(body) : nil
    end

    def mechanize(url)
      agent = Mechanize.new
      page = agent.get(url)
    end

    def save_database(datas)
      datas.each { |item| save_item(item)}
    end

    def list_categories(doc, xpath_text)
      doc.xpath("#{xpath_text}").map(&:value)
    end

    def list_paging(arr_categories)
      hash_master_cat = {}
      arr_categories.each_with_index {|cat, index|
        hash_master_cat["#{index}"] = parse_list_news(cat)
      }
      hash_master_cat
    end

    def filter_link_categories(arr_cat, base_link)
      arr_cat.map {|item|
        item = item.gsub(base_link, "")
        item.include?("javascript") ? nil : (item.present? ? item : nil)
      }.try(:compact)
    end

    def list_details_links(hash_paging, xpath_text)
      hash_paging.each do |_, value|
        get_list_links(value, xpath_text)
      end
    end

    def get_list_links(links, xpath_text)
      links.each do |link|
        doc = nokogiri(link)
        links_detail = doc.xpath("#{xpath_text}").map(&:value)
        detail(links_detail)
      end
    end

    def parse_detail(link)
      doc = nokogiri(link)
      save_item({name: get_title(doc, link), url: link, author: get_author(doc),
       content: get_content(doc), image: get_image(doc), video: get_video(doc), category: get_category(doc)})
    end

    #### visit all links and start crawling each link
    def visit_links(links)
      links.map { |link| nokogiri(link) }
    end

    #### get all links perpage
    ### @params: xpath_code was code xpath to parse get elements
    ### @params: s_doc was document parsed by nokogiri
    ### @return: return list links of perpage
    def links_per_page(xpath_code, s_doc)
    end

    ### get number paging
    ### @params: xpath_code was code xpath to parse get elements
    ### @params: s_doc was document parsed by nokogiri
    ### @return: return s_doc vs number of paging
    def number_paging(xpath_code, s_doc)
      return s_doc, number_paging
    end

    def start_crawl_news
      number_links = parse_list_news
      number_links.each do |link|
        links_result = links_new_detail(link)
        news_detail(links_result)
      end
    end

    def parse_list_news; end
    def links_new_detail(link); end
    def parse_news_detail(link); end

    def news_detail(links)
      links.each {|item| parse_news_detail(item)}
    end

    protected
    def get_author(doc)
      "Sieu Nhac"
    end

    def get_video(doc)
      nil
    end

    def get_image(doc)
      "https://1anh.com/500/0/EZYwYm5NdZIwlXo48tMBufykgNFiWK-L-tezFAefrD8P5AL3AH_lJNJ5HbhrSsenukga058AFGwxwe98NXnbSHgUz37SrbD7EJg4dwgG85AFCA9SR632ZQiwQgRCjw5Z"
    end

    def get_title(doc)
      nil
    end

    def get_content(doc)
      nil
    end

    def get_type(doc)
      nil
    end

    def save_item(item)
      obj_video = Video.find_by(channel: item[:url])
      video = obj_video ? obj_video : Video.new
      begin
        video.name         = item[:name]
        video.channel      = item[:url]
        video.category     = item[:author]
        video.lyrics       = item[:content]
        video.images       = item[:image]
        video.link         = item[:video]
        video.save!
        puts "Added data #{video.name} to database"
      rescue => e
        puts "Ingore data with url: #{item[:url]}"
      end
    end

    def save_item_youtube(item, id)
      obj_video = Video.find_by(link: item.channel_id)
      begin
        if obj_video.nil?
          video              = Video.new
          video.name         = item.title
          video.channel      = item.embed_html
          video.category     = item.category_title
          video.lyrics       = item.description
          video.images       = item.thumbnail_url.gsub("default.jpg","hqdefault.jpg")
          video.link         = item.id
          video.category_id  = id
          video.save!
          puts "Added data #{video.name} to database"
        else
          puts "#{Time.now} Ignore youtube"
        end
      rescue => e
        puts "Ingore data with url: #{item[:url]}"
      end
    end

    def save_articles(article, obj, logger, site_id)
      obj_article = obj.articles.find_by(url: article[:link])
      begin
        if check_before_save_articles?(article) == false
          puts "============ Ignore ========="
          puts article[:link]
        end
        if check_before_save_articles?(article) == true && obj_article.nil?
          # if obj_article.nil?
          obj_article = obj.articles.new
          obj_article.title = article[:title]
          # end
          content = before_save_content(article[:content])
          obj_article.content           = content_of_article(article, content, site_id)
          obj_article.short_content     = article[:short_content]
          obj_article.image             = article[:image]
          obj_article.url               = article[:link]
          obj_article.category_news_id  = article[:category_news_id]
          obj_article.save!
          puts "Added data #{obj_article.title} to database=============="
        else
          puts "Ingore data with url: #{article[:link]}"
        end
      rescue => e
        logger.info("Ingore data with url: #{article[:url]}")
        logger.error("#{Time.now} #{e.inspect}")
        logger.error("#{Time.now} #{e.backtrace}")
      end
    end

    ### This function allowed save musics to database
    def save_musics(musics)
      obj_musics = Music.find_by(url: musics[:url_original])

      obj_musics = obj_musics.present? ? obj_musics : Music.new
      obj_singer = Singer.find_by(name: musics[:singer])
      if obj_singer.nil?
        obj_singer = Singer.new
        obj_singer.name = musics[:singer]
        obj_singer.image_singer = "musics/singers/default.jpg"
        obj_singer.save
      end
      obj_singer = Singer.find_by(name: musics[:singer])

      begin
        obj_musics.title              = musics[:title]
        obj_musics.type_music         = musics[:type_music].present? ? musics[:type_music] : "Nhạc trẻ"
        obj_musics.count_listen       = musics[:count_listen].to_i
        obj_musics.format             = musics[:format].present? ? musics[:format] : "mp3"
        obj_musics.singer_name        = obj_singer.name
        obj_musics.singer_image       = obj_singer.image_singer
        obj_musics.url                = musics[:url]
        obj_musics.url_original       = musics[:url_original]
        obj_musics.site_id            = musics[:site_id].to_i
        obj_musics.category_id        = musics[:category_id]
        obj_musics.lsxanh             = musics[:lsxanh]
        obj_musics.lyrics             = musics[:lyrics]
        obj_musics.information        = musics[:information]
        obj_musics.nhachot            = musics[:nhachot]
        obj_musics.music_top          = musics[:music_top]
        obj_musics.save!
        puts "push data to database"
      rescue => e
        puts "Ingore data with url: #{musics[:url]}"
      end
    end

    def save_singers(name)
      obj_singer = Singer.find_by(name: name)
      obj_singer = obj_singer.present? ? obj_singer : Singer.new
      obj_singer.image_singer = "musics/singers/vietnam/#{name}.jpg"
      obj_singer.name = name
      begin
        obj_singer.save!
        puts "Import singer information #{name}"
      rescue => e
        puts e.inspect
        puts e.backtrace
      end
    end

    def before_save_content(content)
      content = content.to_html
      content = content.include?("style=") ? content.gsub("style=", "") : content
      content =
      if content.present? && content.include?('href="http://')
        content.include?('https://www.youtube.com') ? content : content.gsub('href="http://', "")
      else
        content
      end
    end

    def before_save_content_width(article, content, site_id)
      content = content.gsub("width=", "") if content.include?("width=")
      if site_id == 21
        return content.gsub('href="http://ngoisao.net/', "") if content.include?("http://ngoisao.net/")
      elsif site_id == 30
        return content.gsub('href="http://bepeva.com', "") if content.include?("http://bepeva.com")
      elsif site_id == 23
        return content.gsub('href="http://news.zing', "") if content.include?('href="http://news.zing')
      end
      content
    end

    def content_of_article(article, content, site_id)
      if site_id == 21
        content = before_save_content_width(article, content, site_id)
        if article[:short_content] != article[:title]
          return "#{article[:short_content]}\n #{content}"
        end
        return content
      elsif site_id == 30
        return before_save_content_width(article, content, site_id)
      elsif site_id == 23
        return before_save_content_width(article, content, site_id)
      end
      content
    end

    private

    def open_url(url)
      begin
        body = open(url, "r:UTF-8").read
        status = true
      rescue => e
        status = false
      end
      return body, status
    end

    def ignore_ad(title)
      title = title.try(:downcase)
      return false if title.include?("khuyến mại")
      true
    end

    def check_before_save_articles?(item)
      item[:title].present? && item[:content].present? && item[:image].present?
    end
  end
end

