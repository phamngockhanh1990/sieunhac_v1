# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'sieunhac_v1'
set :repo_url, 'git@bitbucket.org:baran19901990/sieunhac_v1.git'
set :branch, 'master'

# Default branch is :master
set :rvm_type, :system
# set :rvm_ruby_version, '2.2.1@music'

# Default deploy_to directory is /var/www/my_app_name
set :bundle_gemfile, -> { release_path.join('Gemfile') }
set :bundle_dir, -> { shared_path.join('bundle') }
set :bundle_flags, nil
set :bundle_without, %w{development test}.join(' ')
set :bundle_binstubs, nil
set :bundle_roles, :all
set :app_path, '/var/www/sieunhac_v1/current/'
set :unicorn_pid, "#{current_path}/tmp/pids/sieunhac_v1.pid"

# Default value for :scm is :git
set :scm, :git
set :pty, true
set :use_sudo, true
set :format, :pretty
set :log_level, :debug
set :keep_releases, 5

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', "public/images/musics/singers/vietnam")

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }


namespace :deploy do
  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
  after :finishing, "deploy:cleanup"
end

# before :deploy, "deploy:assets:clear"
# after :deploy, "deploy:restart:unicorn"
after :deploy, "deploy:clear:cache"
after 'deploy:publishing', 'deploy:restart'
