require 'exception_notification/rails'

ExceptionNotification.configure do |config|
  config.ignore_if do |exception, options|
    not Rails.env.production?
  end

  config.add_notifier :slack, {
    :webhook_url => 'https://hooks.slack.com/services/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',
    :channel => '#sieunhac_dev',
    :link_names => true
  }
end
