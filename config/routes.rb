Rails.application.routes.draw do

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }


  root 'home#index'
  get 'about', to: 'home#about'
  get 'contact', to: 'home#contact'
  post 'confirm', to: 'home#confirm'
  get 'chinhsach', to: 'home#privacy', as: 'privacy'
  get 'list_api', to: 'home#list_api', as: 'list_api'

  resources :musics

  resources :videos, only: [:index, :show]

  resources :messages
  resources :search, only: [:index]
  resources :articles, only: [:show, :edit, :update]
  resources :category_news, only: [:show]
  resources :category_video, only: [:show]
  resources :singer, only: [:index]
  get "singer/:id", to: "singer#show", constraints: { id: /[0-9]+/ }, as: "singer_show"

  resources :category_story, only: [:show] do
    resources :story_type, only: [:show]
  end

  resources :story_type, only: [:show] do
    resources :story, only: [:show]
  end

  resources :story, only: [:show] do
    resources :chapter, only: [:index, :show]
  end

  get "choose_chapter/:story_id/:id", to: "chapter#choose_chapter", as: "choose_chapter"

  ### redirect to root path if unknown root
  if Rails.env.production?
    get '*path' => redirect('/')
    get '404', :to => 'application#page_not_found'
    get '422', :to => 'application#server_error'
    get '500', :to => 'application#server_error'
  end

  namespace :admin do
    resources :home, only: [:index]
    get "ajax_sort_article", to: "home#sort_article"
  end

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
