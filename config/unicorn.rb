app_name = 'sieunhac_v1'
root = "/var/www/sieunhac_v1/current"
working_directory root
pid_file = "#{root}/tmp/pids/#{app_name}.pid"
stderr_path "#{root}/log/#{app_name}_stderr.log"
stdout_path "#{root}/log/#{app_name}_stdout.log"

old_pid = pid_file + '.oldbin'

pid pid_file
listen "/tmp/#{app_name}.sock", backlog: 64
listen(3000, backlog: 64) if ENV['RAILS_ENV'] == 'development'

worker_processes ENV['RAILS_ENV'] == "production" ? 5 : 3
preload_app true
timeout 60

before_fork do |server, worker|

  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.connection.disconnect!
  end

  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
    end
  end
end

after_fork do |server, worker|
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.establish_connection
  end
end
