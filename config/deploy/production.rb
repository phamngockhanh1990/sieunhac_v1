# server '128.199.231.39', user: 'deploy', roles: %w{app db web}, my_property: :my_value
# server 'example.com', user: 'deploy', roles: %w{app web}, other_property: :other_value
# server 'db.example.com', user: 'deploy', roles: %w{db}
set :stage, :production
set :rails_env, 'production'


# role :app, %w{128.199.231.39}

set :bundle_roles, :all

server '128.199.231.39', user: 'root', roles: %w{web app db}
# http://capistranorb.com/documentation/getting-started/configuration/

set :deploy_to, '/var/www/sieunhac_v1'
# Custom SSH Options
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
set :ssh_options, {
  user: 'root'
}
