# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
set :environment_variable, "RAILS_ENV"
# Environment defaults to production
set :environment, "production"
# Path defaults to the directory `whenever` was run from
set :path, "/home/khanhpn/sieunhac_v1"

set :output, {:error => "log/cron_error_log.log", :standard => "log/cron_log.log"}

### import data from site_id=0 nhaccuatui
# every :day, at: '2:50am' do
  # rake "crawler:import site_id=0"
# end

### import data from site_id=1 zing
every 2.hours do
  rake "crawler:news site_id=23"
end
every 2.hours do
  rake "crawler:news site_id=21"
end

every 3.hours do
  rake "crawler:news site_id=20"
end

every 4.hours do
  rake "crawler:news site_id=22"
end

every 6.hours do
  rake "crawler:news site_id=30"
end

### set schedule crawl all videos from clip.vn page every day at 5:27 AM
every :day, at: "0:20am" do
  rake "crawler:youtube"
end

### set schedule crawl music lansongxanh
every :day, at: '1:30am' do
  rake "crawler:music"
  rake "crawler:viet"
end
