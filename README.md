# Install project sieunhac_v1 #

## 1. Install imagemagick
```
#!ruby
sudo apt-get install imagemagick
```

## 2. Install javascript runtime

```
#!ruby
sudo apt-get install nodejs
yum -y install nodejs
```