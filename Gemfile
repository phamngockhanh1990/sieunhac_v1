source 'http://rubygems.org'

gem 'rails', '4.2.0'
gem 'mysql2'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'turbolinks'
gem 'execjs'
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'jquery-slick-rails'
gem 'jquery-ui-rails'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
gem 'unicorn'
gem 'draper'
gem 'static_model'
gem 'rsolr'
gem 'rsolr-ext'
gem 'devise'
gem 'newrelic_rpm'
gem 'rails_config'

## Gem paginate and bootstrap
gem 'kaminari'
gem 'bootstrap-kaminari-views'

## Gem social media same as twitter vs facebook
gem 'twitter'
gem "koala", "~> 2.0"
gem 'shareable'
gem 'social-share-button', github: "huacnlee/social-share-button"

## Upload image use carrierwave integrated with mini_magick
gem 'carrierwave'
gem "mini_magick"
gem 'eventmachine'
gem 'em-http-request'
gem 'unicode'

## Set crontab
gem 'whenever', :require => false

## Editor
gem 'tinymce-rails'

### Delayed Job ###
gem "delayed_job"
gem "delayed_job_active_record"
gem "daemons"
gem 'sidekiq'

gem 'mechanize'
gem 'mongoid'
gem 'utf8-cleaner'

## Gem check request url
gem 'faraday'

## Gem meta tag
gem 'meta-tags'

## Crawl data from youtube
gem 'google-api-client'
gem 'trollop'
gem 'yt'

group :development, :test do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'awesome_print'
  gem 'hirb'
  gem 'hirb-unicode'
  gem 'thin'
  # gem 'xray-rails'
  gem "rack-dev-mark"
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'capistrano'
  gem 'capistrano-bundler'
  gem 'capistrano-rvm'
  gem 'capistrano-rails', group: :development
  gem 'capistrano3-unicorn'
  gem 'web-console', '~> 2.0'
  gem 'spring', '~> 1.3.6'
  gem 'rspec'
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'capybara'
  # gem 'rack-mini-profiler'
end

# gem pry for debug
gem 'pry-rails'
gem 'pry-doc'
gem 'pry-stack_explorer'
gem 'pry-byebug'
gem 'pry'

### norification to slack ###
gem 'slack-notifier'
gem 'exception_notification', git: 'https://github.com/smartinez87/exception_notification.git'

### Friendly for url
gem 'friendly_id'
# gem "passenger"

## gem signin social
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'will_paginate'

### bootstrap
gem 'bootstrap-sass', '~> 3.3.5'
gem 'adminlte'
